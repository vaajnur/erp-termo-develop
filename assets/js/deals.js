const editButtons = {
  edit: {
    class: 'btn mr-1 btn-primary rounded',
    html: '<span class="mdi mdi-pencil"></span>',
    action: 'edit'
  },
  delete: {
    class: 'btn btn-danger rounded',
    html: '<i class="fas fa-trash-alt"></i>',
    action: 'delete'
  },
  save: {
    class: 'btn btn-success rounded',
    html: '<span class="mdi mdi-check"></span>',
    action: 'save'
  },
  confirm: {
    class: 'btn btn-danger rounded',
    html: '<span class="mdi mdi-check"></span>',
  }
}


var guides_json = $('.guides_json').text()
if(guides_json !== undefined){
	guides_arr = JSON.parse(guides_json)
	console.log(guides_arr)
	for (i in guides_arr) {
		var select = '<select class="tabledit-input form-control input-sm" name="'+i+'">';
		var select2 = {};
		for (i2 = 0; i2<guides_arr[i].length;i2++) {
			select += '<option value="'+guides_arr[i][i2]['id']+'">'+guides_arr[i][i2]['name']+'</option>';
			// var obj1 = {}
			select2[guides_arr[i][i2]['id']] =  guides_arr[i][i2]['name'];
			// select2 = obj1;
		}
		select += '</select>'
		window[i+'_select'] = select
		window[i+'_select2'] = select2
	}
}

var row_el = `<tr role="row" class="added" id="">
<td class="sorting_1" style="display: none;"><span class="tabledit-span tabledit-identifier"></span><input class="tabledit-input tabledit-identifier" type="hidden" name="id" value="" disabled=""></td>
<td class="checkbox-td d-none"><input type="checkbox" name="" value="1" class="select-row"></td>
<td class="tabledit-view-mode"><span class="tabledit-span"></span>${contract_type_select}</td>
<td class="tabledit-view-mode"><span class="tabledit-span"></span><input class="tabledit-input form-control input-sm" type="text" name="manager" value="#MANAGER_ID#" style="display: none;" disabled="" readonly></td>
<td class="tabledit-view-mode"><span class="tabledit-span"></span><input class="tabledit-input form-control input-sm" type="text" name="fio_number" value="" style="display: none;" disabled=""></td>
<td class="tabledit-view-mode"><span class="tabledit-span"></span><input class="tabledit-input form-control input-sm" type="text" name="cost_by_contract" value="" style="display: none;" disabled=""></td>
<td class="tabledit-view-mode"><span class="tabledit-span"></span><input class="tabledit-input form-control input-sm" type="text" name="payment_primary" value="" style="display: none;" disabled=""></td>
<td class="tabledit-view-mode"><span class="tabledit-span"></span><input class="tabledit-input form-control input-sm" type="text" name="self_cost" value="" style="display: none;" disabled=""></td>
<td class="tabledit-view-mode"><span class="tabledit-span"></span><input class="tabledit-input form-control input-sm" type="text" name="square" value="" style="display: none;" disabled=""></td>
<td class="tabledit-view-mode"><span class="tabledit-span"></span><input class="tabledit-input form-control input-sm" type="text" name="length" value="" style="display: none;" disabled=""></td>
<td class="tabledit-view-mode"><span class="tabledit-span"></span><input class="tabledit-input form-control input-sm" type="text" name="width" value="" style="display: none;" disabled=""></td>
<td class="tabledit-view-mode"><span class="tabledit-span"></span><input class="tabledit-input form-control input-sm" type="text" name="cross_section_of_the_bar" value="" style="display: none;" disabled=""></td>
<td class="tabledit-view-mode"><span class="tabledit-span"></span><input class="tabledit-input form-control input-sm" type="text" name="material" value="" style="display: none;" disabled=""></td>
<td class="tabledit-view-mode"><span class="tabledit-span"></span><input class="tabledit-input form-control input-sm" type="text" name="material_from_partners" value="" style="display: none;" disabled=""></td>
<td class="tabledit-view-mode"><span class="tabledit-span"></span><input class="tabledit-input form-control input-sm" type="text" name="cost_of_fitting" value="" style="display: none;" disabled=""></td>
<td class="tabledit-view-mode"><span class="tabledit-span"></span><input class="tabledit-input form-control input-sm" type="text" name="cost_of_painting" value="" style="display: none;" disabled=""></td>
<td class="tabledit-view-mode"><span class="tabledit-span"></span><input class="tabledit-input form-control input-sm" type="text" name="arch_comission" value="" style="display: none;" disabled=""></td>
<td class="tabledit-view-mode"><span class="tabledit-span"></span><input class="tabledit-input form-control input-sm" type="text" name="delivery_sum" value="" style="display: none;" disabled=""></td>
<td class="tabledit-view-mode"><span class="tabledit-span"></span></td>
<td class="tabledit-view-mode"><span class="tabledit-span"></span><input class="tabledit-input form-control input-sm" type="date" name="pay_date" value="" style="display: none;" disabled=""></td>
<td class="tabledit-view-mode"><span class="tabledit-span"></span></td>
<td class="tabledit-view-mode"><span class="tabledit-span"></span><input class="tabledit-input form-control input-sm" type="date" name="shipment" value="" style="display: none;" disabled=""></td>
<td class="tabledit-view-mode"><span class="tabledit-span"></span><input class="tabledit-input form-control input-sm" type="text" name="shipment_fact" value="" style="display: none;" disabled=""></td>
<td class="tabledit-view-mode"><span class="tabledit-span"></span>${pay_type_select}</td>
<td class="tabledit-view-mode"><span class="tabledit-span"></span><input class="tabledit-input form-control input-sm" type="text" name="goods" value="" style="display: none;" disabled=""></td>
<td class="tabledit-view-mode"><span class="tabledit-span"></span>${manufacturer_select}</td>
<td class="tabledit-view-mode"><span class="tabledit-span"></span>${сarrier_select}</td>
<td class="tabledit-view-mode"><span class="tabledit-span"></span>${ad_source_select}</td>
<td class="d-none"><input class="tabledit-input form-control input-sm" type="hidden" value="" name="parent_id"></td>
<td class="d-none"><input class="tabledit-input form-control input-sm month" type="hidden" value="#MONTH#" name="month"></td>
<td style="white-space: nowrap; width: 1%;"><div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
                   <div class="btn-group btn-group-sm" style="float: none;"><button type="button" class="tabledit-edit-button btn mr-1 btn-primary rounded" style="float: none;"><span class="mdi mdi-pencil"></span></button><button type="button" class="tabledit-delete-button btn btn-danger rounded" style="float: none;"><i class="fas fa-trash-alt"></i></button></div>
                   <button type="button" class="tabledit-save-button btn btn-success rounded" style="display: none; float: none;"><span class="mdi mdi-check"></span></button>
                   <button type="button" class="tabledit-confirm-button btn btn-danger rounded" style="display: none; float: none;"><span class="mdi mdi-check"></span></button>
                   <button type="button" class="tabledit-restore-button btn btn-sm btn-warning" style="display: none; float: none;">Restore</button>
               </div></td></tr>`
	

var row_el2 = `<tr role="row" class="added" id="">
						<td class="sorting_1" style="display: none;"><span class="tabledit-span tabledit-identifier"></span><input class="tabledit-input tabledit-identifier" type="hidden" name="id" value="" disabled=""></td>
						<td class="checkbox-td"></td>
						<td class="tabledit-view-mode"><span class="tabledit-span"></span></td>
						<td class="tabledit-view-mode"><span class="tabledit-span"></span></td>
						<td class="tabledit-view-mode"><span class="tabledit-span"></span></td>
						<td class="tabledit-view-mode"><span class="tabledit-span"></span><input class="tabledit-input form-control input-sm" type="text" name="cost_by_contract" value="" style="display: none;" disabled=""></td>
						<td class="tabledit-view-mode"><span class="tabledit-span"></span><input class="tabledit-input form-control input-sm" type="text" name="payment_primary" value="" style="display: none;" disabled=""></td>
						<td class="tabledit-view-mode"><span class="tabledit-span"></span><input class="tabledit-input form-control input-sm" type="text" name="self_cost" value="" style="display: none;" disabled=""></td>
						<td class="tabledit-view-mode"><span class="tabledit-span"></span><input class="tabledit-input form-control input-sm" type="text" name="square" value="" style="display: none;" disabled=""></td>
						<td class="tabledit-view-mode"><span class="tabledit-span"></span><input class="tabledit-input form-control input-sm" type="text" name="length" value="" style="display: none;" disabled=""></td>
						<td class="tabledit-view-mode"><span class="tabledit-span"></span><input class="tabledit-input form-control input-sm" type="text" name="width" value="" style="display: none;" disabled=""></td>
						<td class="tabledit-view-mode"><span class="tabledit-span"></span><input class="tabledit-input form-control input-sm" type="text" name="cross_section_of_the_bar" value="" style="display: none;" disabled=""></td>
						<td class="tabledit-view-mode"><span class="tabledit-span"></span><input class="tabledit-input form-control input-sm" type="text" name="material" value="" style="display: none;" disabled=""></td>
						<td class="tabledit-view-mode"><span class="tabledit-span"></span><input class="tabledit-input form-control input-sm" type="text" name="material_from_partners" value="" style="display: none;" disabled=""></td>
						<td class="tabledit-view-mode"><span class="tabledit-span"></span><input class="tabledit-input form-control input-sm" type="text" name="cost_of_fitting" value="" style="display: none;" disabled=""></td>
						<td class="tabledit-view-mode"><span class="tabledit-span"></span><input class="tabledit-input form-control input-sm" type="text" name="cost_of_painting" value="" style="display: none;" disabled=""></td>
						<td class="tabledit-view-mode"><span class="tabledit-span"></span><input class="tabledit-input form-control input-sm" type="text" name="arch_comission" value="" style="display: none;" disabled=""></td>
						<td class="tabledit-view-mode"><span class="tabledit-span"></span><input class="tabledit-input form-control input-sm" type="text" name="delivery_sum" value="" style="display: none;" disabled=""></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td class="tabledit-view-mode"><span class="tabledit-span"></span><input class="tabledit-input form-control input-sm" type="text" name="goods" value="" style="display: none;" disabled=""></td>
						<td></td>
						<td></td>
						<td></td>
						<td class="tabledit-view-mode d-none"><span class="tabledit-span"></span><input class="tabledit-input form-control input-sm" type="text" name="parent_id" value="#PARENT_ID#" style="display: none;" disabled=""></td>
						<td class="d-none"><input class="tabledit-input form-control input-sm month" type="hidden" value="#MONTH#" name="month"></td>
					<td style="white-space: nowrap; width: 1%;"><div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
                                       <div class="btn-group btn-group-sm" style="float: none;"><button type="button" class="tabledit-edit-button btn mr-1 btn-primary rounded" style="float: none;"><span class="mdi mdi-pencil"></span></button><button type="button" class="tabledit-delete-button btn btn-danger rounded" style="float: none;"><i class="fas fa-trash-alt"></i></button></div>
                                       <button type="button" class="tabledit-save-button btn btn-success rounded" style="display: none; float: none;"><span class="mdi mdi-check"></span></button>
                                       <button type="button" class="tabledit-confirm-button btn btn-danger rounded" style="display: none; float: none;"><span class="mdi mdi-check"></span></button>
                                       <button type="button" class="tabledit-restore-button btn btn-sm btn-warning" style="display: none; float: none;">Restore</button>
                                   </div></td></tr>`


$(function(){
	// console.log(contract_type_select2)
	
$('.jtable-editable').each(function(){
	tbl_edit($(this))
})

function tbl_edit(tag){	
	tag.Tabledit({
	    hideIdentifier: true,
	    columns: {
	      identifier: [0, 'id'],
	      editable: [
	        // [1, 'parent_id'],
	        [2, 'contract_type', JSON.stringify(contract_type_select2)],
	        // [3, 'manager'],
	        [4, 'fio_number'],
	        [5, 'cost_by_contract'],
	        [6, 'payment_primary'],
	        [7, 'self_cost'],
	        [8, 'square'],
	        [9, 'length'],
	        [10, 'width'],
	        [11, 'cross_section_of_the_bar'],
	        [12, 'material'],
	        [13, 'material_from_partners'],
	        [14, 'cost_of_fitting'],
	        [15, 'cost_of_painting'],
	        [16, 'arch_comission'],
	        [17, 'delivery_sum'],
	        // [18, 'margin'],
	        [19, 'pay_date', 'date'],
	        // [20, 'need_postpay'],
	        [21, 'shipment', 'date'],
	        [22, 'shipment_fact'],
	        [23, 'pay_type', JSON.stringify(pay_type_select2)],
	        [24, 'goods'],
	        [25, 'manufacturer', JSON.stringify(manufacturer_select2)],
	        [26, 'сarrier', JSON.stringify(сarrier_select2)],
	        [27, 'ad_source', JSON.stringify(ad_source_select2)],
	        [28, 'parent_id'],
	        [29, 'month'],
	      ]
	    },
	    buttons: editButtons,
	    onDraw: function () {},
	    onSuccess: function (data, textStatus, jqXHR) {},
	    onFail: function (jqXHR, textStatus, errorThrown) {},
	    onAlways: function () {

	    },
	    onAjax: function (action, serialize) {}		
	})
}

	// add row
	$('.add-deal').on('click', function(){
		row_el = row_el.replace('#MONTH#', $('.tab-pane.active.show').attr('data-month'))
		row_el = row_el.replace('#MANAGER_ID#', $('.user_id').text())
		$('.deals-table').append(row_el)
		$('.deals-table').find('.added .tabledit-edit-button').trigger('click')
	})

	// add line
	$('.add-deal-line').on('click', function(){
		$(this).addClass('d-none')
		$('.add-deal-line-confirm').removeClass('d-none')
		$('.add-deal-line-cancel').removeClass('d-none')
		$('.checkbox-td').removeClass('d-none')
	})

	// cancel line
	$('.add-deal-line-cancel').on('click', function(){
		$(this).addClass('d-none')
		$('.add-deal-line').removeClass('d-none')
		$('.add-deal-line-confirm').addClass('d-none')
		$('.checkbox-td').addClass('d-none')

	})
	// confirm
	$('.add-deal-line-confirm').on('click', function(){
		$('.deals-table tr').find('.select-row:checked').each(function(){
			var parent_id = $(this).closest('tr').attr('id')
			row_el2 = row_el2.replace('#PARENT_ID#', parent_id)
			row_el2 = row_el2.replace('#MONTH#', $('.tab-pane.active.show').attr('data-month'))
			$(this).closest('tr').after(row_el2)
			$(this).closest('tr').next().find('.tabledit-edit-button').trigger('click')

		})
	})

	// active tab
	if(localStorage.getItem('active-deal-tab') !== undefined)
		$("#"+localStorage.getItem('active-deal-tab')).trigger('click')

	$('#myTab li a').on('click', function(){
		localStorage.setItem('active-deal-tab', $(this).attr('id'))
	})

})