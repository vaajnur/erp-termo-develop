/*jslint browser: true*/

/*global $, jQuery, alert*/

/**
 * Перевод кнопки в состояние "Загрузка"
 */
function button_loading(button) {
	button.prop('disabled', true);
	button.find('.button-text').css('display', 'none');
	button.find('.button-wait').css('display', 'block');
}

/**
 * Перевод кнопки в начальное состояние
 */
function button_init(button) {
	button.prop('disabled', false);
	button.find('.button-text').css('display', 'block');
	button.find('.button-wait').css('display', 'none');
}

/**
 * Перевод кнопки в состояние "Выполнено"
 */
function button_success(button) {
	button.prop('disabled', true);
	button.find('.button-text').html(button.data('msg')).css('display', 'block');
	button.removeClass('btn-primary').addClass('btn-success');
	button.find('.button-wait').css('display', 'none');
	/*$('.btn-edit-table').removeClass('d-none');*/
}

/**
 * Числовое значение в денежный формат
 */
function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
	try {
		decimalCount = Math.abs(decimalCount);
		decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

		const negativeSign = amount < 0 ? "-" : "";

		let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
		let j = (i.length > 3) ? i.length % 3 : 0;

		return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
	} catch (e) {
		console.log(e)
	}
}

function ActiveTouchSpin(el) {
	$(el).TouchSpin({
		min: 0,
		max: 10000,
		step: 1,
		decimals: 0,
		boostat: 5,
		maxboostedstep: 10
	});
}

function fieldValid () {
	let isValid;
	const error = 0;
	let formReq = $(`.card-box ._req`);

	for (let i = 0; i < formReq.length; i++) {
		let input = formReq[i];
		$(input).removeClass('is-invalid');
		console.log(input.value)


		if (input.value === '') {
			$(input).addClass('is-invalid');
			error++;
		}
	}
	return error;
}

ActiveTouchSpin(".touchspin");

$(document).ready(function () {

  $('.modal-form-sendcheck-material').on('hidden.bs.modal', function (e) {
    $( ".table-sendcheck" ).html(' ');
  });
	
	$('.select2').select2({
		minimumResultsForSearch: -1
	});
	$(".profile-select, .breed-select").select2({
        tags: true,
        selectOnClose: true
    });
	$("#tocolor_color_popup, #tocolor_maslo_popup").select2({
        tags: true,
        selectOnClose: true,
        dropdownParent: $(".modal-form-tocolor-material")
    });

	$('.datepicker').datepicker({
		format: 'dd.mm.yyyy',
		startDate: '+1d',
		autoclose: true,
		changeMonth: true 
	});
	$('.input-count-raw-material').on('change', function () {
		console.log($(this).val())
		console.log($(this).attr('max'))
		console.log($(this).val()>$(this).attr('max')) 
    	if (parseInt($(this).val())>parseInt($(this).attr('max'))) 
    		$(this).val($(this).attr('max'));
	});

	// Фиксируем панель управления таблицами при скролле
	// $(window).scroll(function () {
	// 	console.log($(window).scrollTop())
	//   if ($(window).scrollTop() > 249) {
	// 	$('.table-top-controls').addClass('table-top-controls-fixed')
	// 	$('.table-top-controls-fake').css('height', '156px')
	//   } else {
	//   	$('.table-top-controls').removeClass('table-top-controls-fixed')
	//   	$('.table-top-controls-fake').css('height', '0')
	//   }
	// });
	//
});

/**
 * Отображение формы создания новой партии
 */
$(document).on('click', '.btn-show-form-add-consignment', function () {
	const btn = $(this);
	btn.addClass('d-none');
	$('form[name="add_consignment"]').removeClass('d-none');
	$('#storage_block_control').addClass('d-none');
});

/**
 * Скрытие формы создания новой партии
 */
$(document).on('click', '.btn-hide-form-add-consignment', function () {
	const btn = $(this);
	$('.btn-show-form-add-consignment').removeClass('d-none');
	$('form[name="add_consignment"]').addClass('d-none');
	$('#storage_block_control').removeClass('d-none');
});

/**
 * Добавление новых пустых полей в конец формы создания партии
 */
$(document).on('click', '.add-more-products-raw', function () {

    const input = $('.list-raw-materials .field-input-pack-raw-materials');
    input.removeClass('field-input-pack-raw-materials');
    const element = $('#row_raw_materials').clone().removeClass('d-none').appendTo('.list-raw-materials').removeAttr('id');
	element.find(".select2").select2({
        tags: true,
        selectOnClose: true
	});
	// удаление дублирующихся селектов если они задулировались
	let selectsCount = element.find('.select-td:first').find('.select2-container--default').length
	if (selectsCount > 2) {
		element.find('.select-td').find('.select2-container--default:last').remove();
	}

    ActiveTouchSpin(element.find(".touchspin-input"));
});

/**
 * Добавление новых пустых полей в конец формы создания конечных товаров из сырья
 */

$(document).on('click', '.add-more-products-complete', function () {
    const input = $('.list-complete-products .field-input-name-complete-products');
    input.removeClass('field-input-name-complete-products');
    const element = $('#row_complete_product').clone().removeClass('d-none').appendTo('.list-complete-products').removeAttr('id');
    ActiveTouchSpin(element.find(".touchspin-input"));
});

/**
 * Выбор породы из выпадающего списка 
 */

$(document).on('change', '#breed_select, #breed_select_popup', function () {
	let elem = $(this);
	$.ajax({
		url: '/storage/GetListTintByID/',
		type: 'POST',
		data: {id: $(this).val()},
		success: function (data) {
			const array = JSON.parse(data);
			let tint_select = $('#tint_select');
			if(elem.attr("id") == "breed_select_popup"){
                tint_select = $('#tint_select_popup');
			}
			const select = tint_select.select2('destroy');
			select.find('option').not(':first').remove();

			$.each(array, function (i, item) {
				const add_option = $('<option>', {
					value: item['id'],
					text: item['name']
				});
				select.append(add_option);
			});

			tint_select.select2({
				minimumResultsForSearch: -1
			});
		}
	});
});

/**
 * Создание новой партии
 */
 
$(document).on('click', '.btn-add-consignment', function () {
	const button = $(this);

	button_loading(button);

	let error = 0;
	let test = '';
	let filled = false;
	button.closest('form').find('.form-control').each(function () {
		$(this).removeClass('is-invalid');
		var row = $(this).closest('.row')
		var name = row.find('[name=name\\[\\]]').val()
		if(name != '' && name != undefined) filled = true;
		if (  ($(this).prop('required') && name != '') || (filled == false && $(this).prop('required')) ) {
			$(this).removeClass('is-invalid');
			if($(this).hasClass('select2'))
				$(this).next().removeClass('is-invalid')
			// var a1 = $(this).val()
			// console.log(a1)
			if ($(this).val() === null || $(this).val() === '0' || $(this).val() === '0.000' || $(this).val() === 0 || $(this).val().length === 0) {
				if($(this).hasClass('select2'))
					$(this).next().addClass('is-invalid')
				$(this).addClass('is-invalid');
				error++;
				/*test=test+' '+$(this).attr('name');*/
			}
		}
	});

	if (error === 0) {
		const formData = new FormData(button.closest('form')[0]);
		console.log(button.closest('form').serializeArray())
		
		// return false
		$.ajax({
			url: '/storage/AddConsignment/',
			type: 'POST',
			processData: false,
			contentType: false,
			data: formData,
			success: function (data) {
				const answer = data.split(';');
				const success = answer[0].split('::');
				const error = answer[0].split(':');

				if (error[0] === 'error' && error[1] !== null) {
					const input_error = $('[name="' + error[1] + '"]');
					input_error.addClass('is-invalid');
					throw new Error(error[1])
				} else if (success[0] === 'success') {
					button_success(button);
					button.prop('disabled', true);
					setTimeout(function () {
						location.reload();
					}, 1500);
				}
			}
		});
	} else {
		button_init(button);
	}
	return false;
});

/**
 * Удаление товара из партии
 */
$(document).on('click', '.btn-delete-raw-material', function () {
	//if (confirm('Вы уверены?')) {
		const button = $(this);
		const tr = button.closest('tr');
		tr.remove()
		const count_raw_material = button.closest('tbody').find('tr').length;
    if (button.hasClass('btn-danger')) {
      tr.find('input').prop('disabled',true);
      button.removeClass('btn-danger');
      button.addClass('btn-success');
      tr.addClass('bg-secondary');
    } else {
      tr.find('input').prop('disabled',false);
      button.addClass('btn-danger');
      button.removeClass('btn-success');
      tr.removeClass('bg-secondary');
    }
    /*
		if (count_raw_material === 1) {
			button.closest('.card-box').remove();
		} else {
			button.closest('tr').remove();
		}
    */
    
		$.ajax({
			url: '/storage/DeleteRawMaterial/',
			type: 'POST',
			data: {id: button.data('id')},
			success: function (data) {
			}
		});
    
	//}
});

/*
удаление пустой строки добавления новой группы
 */
$(document).on('click', '.btn-delete-raw-material-new', function () {
	const button = $(this);
	const tr = button.closest('.row');
	tr.remove()
})


// Отмена редактирования групп
$(document).on('click', '.btn-edit-table-cancel', function () {
	const button = $(this);
	const tr = button.closest('.card-box');
  hideTableEditor(tr);
  // ккнопка "изменить группу" disabled false
  button.closest('.card-box').find('.btn-edit-table').attr("disabled", false);
  // удаляю лишнюю строку
  if(window.new_row[0] !== undefined){
  	for(i=0;i<window.new_row.length;i++){
  		window.new_row[i].remove()
  	}
  }

	// заголовок для партии
	if(tr.find('.production-title-inp').length){
		tr.find('.production-title-inp').addClass('d-none')
		tr.find('.production-title').show()
	}

	// редактирование поставки
	if(tr.find('.form-block').length){
		tr.find('.form-block').addClass('d-none')
		tr.find('.cons-params-block').removeClass('d-none')
	}

});

$(document).on('click', '.btn-edit-table-delete', function () {
	const thisbutton = $(this);
	const deleteId = $(this).data('id');
	//console.log('delete '+deleteId);
	if (confirm('Вы уверены?')) {
    const tr = thisbutton.closest('.card-box');
    if (tr.find('tr .checkbox-raw-material').length == 0) {
        $.ajax({
          url: '/storage/DeleteAllMaterial/',
          type: 'POST',
          data: {id: deleteId},
          success: function (data) {
          }
        });
    } else {
      tr.find('tr').each(function () {
        if ($(this).find('.checkbox-raw-material').data('id') === undefined) return true;
        //console.log($(this).find('.checkbox-raw-material').data('id'));
        //const button = $(this).find('.btn-delete-raw-material');
        //if (typeof button.data('id') === 'undefined') return true;
        //console.log('delete '+button.data('id'));
        
        $.ajax({
          url: '/storage/DeleteRawMaterial/',
          type: 'POST',
          data: {id: $(this).find('.checkbox-raw-material').data('id')},
          success: function (data) {
          }
        });
      });
    }
    setTimeout(function () {
      location.reload();
    }, 1500);
    
  }
});

// Сохранение редактирования (Изменить группу)
$(document).on('click', '.btn-edit-table-save', function () {
	console.log($(this).closest('.card-box').find('._req'))
	save_edit_row_errors = ''; 

	// if (confirm('Вы уверены?')) {
    const button = $(this);
    const tr = button.closest('.card-box');
    var error = 0
    tr.find('tr').each(function (key, item) {
		var tr1 = $(this)
      var position = $(this).find('.checkbox-raw-material')
      if ($(this).find('.checkbox-raw-material').data('id') === undefined) return true;
      const positionId = $(this).find('.checkbox-raw-material').data('id');
      console.log($(this).hasClass('bg-secondary'));
      	error = 0
      	// валидация
      	$(item).find('.form-control').each(function () {
			if ($(this).prop('required') && $(this).attr('name') != 'input_count_raw_material') {
				$(this).removeClass('is-invalid');
				if($(this).hasClass('select2'))
					$(this).next().removeClass('is-invalid')
				if ($(this).val() === null || $(this).val() === '0' || $(this).val() === '0.000' || $(this).val() === 0 || $(this).val().length === 0) {
					console.log($(this))
					$(this).addClass('is-invalid');
					if($(this).hasClass('select2'))
						$(this).next().addClass('is-invalid')
					error++;
				}
			}
		});
      
      if(error == 0){
	      // storage/SaveRawMaterial
	      
	      // ++++++++++++++++++++++++
	      // тут добавление новой строки ++++++++++
	      // // ++++++++++++++++++
	      if (positionId == 0) 
	      	addPositionEditor($(this),positionId,button);
	      

	      // удаление
	      if ($(this).hasClass('bg-secondary') && positionId > 0) return delPositionEditor($(this),positionId,button);
			// storage/SaveRawMaterial
			
			// +++++++++++++++++++++
			//  редактирование строк ++++++++++
			// ++++++++++++++++++++
	      if (positionId > 0) 
	      	addTableEditor($(this),positionId,button);
	      // console.log('FAIL: '+positionId);
      }
    });

    if(save_edit_row_errors == '' && error == 0){
	    console.log('reload must be')
	    hideTableEditor(tr);

	    // заголовк продукции
	    if($('.production-title-inp').length){

				$.ajax({
						url: '/storage/UpdateTitleConsignment/',
						type: 'POST',
						async: false,
						data: {
								id: tr.find('[name="consignment_id"]').val(), 
								title: tr.find('.production-title-inp').val()
						},
						success: function (data) {
							console.log(data)
						}
					});
	    }

	    // редактирование поставки
	    if(tr.find('.form-block').length){
	    	tr.find('.consignment_edit_form').submit()
	    }else{
	      location.reload();
	    }

    }
});

function delPositionEditor(tr,id,button){
  console.log('/storage/DeleteRawMaterial/ '+id);
  $.ajax({
    url: '/storage/DeleteRawMaterial/',
    type: 'POST',
    data: {id: id},
    success: function (data) {
    }
  });
}

// Добавление новой строки при редактировании
function addPositionEditor(tr,id,button){
	tr.find('td').each(function () {
		if ($(this).find('input')) {
			$(this).find('input').removeClass('is-invalid');
		}
	});

	console.log(button.parent().parent().find('.edit-etap-date .date_finish').val())
	var data2 = {
			date_finish: button.parent().parent().find('.edit-etap-date .date_finish').val(),
			id: id,
			pack: tr.find('input[name="input_pack"]').val(),
			name: tr.find('select[name="input_name"]').val(),
			breed: tr.find('select[name="breed"] option:selected').val(),
			quantity: tr.find('input[name="input_quantity"]').val(),
			 profile: tr.find('select[name="profile"] option:selected').val(),
			thickness: tr.find('input[name="input_thickness"]').val(),
			width: tr.find('input[name="input_width"]').val(),
			length: tr.find('input[name="input_length"]').val(),
			// volume_m3: tr.find('input[name="input_volume_m3"]').val(),
			volume_m3: tr.find('.volume_m3').text(),
			color: tr.find('select[name="select_color"]').val(),
			consignment_id_success: button.closest('.card-box').find('input[name="consignment_id"]').val()
		}

		console.log(data2)

	$.ajax({
		url: '/storage/SaveRawMaterial/',
		type: 'POST',
		data: data2,
		async: false,
		success: function (data) {
			const answer = data.split(';');
			const success = answer[0].split('::');
			const error = answer[0].split(':');

			if (error[0] === 'error' && error[1] !== null) {
				const input_error = tr.find('[name="input_' + error[1] + '"]');
				input_error.addClass('is-invalid');
				save_edit_row_errors += error[1]
				throw new Error('error input ' + error[1])
			} else if (success[0] === 'success') {
				tr.find('.hide-input').addClass('d-none');
				tr.find('.edit-value').removeClass('d-none');
				tr.find('.btn-edit-raw-material').removeClass('d-none');
				tr.find('.btn-delete-raw-material').removeClass('d-none');
				tr.find('.btn-cancel-edit-raw-material').addClass('d-none');
				tr.find('.btn-save-raw-material').addClass('d-none');
				tr.find(".touchspin-input").trigger('touchspin.destroy');
				tr.find(".select2-active").select2('destroy');

				tr.find('td').each(function () {
					if ($(this).find('input').is(".hide-input")) {
						$(this).find('.edit-value').html($(this).find('input').val());
					}
					if ($(this).find('select').is(".hide-input")) {
						console.log('нашел select');
						$(this).find('.edit-value').html($(this).find('select').find('option:selected').text());
					}
				});

				//Если в ответе был возвращен ID элемента, значит произошло добавление новой строки в БД и обновляем параметры в элемента в таблице
				if (answer[1].split('::')[1] !== undefined) {
					const id = answer[1].split('::')[1];
					tr.find('.checkbox-raw-material').data('id', id).attr('data-id', id);
					tr.find('.btn-edit-raw-material').data('id', id).attr('data-id', id);
					tr.find('.btn-delete-raw-material').data('id', id).attr('data-id', id);
					tr.find('.btn-cancel-edit-raw-material').data('id', id).attr('data-id', id);
					tr.find('.btn-save-raw-material').data('id', id).attr('data-id', id);
				}
			}
		}
	});
}



// сохранение редактированных строк
function addTableEditor(tr,id,button){
	tr.find('td').each(function () {
		if ($(this).find('input')) {
			$(this).find('input').removeClass('is-invalid');
		}
	});
	console.log(button.parent().parent().find('.edit-etap-date .date_finish').val())
	// console.log($('.edit-etap-date .date_finish').val());
	var data1 = {
			id: id,
			date_finish: button.parent().parent().find('.edit-etap-date .date_finish').val(),
			pack: tr.find('input[name="input_pack"]').val() > 0 ? tr.find('input[name="input_pack"]').val() : 1 ,
			// breed: tr.find('select[name="breed"]').val(),
			breed: tr.find('select[name="breed"] option:selected').val(),
			name: tr.find('select[name="input_name"] option:selected').val(),
			quantity: tr.find('input[name="input_quantity"]').val(),
			profile: tr.find('select[name="profile"] option:selected').val(),
			thickness: tr.find('input[name="input_thickness"]').val(),
			width: tr.find('input[name="input_width"]').val(),
			length: tr.find('input[name="input_length"]').val(),
			// volume_m3: tr.find('input[name="input_volume_m3"]').val(),
			volume_m3: tr.find('.volume_m3').text(),
			color: tr.find('select[name="select_color"]').val(),
			consignment_id_success: button.closest('.card-box').find('input[name="consignment_id"]').val()
		}
		console.log(data1)
	// throw new Error('end script')

	$.ajax({
		url: '/storage/SaveRawMaterial/',
		type: 'POST',
		data: data1,
		async: false,
		success: function (data) {
			console.log(data)
			const answer = data.split(';');
			const success = answer[0].split('::');
			const error = answer[0].split(':');

			if (error[0] === 'error' && error[1] !== null) {
				const input_error = tr.find('[name="input_' + error[1] + '"]');
				input_error.addClass('is-invalid');
				save_edit_row_errors += error[1]
				throw new Error('error input ' + error[1])
			} else if (success[0] === 'success') {
				tr.find('.hide-input').addClass('d-none');
				tr.find('.edit-value').removeClass('d-none');
				tr.find('.btn-edit-raw-material').removeClass('d-none');
				/*tr.find('.btn-delete-raw-material').removeClass('d-none');*/
				tr.find('.btn-cancel-edit-raw-material').addClass('d-none');
				tr.find('.btn-save-raw-material').addClass('d-none');
				tr.find(".touchspin-input").trigger('touchspin.destroy');
				tr.find(".select2-active").select2('destroy');
       			 tr.find('.btn-delete-raw-material').addClass('d-none');

				tr.find('td').each(function () {
					if ($(this).find('input').is(".hide-input")) {
						$(this).find('.edit-value').html($(this).find('input').val());
					}
					if ($(this).find('select').is(".hide-input")) {
						console.log('нашел select');
						$(this).find('.edit-value').html($(this).find('select').find('option:selected').text());
					}
				});

				//Если в ответе был возвращен ID элемента, значит произошло добавление новой строки в БД и обновляем параметры в элемента в таблице
				if (answer[1].split('::')[1] !== undefined) {
					const id = answer[1].split('::')[1];
					tr.find('.checkbox-raw-material').data('id', id).attr('data-id', id);
					tr.find('.btn-edit-raw-material').data('id', id).attr('data-id', id);
					tr.find('.btn-delete-raw-material').data('id', id).attr('data-id', id);
					tr.find('.btn-cancel-edit-raw-material').data('id', id).attr('data-id', id);
					tr.find('.btn-save-raw-material').data('id', id).attr('data-id', id);
				}
			}
		}
	});
}


// отмена редактирования
function hideTableEditor(tr){
	// изменить дату окончания сушки
	$('.edit-etap-date').addClass('d-none')
	tr.find('.title_form_consignment_edit').addClass('d-none');
	tr.find('.edit-table-save').addClass('d-none');
	tr.find('.edit-table').removeClass('d-none');
	tr.find('.hide-input').addClass('d-none');
	tr.find('.edit-value').removeClass('d-none');
	tr.find('.btn-edit-raw-material').removeClass('d-none');
	tr.find('.btn-copy-raw-material-inedit').addClass('d-none');
	tr.find('.btn-cancel-edit-raw-material').addClass('d-none');
	tr.find('.btn-save-raw-material').addClass('d-none');
	tr.find(".touchspin-input").trigger('touchspin.destroy');
	tr.find(".select2-active").select2('destroy');
	tr.find('.btn-add-row-table-raw-material').addClass('d-none');
	tr.find('.btn-delete-raw-material').addClass('d-none');
	// tr.find('.volume_m2, .volume_m3').addClass('d-none');
	$('.btn-show-form-send-raw-materials-to-manufacture').removeClass('d-none');
      tr.find('input').prop('disabled',false);
      tr.find('.btn-delete-raw-material').addClass('btn-danger');
      tr.find('.btn-delete-raw-material').removeClass('btn-success');
      tr.find('tr').removeClass('bg-secondary');
}

// Завершить производство (строгание)/Изменить группу
$(document).on('click', '.btn-edit-table', function () {
	const button = $(this);
	const tr = button.closest('.card-box');
	// disabled кнопки
	button.attr('disabled', true);
	// изменить дату окончания сушки
	tr.find('.edit-etap-date').removeClass('d-none')
	// скрытие кнопки Отправить
	$('.btn-show-form-send-raw-materials-to-manufacture').addClass('d-none');
	// Показ заголовка
	tr.find('.title_form_consignment_edit').removeClass('d-none');
	// Кнопки удалить отменить сохранить
	tr.find('.edit-table-save').removeClass('d-none');
	// Скрытие кнопки Изменить
	tr.find('.edit-table').addClass('d-none');
	// Показ ячеек
	tr.find('.hide-input').removeClass('d-none');
	// Кнопка Добавить позицию
	tr.find('.btn-add-row-table-raw-material').removeClass('d-none');
	// Кнопка Удалить
	tr.find('.btn-delete-raw-material').removeClass('d-none');
	// копировать
	tr.find('.btn-copy-raw-material-inedit').removeClass('d-none');
	// Скрытие span
	tr.find('.edit-value').addClass('d-none');
	tr.find('.volume_m2, .volume_m3, .pogonni-metr').removeClass('d-none')
	// 
	tr.find('.btn-edit-raw-material').addClass('d-none');
	/*tr.find('.btn-delete-raw-material').addClass('d-none');*/
	tr.find('.btn-cancel-edit-raw-material').removeClass('d-none');
	tr.find('.btn-save-raw-material').removeClass('d-none');
	ActiveTouchSpin(tr.find(".touchspin-input"));
	tr.find(".select2-active").select2({
		minimumResultsForSearch: -1
	});
	// заголовок для партии
	if(tr.find('.production-title-inp').length){
		tr.find('.production-title-inp').removeClass('d-none')
		tr.find('.production-title').hide()
	}

	// редактирование поставки
	if(tr.find('.form-block').length){
		tr.find('.form-block').removeClass('d-none')
		tr.find('.cons-params-block').addClass('d-none')
	}

});

/**
 * Включение режима редактирования товара в партии
 */
$(document).on('click', '.btn-edit-raw-material', function () {
	const button = $(this);
	const tr = button.closest('tr');
	tr.find('.hide-input').removeClass('d-none');
	tr.find('.edit-value').addClass('d-none');
	tr.find('.btn-edit-raw-material').addClass('d-none');
	tr.find('.btn-delete-raw-material').addClass('d-none');
	tr.find('.btn-cancel-edit-raw-material').removeClass('d-none');
	tr.find('.btn-save-raw-material').removeClass('d-none');
	ActiveTouchSpin(tr.find(".touchspin-input"));
	tr.find(".select2-active").select2({
		minimumResultsForSearch: -1
	});
});

/**
 * Выключение режима редактирования товара в партии
 */
$(document).on('click', '.btn-cancel-edit-raw-material', function () {
	const button = $(this);
	const tr = button.closest('tr');
	tr.find('.hide-input').addClass('d-none');
	tr.find('.edit-value').removeClass('d-none');
	tr.find('.btn-edit-raw-material').removeClass('d-none');
	tr.find('.btn-delete-raw-material').removeClass('d-none');
	tr.find('.btn-cancel-edit-raw-material').addClass('d-none');
	tr.find('.btn-save-raw-material').addClass('d-none');
	tr.find(".touchspin-input").trigger('touchspin.destroy');
	tr.find(".select2-active").select2('destroy');
});

/**
 * Сохранение изменений отредактированного или нового товара в партии
 */
$(document).on('click', '.btn-save-raw-material', function () {
	const button = $(this);
	const id = button.data('id');
	const tr = button.closest('tr');

	tr.find('td').each(function () {
		if ($(this).find('input')) {
			$(this).find('input').removeClass('is-invalid');
		}
	});

	$.ajax({
		url: '/storage/SaveRawMaterial/',
		type: 'POST',
		data: {
			id: id,
			pack: tr.find('input[name="input_pack"]').val(),
			name: tr.find('select[name="input_name"]').val(),
			quantity: tr.find('input[name="input_quantity"]').val(),
			 profile: tr.find('select[name="profile"] option:selected').val(),
			thickness: tr.find('input[name="input_thickness"]').val(),
			width: tr.find('input[name="input_width"]').val(),
			length: tr.find('input[name="input_length"]').val(),
			volume_m3: tr.find('input[name="input_volume_m3"]').val(),
			color: tr.find('select[name="select_color"]').val(),
			consignment_id_success: button.closest('.card-box').find('input[name="consignment_id"]').val()
		},
		success: function (data) {
			const answer = data.split(';');
			const success = answer[0].split('::');
			const error = answer[0].split(':');

			if (error[0] === 'error' && error[1] !== null) {
				const input_error = tr.find('[name="input_' + error[1] + '"]');
				input_error.addClass('is-invalid');
				throw new Error(error[1])
			} else if (success[0] === 'success') {
				tr.find('.hide-input').addClass('d-none');
				tr.find('.edit-value').removeClass('d-none');
				tr.find('.btn-edit-raw-material').removeClass('d-none');
				tr.find('.btn-delete-raw-material').removeClass('d-none');
				tr.find('.btn-cancel-edit-raw-material').addClass('d-none');
				tr.find('.btn-save-raw-material').addClass('d-none');
				tr.find(".touchspin-input").trigger('touchspin.destroy');
				tr.find(".select2-active").select2('destroy');

				tr.find('td').each(function () {
					if ($(this).find('input').is(".hide-input")) {
						$(this).find('.edit-value').html($(this).find('input').val());
					}
					if ($(this).find('select').is(".hide-input")) {
						console.log('нашел select');
						$(this).find('.edit-value').html($(this).find('select').find('option:selected').text());
					}
				});

				//Если в ответе был возвращен ID элемента, значит произошло добавление новой строки в БД и обновляем параметры в элемента в таблице
				if (answer[1].split('::')[1] !== undefined) {
					const id = answer[1].split('::')[1];
					tr.find('.checkbox-raw-material').data('id', id).attr('data-id', id);
					tr.find('.btn-edit-raw-material').data('id', id).attr('data-id', id);
					tr.find('.btn-delete-raw-material').data('id', id).attr('data-id', id);
					tr.find('.btn-cancel-edit-raw-material').data('id', id).attr('data-id', id);
					tr.find('.btn-save-raw-material').data('id', id).attr('data-id', id);
				}
			}
		}
	});
});

/**
 * Отображение формы и блоков управления для перемещения товаров с склада Сырья на склад Производство
 */
$(document).on('click', '.btn-show-form-send-raw-materials-to-manufacture', function () {
	const button = $(this);
	if($(this).hasClass('otgruzka-zakazchiku')){
		$('[name="storage"]').find('option').each(function(k, item){
		   $(item).removeAttr('selected')
		})
		$('[name="storage"]').find('option[value="13"]').attr('selected', 1)
		$('[name="storage"]').attr('disabled', true)
	}
	// Блок с выбором Куда: Срок производства: Группа:
	$('#block_send_raw_materials_to_manufacture').removeClass('d-none');
	// показать чекбокс "отметить все"
	$('.check_all_box').removeClass('d-none')
	$('.check_all').addClass('forSendToManufact')
	// инпут для кол-ва отправки
	$('.manufacture-form').removeClass('d-none');
	// пустая ячейка для сдвига таблицы
	$('.summ-first-cell').removeClass('d-none');
	// Укажите товар и его количество для отправки на склад
	$('#title_form_send_raw_material_to_manufacture').removeClass('d-none');
	
	$('.btn-show-form-add-consignment').addClass('d-none');
	$('form[name="add_consignment"]').addClass('d-none');
	$('#storage_block_control').addClass('d-none');
	$('.btn-add-row-table-raw-material').addClass('d-none');
	$('.btn-edit-table').addClass('d-none');
});

/**
 * Объединение таблиц
 */
$('.join_groups').on('click', function(ev){
	ev.preventDefault()
	$(this).addClass('d-none')
	// плавающий
	$("#join_groups_sticky_block").removeClass('d-none')
	// инпут для кол-ва отправки
	$('.manufacture-form').removeClass('d-none');
		// пустая ячейка для сдвига таблицы
	$('.summ-first-cell').removeClass('d-none');
	// Выбрать все
	$('.check_all_box').removeClass('d-none')
	// Сохранить
	$('.join_groups_save').removeClass('d-none')
	//  отменить
	$('.join_groups_cancel').removeClass('d-none')
	// отправить/Изменить группу
	$('.btn-show-form-send-raw-materials-to-manufacture, .btn-edit-table').addClass('d-none') 
})

/**
 * отменить объединение
 */
$('.join_groups_cancel').on('click', function(ev){
	ev.preventDefault()
	$("#join_groups_sticky_block").addClass('d-none')
	// инпут для кол-ва отправки
	$('.manufacture-form').addClass('d-none');
		// пустая ячейка для сдвига таблицы
	$('.summ-first-cell').addClass('d-none');
	// Выбрать все
	$('.check_all_box').addClass('d-none')
	// Сохранить
	$('.join_groups_save').addClass('d-none')	
		//  отменить
	$('.join_groups_cancel').addClass('d-none')
	// отправить/Изменить группу
	$('.btn-show-form-send-raw-materials-to-manufacture, .btn-edit-table, .join_groups_block, .join_groups').removeClass('d-none')

})


/**
 * Выбрать все
 */
// $('.check_all').on('click', function(ev){
//    if($(this).parent().find('input').is(':checked')){
//    		console.log('unchecked')
//    		$(this).closest('.form-group').next().find('.manufacture-form .checkbox-raw-material').attr('checked', false)
// 		$(this).closest('.form-group').next().find('.manufacture-form').each(function(k, v){
// 			// var max_quant = $(this).find('.input_count_raw_material').val()
// 			$(this).find('.input_count_raw_material').attr('disabled', false)
// 			$(this).find('.checkbox-raw-material').attr('disabled', false)
// 		})
// 	}else{
// 		$(this).closest('.form-group').next().find('.manufacture-form .checkbox-raw-material').attr('checked', true)
// 		$(this).closest('.form-group').next().find('.manufacture-form').each(function(k, v){
// 			var max_quant = $(this).find('.input_count_raw_material').val()
// 			// var ids = $(this).find('.checkbox-raw-material').attr('data-id')
// 			$(this).find('.checkbox-raw-material').attr('disabled', true)
// 			$(this).find('.input_count_raw_material').attr('disabled', true)
// 		})
// 		console.log('checked')
// 	}
// })

/**
 * Выбрать все
 */
 $('.check_all').on('click', function(ev) {
 	if ($(this).prev().is(':checked')) {
		$(this).parent().parent().next('.table-responsive').find('.checkbox-raw-material').prop('checked', false)
 	} else {
 		$(this).parent().parent().next('.table-responsive').find('.checkbox-raw-material').prop('checked', true)
 	}
 })



/**
 * Сохранить объединение групп
 */
$('.join_groups_save').on('click', function(ev){
	ev.preventDefault()
	$('.mess').addClass('d-none')
	// полное объединение
	var consign_ids = []
	var chosen_groups = {}
	var raw_materials_ids = []
	if($('.check_all_in_group:checked').length > 1){
		$('.check_all_in_group:checked').each(function(){
			consign_ids.push($(this).attr('data-id'))
		})

		// остальные инпуты
		$('td.manufacture-form').each(function(){
			var checked2 = $(this).find('.checkbox-raw-material').is(':checked')
			var disabled1 = $(this).find('.checkbox-raw-material:checked').attr('disabled')
			if(checked2 == true &&  $(this).find('.checkbox-raw-material:checked').attr('disabled') != 'disabled'){
				var id1 = $(this).find('.checkbox-raw-material:checked').attr('data-id')
				var quant1 = $(this).find('.input-count-raw-material').val()
				if(id1 != undefined && quant1 > 0){
					chosen_groups[$(this).closest('.table').attr('data-consignment')] = 1
					var raw1 = {}
					raw1[id1] = quant1
					raw_materials_ids.push(raw1)
				}
			}
		})
	}else{
		$('td.manufacture-form').each(function(){
			var id1 = $(this).find('.checkbox-raw-material:checked').attr('data-id')
			var quant1 = $(this).find('.input-count-raw-material').val()
			if(id1 != undefined && quant1 > 0){
				chosen_groups[$(this).closest('.table').attr('data-consignment')] = 1
				var raw1 = {}
				raw1[id1] = quant1
				raw_materials_ids.push(raw1)
			}	
		})
	}
		console.log(consign_ids)
		console.log(raw_materials_ids)
		console.log(chosen_groups)
		if(Object.values(chosen_groups).length <= 1)
			raw_materials_ids = []
		// return
	if(consign_ids.length > 1 || raw_materials_ids.length > 1){
		var storage = $('.join_groups').attr('data-storage')
		$.post('/storage/joinGroups/', {
			'consign_ids': consign_ids, 
			'storage': storage, 
			'raw_materials_ids': raw_materials_ids
		})
		.done(function(resp){
			console.log(resp)
			location.reload()
		}).fail(function(err){
			console.log(err)
		})
	}else{
		$('.mess').removeClass('d-none').text('Должно быть выбрано не менее 2-х групп')
	}

})



/**
 * Отображение формы и блоков управления для перемещения товаров с склада Продукция на склад Покраска
 */
$(document).on('click', '.btn-show-form-send-products-to-repair', function () {
	const button = $(this);
	$('#block_send_products_to_repair').removeClass('d-none');
	$('.repair-form').removeClass('d-none');
	$('#title_form_send_products_to_repair').removeClass('d-none');
	$('#storage_block_control').addClass('d-none');

	$('.repair-form').find('.select2-active').select2({
		minimumResultsForSearch: -1
	});
});

/**
 * Скрытие формы и блоков управления для перемещения товаров с склада Сырья на склад Производство
 */
$(document).on('click', '.btn-hide-form-send-raw-materials-to-manufacture', function () {
	const button = $(this);
	$('.check_all_box').addClass('d-none')
	$('.check_all').removeClass('forSendToManufact')
	$('#block_send_raw_materials_to_manufacture').addClass('d-none');
	$('.manufacture-form').addClass('d-none');
	$('.summ-first-cell').addClass('d-none');
	$('#title_form_send_raw_material_to_manufacture').addClass('d-none');
	$('.btn-show-form-add-consignment').removeClass('d-none');
	$('#storage_block_control').removeClass('d-none');
	/*$('.btn-add-row-table-raw-material').removeClass('d-none');*/
	$('.btn-edit-table').removeClass('d-none');
});

/**
 * Скрытие формы и блоков управления для перемещения товаров с склада Продукция на склад Покраска
 */
$(document).on('click', '.btn-hide-form-send-products-to-repair', function () {
	const button = $(this);
	$('#block_send_products_to_repair').addClass('d-none');
	$('.repair-form').addClass('d-none');
	$('#title_form_send_products_to_repair').addClass('d-none');
	$('#storage_block_control').removeClass('d-none');

	$('.repair-form').find('.select2-active').select2('destroy');
});

/**
 * Подсчет количества выбранных позиций и товаров при изменение состояния чекбоксов и количество позиций в форме перемещения товаров по складам
 */
$(document).on('change, input', '.checkbox-raw-material, .input-count-raw-material', function () {

	const element = $(this);
	const input = element.closest('.form-inline').find('.input-count-raw-material');
	const select2 = element.closest('.form-inline').find('.select2-active');

	let counter = 0;
	let counter_count = 0;

	if (element.hasClass('checkbox-raw-material')) {
		if (element.prop("checked") === true) {
			input.removeAttr('disabled');
			select2.removeAttr('disabled');
		} else {
			input.attr('disabled', 'disabled');
			select2.attr('disabled', 'disabled');
		}
	}

	$('.checkbox-raw-material').each(function () {
		if ($(this).prop("checked") === true) {
			counter = counter + 1;
			counter_count = counter_count + parseInt($(this).closest('.form-inline').find('.input-count-raw-material').val());
		}
	});

	$('#counter_checked_raw_material').html(counter);
	$('#counter_count_checked_raw_material').html(counter_count);
  if (counter_count > 0) $('.btn-form-sendcheck').prop('disabled',false);
    else $('.btn-form-sendcheck').prop('disabled',true);
});

/**
 * Подсчет количества выбранных позиций "Выбрать все"
 * @param  {[type]} ){ 	if           ($(this).prev().is(':checked')) { 		var checked_all [description]
 * @return {[type]}     [description]
 */
$(document).on('click', '.check_all.forSendToManufact', function(){
 	if ($(this).prev().is(':checked')) {
 		var checked_all = true	
 	} else {
 		var checked_all = false	
 	}
	let counter = 0;
	let counter_count = 0;

	$(this).parent().parent().next('.table-responsive').find('.checkbox-raw-material').each(function(k, v){
		const element = $(v);
		// $(v).trigger('change')
		const input = element.closest('.form-inline').find('.input-count-raw-material');
		const select2 = element.closest('.form-inline').find('.select2-active');
		if (element.hasClass('checkbox-raw-material')) {
			if (checked_all === true) {
				input.removeAttr('disabled');
				select2.removeAttr('disabled');
			} else {
				input.attr('disabled', 'disabled');
				select2.attr('disabled', 'disabled');
			}
		}
	})
		$('.checkbox-raw-material').each(function () {
			if ($(this).prop("checked") === true) {
				counter = counter + 1;
				counter_count = counter_count + parseInt($(this).closest('.form-inline').find('.input-count-raw-material').val());
			}
		});
	$('#counter_checked_raw_material').html(counter);
	$('#counter_count_checked_raw_material').html(counter_count);
	  if (counter_count > 0) $('.btn-form-sendcheck').prop('disabled',false);
	    else $('.btn-form-sendcheck').prop('disabled',true);
})

// add
/*$(document).on('change, input, keyup, keydown', '.input-form-quantity, .input-form-thickness, .input-form-width, .input-form-length', function () {
	calc_volume_m3($(this), '.row', '.input-form-');
});
*/
$(document).on('input', '.input-form-quantity, .input-form-thickness, .input-form-width, .input-form-length', function () {
	calc_volume_m3($(this), '.row', '.input-form-');
});
$(document).on('keyup', '.input-form-quantity, .input-form-thickness, .input-form-width, .input-form-length', function () {
	calc_volume_m3($(this), '.row', '.input-form-');
});
$(document).on('keydown', '.input-form-quantity, .input-form-thickness, .input-form-width, .input-form-length', function () {
	calc_volume_m3($(this), '.row', '.input-form-');
});
$(document).on('change', '.input-form-quantity, .input-form-thickness, .input-form-width, .input-form-length', function () {
	calc_volume_m3($(this), '.row', '.input-form-');
});

// edit
// $(document).on('change, input, keyup, keydown', '.input-consignment-quantity, .input-consignment-thickness, .input-consignment-width, .input-consignment-length', function () {
// 	calc_volume_m3($(this), 'tr', '.input-consignment-');
// });

$(document).on('input', '.input-consignment-quantity, .input-consignment-thickness, .input-consignment-width, .input-consignment-length', function () {
	calc_volume_m3($(this), 'tr', '.input-consignment-');
});
$(document).on('keyup', '.input-consignment-quantity, .input-consignment-thickness, .input-consignment-width, .input-consignment-length', function () {
	calc_volume_m3($(this), 'tr', '.input-consignment-');
});
$(document).on('keydown', '.input-consignment-quantity, .input-consignment-thickness, .input-consignment-width, .input-consignment-length', function () {
	calc_volume_m3($(this), 'tr', '.input-consignment-');
});
$(document).on('change', '.input-consignment-quantity, .input-consignment-thickness, .input-consignment-width, .input-consignment-length', function () {
	calc_volume_m3($(this), 'tr', '.input-consignment-');
});


/**
 * Расчет объема товара при изменение полей "Количество", "Толщина", "Ширина", "Длина"
 */
function calc_volume_m3(el, parent, children) {
	const element = $(el);
	const quantity = element.closest(parent).find(children + 'quantity').val();
	const thickness = element.closest(parent).find(children + 'thickness').val() / 1000;
	const width = element.closest(parent).find(children + 'width').val() / 1000;
	const length = element.closest(parent).find(children + 'length').val() / 1000;

	const volume3 = (quantity * thickness * width * length).toFixed(3);
	const volume_v2 = (quantity * width * length).toFixed(3);
	const pogonni_metr = (quantity * length).toFixed(3);

	// element.closest(parent).find(children + 'volume-m3').val(volume3);
	element.closest(parent).find('.volume_m3').text(volume3);
	element.closest(parent).find('.volume_m2').text(volume_v2);
	element.closest(parent).find('.pogonni-metr').text(pogonni_metr);
}

/**
 * На покраску
 */
$(document).on('change', '#send-to-storage-select', function () {
  var storageSelect = $(this).val();
	$('#send-to-storage-consignment-select option').each(function () {
    if ($(this).data('storage')!=storageSelect) $(this).hide();
    if ($(this).data('storage')==storageSelect) $(this).show();
    if ($(this).data('storage')=='new') {
      $(this).show();
      $('#send-to-storage-consignment-select').val($(this).data('storage'));
    }
  });
  if (storageSelect == 4) {
    $('.btn-form-send-raw-materials-tocolor').removeClass('d-none');
    $('.btn-form-sendcheck').addClass('d-none')
    $('#block_send_raw_materials_to_manufacture .btn-form-send-raw-materials-to-manufacture').addClass('d-none');
  } else {
    $('.btn-form-sendcheck').removeClass('d-none')
    $('.btn-form-send-raw-materials-tocolor').addClass('d-none');
    $('#block_send_raw_materials_to_manufacture .btn-form-send-raw-materials-to-manufacture').removeClass('d-none');
  }
});
$(document).on('click', '.btn-form-send-raw-materials-tocolor', function () {
    $('.modal-form-tocolor-material').modal('show');
});

/**
 * Отправка формы перемещения товаров с склада Сырье на склад Производство
 */
$(document).on('click', '.btn-form-sendcheck', function () {
  	var count = 0;
  	$( ".table-sendcheck" ).html(' ');
	$('.checkbox-raw-material').each(function () {
		if ($(this).prop("checked") === true) {
			// var html = $(this).closest('tr').outerHTML;
	      // console.log(html);
	      if (count == 0) {
	        $( ".table-sendcheck" ).append( $(this).closest('table').find('thead tr').clone() );
	        count = 1;
	      }
	      $( ".table-sendcheck" ).append( $(this).closest('tr').clone() );
		}
	});
	// таблица в модалке
	$('.table-sendcheck .input-consignment-quantity').each(function () {
	    var tr = $(this).closest('tr');
	    var td = $(this).closest('td');
	    var count = parseFloat(tr.find('.input-count-raw-material').val());
	    // console.log(count)
	    td.find('.edit-value').html(count);
	    var tolshina = parseFloat(tr.find('.input-consignment-thickness').prev().text().replace(/\s/, ''))
	    tr.find('.input-consignment-thickness').prev().text(tolshina * count)
	    var width1 = parseFloat(tr.find('.input-consignment-width').prev().text().replace(/\s/, ''))
	    tr.find('.input-consignment-width').prev().text(width1 * count)
	    var length1 = parseFloat(tr.find('.input-consignment-length').prev().text().replace(/\s/, ''))
	    tr.find('.input-consignment-length').prev().text(length1 * count)
	    
	    var volume2 = parseFloat(tr.find('.volume_m2').attr('data-volume-m2-x1'))
	    // console.log(volume2)
	    tr.find('.volume_m2').text(volume2 * count)
	    var volume3 = parseFloat(tr.find('.volume_m3').attr('data-volume-m3-x1'))
	    // console.log(volume3)
	    tr.find('.volume_m3').text(volume3 * count)
	    // погонный метр
	    tr.find('.pogonni-metr').text( (length1/1000 * count) )
	  });
	  $( ".table-sendcheck" ).find( '.td-4-btn' ).addClass('d-none');
	  $( ".table-sendcheck" ).find( '.manufacture-form' ).addClass('d-none');
	  $( ".table-sendcheck" ).find( '.manufacture-form' ).html(' ');
	  $('.modal-form-sendcheck-material').modal('show');
});

// окончательная отправка / на покраску
$(document).on('click', '.btn-form-send-raw-materials-to-manufacture', function (ev) {
	ev.preventDefault()
	const button = $(this);
	const date = $('#date_manufacture');
	let arr = [];
	// Выбрано позиций
	let counter_checked_raw_material = parseInt($('#counter_checked_raw_material').text());
	// Указано товаров
	let counter_count_checked_raw_material = parseInt($('#counter_count_checked_raw_material').text());

	// Куда:
	  var storageSelect = $('#send-to-storage-select').val();
	  // Группа:
	  var consignmentSelect = $('#send-to-storage-consignment-select').val();
	  console.log(consignmentSelect);
  
  /* На покраску */
	let tocolor = [];
	  if (storageSelect == 4) {
	    $('#tocolor_products select').each(function(){
	      var name = $(this).attr('name');
	      var val = $(this).val();
				let arr2 = [];
				arr2.push(name);
				arr2.push(val);
				tocolor.push(arr2);
	    });
	    if($('#tocolor_products .tocolor_zakazchik').length){
	    	tocolor.push([
	    		'zakazchik',   
	    		$('#tocolor_products .tocolor_zakazchik').val() 
	    		]);
	    }
	  }
	// console.log('tocolor array')  
	// console.log(tocolor)  
	// throw new Error("end script");
	  
  
	button_loading(button);

	$('.checkbox-raw-material').each(function () {
		if ($(this).prop("checked") === true) {
			let arr2 = [];
			arr2.push($(this).data('id'));
			// тупо берем id и кол-во материала
			arr2.push($(this).closest('.form-inline').find('.input-count-raw-material').val());
			arr.push(arr2);
		}
	});

	date.removeClass('is-invalid');

	if (counter_checked_raw_material > 0 && counter_count_checked_raw_material > 0) {
		var data1 = {
				array: arr, 
				tocolor: tocolor, 
				date: date.val(), 
				storage: storageSelect, 
				consignment: consignmentSelect, 
				breed: $('#breed_active').val(), 
				tint: $('#tint_active').val()
			}
		console.log(data1)	
		$.ajax({
			url: '/storage/SendRawMaterialsToManufacture/',
			type: 'POST',
			data: data1,
			success: function (data) {

				const answer = data.split(';');
				const success = answer[0].split('::');
				const error = answer[0].split(':');

				button_init(button);

				if (error[0] === 'error' && error[1] !== null) {
					if (error[1] === 'date') {
						date.addClass('is-invalid');
					}
				} else if (success[0] === 'success') {
					button_success(button);
					button.prop('disabled', true);
					setTimeout(function () {
						location.reload();
					}, 1500);
				}
			}
		});
	} else {
		button_init(button);
	}
});

/**
 * Отправка формы перемещения товаров с склада Продукция на склад Покраска
 */
$(document).on('click', '.btn-form-send-products-to-repair', function () {
	const button = $(this);
	let arr = [];
	let counter_checked_raw_material = parseInt($('#counter_checked_raw_material').text());
	let counter_count_checked_raw_material = parseInt($('#counter_count_checked_raw_material').text());

	button_loading(button);

	$('.checkbox-raw-material').each(function () {
		if ($(this).prop("checked") === true) {
			let arr2 = [];
			arr2.push($(this).data('id'));
			arr2.push($(this).closest('.form-inline').find('.input-count-raw-material').val());
			arr2.push($(this).closest('.form-inline').find('.select2-active').val());
			arr.push(arr2);
		}
	});

	if (counter_checked_raw_material > 0 && counter_count_checked_raw_material > 0) {
		$.ajax({
			url: '/storage/SendProductsToRepair/',
			type: 'POST',
			data: {array: arr, breed: $('#breed_active').val(), tint: $('#tint_active').val()},
			success: function (data) {
				const answer = data.split(';');
				const success = answer[0].split('::');
				const error = answer[0].split(':');

				button_init(button);

				if (error[0] === 'error' && error[1] !== null) {

				} else if (success[0] === 'success') {
					button_success(button);
					button.prop('disabled', true);
					setTimeout(function () {
						location.reload();
					}, 1500);
				}
			}
		});
	} else {
		button_init(button);
	}
});

/**
 * Отображение поп-апа с формой для создания 
 * конечных товаров для склада Продукция из партии сырья 
 * на складе Производство (old version)
 */
$(document).on('click', '.btn-show-form-success-manufacture-raw-materials', function () {
	$('.modal-form-success-manufacture-raw-material').modal('show');
	$('#consignment_id_success').val($(this).data('id'));
});

/**
 * Отправка формы создания конечных товаров из сырья
 */
$(document).on('click', '.btn-add-consignment-products, .btn-add-consignment-products-paint', function () {
	const button = $(this);

	button_loading(button);

	let error = 0;
	button.closest('form').find('.form-control').each(function () {
		if ($(this).prop('required')) {
			$(this).removeClass('is-invalid');
			if ($(this).val() === null || $(this).val() === '0' || $(this).val() === '0.000' || $(this).val() === 0 || $(this).val().length === 0) {
				$(this).addClass('is-invalid');
				error++;
			}
		}
	});

	if (error === 0) {
		const formData = new FormData(button.closest('form')[0]);
		if($(this).hasClass("btn-add-consignment-products-paint")){
            formData.append("paint", "y");
        }
		$.ajax({
			url: '/storage/AddConsignmentProducts/',
			type: 'POST',
			processData: false,
			contentType: false,
			data: formData,
			success: function (data) {
				const answer = data.split(';');
				const success = answer[0].split('::');
				const error = answer[0].split(':');

				if (error[0] === 'error' && error[1] !== null) {
					const input_error = $('input[name="' + error[1] + '"]');
					input_error.addClass('is-invalid');
				} else if (success[0] === 'success') {
					button_success(button);
					button.prop('disabled', true);
					setTimeout(function () {
						location.reload();
					}, 1500);
				}
			}
		});
	} else {
		button_init(button);
	}
	return false;
});

/**
 * Изменение статуса покраски посредством выбора значения в выпадающем списке
 */
$(document).on('change', '.select_status_consigment', function () {
	const select = $(this);

	$.ajax({
		url: '/storage/UpdateStatusConsignment/',
		type: 'POST',
		data: {id: select.data('id'), status: select.val()},
		success: function (data) {
		}
	});
});

/**
 * Отображение формы создания новой сделки
 */
$(document).on('click', '.btn-show-form-add-sale', function () {
	const btn = $(this);
	btn.addClass('d-none');
	$('form[name="add_sale"]').removeClass('d-none');
});

/**
 * Скрытие формы создания новой сделки
 */
$(document).on('click', '.btn-hide-form-add-sale', function () {
	const btn = $(this);
	$('.btn-show-form-add-sale').removeClass('d-none');
	$('form[name="add_sale"]').addClass('d-none');
});

$(document).on('click', '.btn-delete-consignment', function () {
	if (confirm('Вы уверены?')) {
		const btn = $(this);

		$.ajax({
			url: '/storage/DeleteConsignment/',
			type: 'POST',
			data: {id: btn.data('id')},
			success: function (data) {
				location.reload();
			}
		});
	}
});

// кнопка копирования при добавлении группы
$(document).on('click', '.btn-copy-raw-material', function () {
	const btn = $(this);
	btn.closest('.row').find('.touchspin').trigger('touchspin.destroy');
	const element = btn.closest('.row').clone();
	ActiveTouchSpin(btn.closest('.row').find('.touchspin'));
	ActiveTouchSpin(element.find(".touchspin"));
	// console.log(btn.closest('.row').find('.breed-select'))
	var name = btn.closest('.row').find('.name-select').val();
	var breed = btn.closest('.row').find('.breed-select').val();
	var profile = btn.closest('.row').find('.profile-select').val();
	element.find('select.name-select').val(name);
	element.find('select.breed-select').val(breed);
	element.find('select.profile-select').val(profile);
	// element.find('span.select2').select2("destroy");
	element.find('span.select2').remove();
	element.find('select.select2').removeAttr('data-select2-id')
	element.find('select.select2').select2()
	element.find('.input-form-quantity, .input-form-thickness, .input-form-width, .input-form-length').change(function () {
		calc_volume_m3($(this), '.row', '.input-form-');
	});
	element.insertAfter(btn.closest('.row'))
});

// копирование при редактировании
$(document).on('click', '.btn-copy-raw-material-inedit', function(ev){
	ev.preventDefault();
	const btn = $(this);
	const rand = Math.floor(Math.random() * Math.floor(999999999));
	btn.closest('tr').find('.touchspin').trigger('touchspin.destroy');
	const element = btn.closest('tr').clone();
	/*const newSelects = element.find('select.select2');
	newSelects.each(function() {
		$(this).attr('data-select2-id', 'select2-data-' + Math.floor(Math.random() * Math.floor(999999999)))
	})*/ 
	element.find('.checkbox-raw-material').attr('data-id', rand);
	ActiveTouchSpin(btn.closest('tr').find('.touchspin'));
	ActiveTouchSpin(element.find(".touchspin"));
	
	var cort = btn.closest('tr').find('.cort-select').val();
	var breed = btn.closest('tr').find('.breed-select').val();
	var profile = btn.closest('tr').find('.profile-select').val();
	element.find('select.cort-select').val(cort);
	element.find('select.breed-select').val(breed);
	element.find('select.profile-select').val(profile);

	element.find('select.select2').removeAttr('data-select2-id');
	element.find('span.select2').remove();
	element.find('select.select2').select2();

	element.find('.input-consignment-quantity, .input-consignment-thickness, .input-consignment-width, .input-consignment-length').change(function () {
		calc_volume_m3($(this), 'tr', '.input-consignment-')
	});
	element.insertAfter(btn.closest('tr'))
});


// описание партии
$(document).on('click', '.show-form-update-description-consignment', function () {
	const btn = $(this);
	btn.addClass('d-none');
	btn.closest('div').find('.form-update-description-consignment').removeClass('d-none');
	btn.closest('div').find('input').focus();
	return false;
});

// описание партии сохранить
$(document).on('click', '.btn-update-description-consignment', function () {
	const btn = $(this);
	btn.closest('.row').addClass('d-none');

	let val = btn.closest('form').find('textarea').val();
	if (val.length < 1) val = btn.closest('.card-box').find('.show-form-update-description-consignment').data('text');
	btn.closest('.card-box').find('.show-form-update-description-consignment').removeClass('d-none').text(val);

	const formData = new FormData(btn.closest('form')[0]);
	formData.append('id', btn.data('id'));
	$.ajax({
		url: '/storage/UpdateDescriptionConsignment/',
		type: 'POST',
		processData: false,
		contentType: false,
		data: formData,
		success: function (data) {
		}
	});
	return false;
});

// добавить новую строку
window.new_row = [];
$(document).on('click', '.btn-add-row-table-raw-material', function () {
	
	const btn = $(this);
	const rand = Math.floor(Math.random() * Math.floor(999999999));
	/*const tbody = btn.closest('.card-box').find('tbody');
	const element = $('#template_row_raw_material').clone().appendTo(tbody).removeClass('d-none').removeAttr('id');*/
	const tbody = btn.closest('.card-box').find('.tr-summ');
	const element = $('#template_row_raw_material').clone().insertBefore(tbody).removeClass('d-none').removeAttr('id').attr('id', 'new_row_' + rand);;
	ActiveTouchSpin(element.find(".touchspin-input"));
	// element.find(".select2").select2('destroy')



	element.find(".select2").select2({
		// minimumResultsForSearch: -1
        tags: true,
        selectOnClose: true,
        // dropdownParent: $('.modal-form-tocolor-material')
	});



/*	$(".profile-select, .breed-select").select2('destroy')
	$(".profile-select, .breed-select").select2({
        tags: true,
          selectOnClose: true,
    });*/
    // selectRefresh();
	element.find('.checkbox-raw-material').removeAttr('id').attr('id', 'input_checkbox_' + rand);
	element.find('.checkbox-raw-material').closest('div').find('label').removeAttr('for').attr('for', 'input_checkbox_' + rand);
	element.find('.input-consignment-quantity, .input-consignment-thickness, .input-consignment-width, .input-consignment-length').change(function () {
		calc_volume_m3($(this), 'tr', '.input-consignment-');
	});
	element.find('[name="input_name"]').addClass('_req'); // добавление сласса "_req" для валидации инпутов функцией 
	element.find('[name="input_quantity"]').addClass('_req'); // добавление сласса "_req" для валидации инпутов функцией 

	//удаление лишних ячеек при добавлении строки
	let selectsCount = element.find('.select-td:first').find('.select2-container--default').length
	if (selectsCount > 2) {
		element.find('.select-td').find('.select2-container--default:last').remove();
	}
	

	window.new_row.push($('#new_row_' + rand))
});
/**
 * Создание и обновление сделки
 */
$(document).on('click', '.btn-add-sale, .btn-update-sale', function () {
    const button = $(this);
    let arr = [];
    let counter_checked_raw_material = 0;
    let form = button.closest('form');
    button_loading(button);
	let url ='/sales/Add/';
	if(button.hasClass('btn-update-sale'))
		url ='/sales/Update/';
    let error = 0;
    form.find('.form-control').each(function () {
        if ($(this).prop('required')) {
            $(this).removeClass('is-invalid');
            if ($(this).val() === null || $(this).val() === '0' || $(this).val() === '0.000' || $(this).val() === 0 || $(this).val().length === 0) {
                $(this).addClass('is-invalid');
                error++;
            }
        }
    });

    form.find('.checkbox-raw-material').each(function () {
        if ($(this).prop("checked") === true) {
            counter_checked_raw_material = counter_checked_raw_material + 1;
            let arr2 = [];
            arr2.push($(this).data('id'));
            arr2.push($(this).closest('.form-inline').find('.input-count-raw-material').val());
            arr2.push($(this).closest('.form-inline').find('.select2-active').val());
            arr.push(arr2);
        }
    });
	console.log(arr);
    if (error === 0 && counter_checked_raw_material > 0) {
        $.ajax({
            url: url,
            type: 'POST',
            data: {array: arr, id: form.data("id"), contract: form.find('input[name="contract"]').val(), fio: form.find('input[name="fio"]').val(), type: form.find('select[name="type"]').val(), cost: form.find('input[name="cost"]').val(), prepaid: form.find('input[name="prepaid"]').val(), payment_type: form.find(' select[name="payment_type"]').val(), status: form.find('select[name="status"]').val()},
            success: function (data) {
                console.log(data);
                const answer = data.split(';');
                const success = answer[0].split('::');
                const error = answer[0].split(':');

                button_init(button);

                if (error[0] === 'error' && error[1] !== null) {
                    const input_error = form.find('input[name="' + error[1] + '"]');
                    input_error.addClass('is-invalid');
                } else if (success[0] === 'success') {
                    button_success(button);
                    button.prop('disabled', true);
                    setTimeout(function () {
                        location.reload();
                    }, 1500);
                }
            }
        });
    } else {
        button_init(button);
    }
    return false;
});
/**
 * Удаление сделки
 */
$(document).on('click', '.btn-delete-sale', function () {
    if (confirm('Вы уверены?')) {
        const btn = $(this);

        $.ajax({
            url: '/sales/Delete/',
            type: 'POST',
            data: {id: btn.data('id')},
            success: function (data) {
                location.reload();
            }
        });
    }
});
/**
 * Изменение статуса сделки
 */
$(document).on('change', '.select_status_sale', function () {
    const select = $(this);
	if(select.data('id') != undefined){
        $.ajax({
            url: '/sales/UpdateStatusSale/',
            type: 'POST',
            data: {id: select.data('id'), status: select.val()},
            success: function (data) {

            }
        });
	}
});
/**
 * Скрытие и показ формы обновления сделки
 */
$(document).on('click', '.btn-hide-form-update-sale', function () {
    const button = $(this);
    let form = button.closest('form');
    form.addClass('d-none');
    $('.sale-card-box-'+form.data("id")).removeClass('d-none');
});

$(document).on('click', '.btn-edit-sale', function () {
    const button = $(this);
    $('.sale-card-box-'+button.data("id")).addClass('d-none');
    $('.form-update-sale-'+button.data("id")).removeClass('d-none');
});

/**
  * Сохдание пользователя, редактирование и удаление
 */
$(document).on('click', '.new-user-btn', function () {
    $(".save-user").data("id","");
    $(".password-control").attr("required","required");
    $(".name-control").val("");
    $(".surname-control").val("");
    $(".patronymic-control").val("");
    $(".email-control").val("");
    $(".phone-control").val("");
    $('.type-select').val(1).trigger('change');
    button_init(".save-user");
});
$(document).on('click', '.edit-user', function () {
    $(".save-user").data("id",$(this).data("id"));
    $(".name-control").val($(this).data("name"));
    $(".surname-control").val($(this).data("surname"));
    $(".patronymic-control").val($(this).data("patronymic"));
    $(".email-control").val($(this).data("email"));
    $(".phone-control").val($(this).data("phone"));
    $('.type-select').val($(this).data("type")).trigger('change');
    $(".password-control").removeAttr("required");
    button_init(".save-user");
});
$(document).on('click', ".save-user", function () {
    const button = $(this);
    button_loading(button);

    let error = 0;
    button.closest('form').find('.form-control').each(function () {
        if ($(this).prop('required')) {
            $(this).removeClass('is-invalid');
            if ($(this).val() === null || $(this).val() === '0' || $(this).val() === '0.000' || $(this).val() === 0 || $(this).val().length === 0) {
                $(this).addClass('is-invalid');
                error++;
            }
        }
    });

    if (error === 0 && button.data("id") == "") {
        const formData = new FormData(button.closest('form')[0]);

        $.ajax({
            url: '/users/Add/',
            type: 'POST',
            processData: false,
            contentType: false,
            data: formData,
            success: function (data) {
            	console.log(data);
                const answer = data.split(';');
                const success = answer[0].split('::');
                const error = answer[0].split(':');
				button_init(button);
                if (error[0] === 'error' && error[1] !== null) {
                    const input_error = $('input[name="' + error[1] + '"]');
                    input_error.addClass('is-invalid');
                } else if (success[0] === 'success') {
                    button_success(button);
                    button.prop('disabled', true);
                    setTimeout(function () {
                       location.reload();
                    }, 1500);
                }
            }
        });
    }
    else if(error === 0 && button.data("id") > 0){
        const formData = new FormData(button.closest('form')[0]);
        formData.append('id', button.data('id'));
        $.ajax({
            url: '/users/Update/',
            type: 'POST',
            processData: false,
            contentType: false,
            data: formData,
            success: function (data) {
                console.log(data);
				button_init(button);
                const answer = data.split(';');
                const success = answer[0].split('::');
                const error = answer[0].split(':');

                if (error[0] === 'error' && error[1] !== null) {
                    const input_error = $('input[name="' + error[1] + '"]');
                    input_error.addClass('is-invalid');
                } else if (success[0] === 'success') {
                    button_success(button);
                    button.prop('disabled', true);
                    setTimeout(function () {
                        location.reload();
                    }, 1500);
                }
            }
        });
	}
    else {
        button_init(button);
    }
    return false;
});
$(document).on('click', '.delete-user', function () {
    if (confirm('Вы уверены?')) {
        const btn = $(this);

        $.ajax({
            url: '/users/Delete/',
            type: 'POST',
            data: {id: btn.data('id')},
            success: function (data) {
                location.reload();
            }
        });
    }
});

//  Справочники
$(function(){
/*	$('.btn-delete-guide').on('click', function(ev){
		ev.preventDefault()
		var that = $(this)
		var name =  $(this).attr('data-name')
		var id =  $(this).attr('data-id')
		$.post('/guides/edit', {'id': id, 'name': name, 'action': 'del'}).done(function(resp){
			console.log(resp)
			if(resp == 'good'){
				that.parent().remove()
			}
		}).fail(function(err){
			console.log(err)
		})
	})

	$('.btn-edit-guide').on('click', function(ev){
		ev.preventDefault()
		$(this).parent().find('.edit-block').toggle()
	})

	$('.btn-submit-edit-guide').on('click', function(ev){
		ev.preventDefault()
		var that = $(this)
		var type =  $(this).attr('data-name')
		var id =  $(this).attr('data-id')
		var new_val =  $(this).parent().find('.new_val').val()
		$.post('/guides/edit', {'id': id, 'name': type, 'action': 'edit', 'new_val': new_val}).done(function(resp){
			console.log(resp)
			if(resp == 'good'){
				that.parent().parent().find('span').text(new_val)
				that.parent().hide()
			}
		}).fail(function(err){
			console.log(err)
		})
	})
*/
	// ============ add guides
/*	$('.btn-add-guide').on('click', function(){
		var type =  $(this).attr('data-type')
		var new_val =  $(this).prev().val()
		var that = $(this)
		$.post('/guides/add', {'table': type, 'name': new_val}).done(function(resp){
			that.prev().val('')
			that.parent().next().append('<li>'+new_val+'</li>')
		})
	})
*/
	// $('.table').DataTable({
 	//    	searching: false,
 	//    	"order": [],
 	//    	"columnDefs": [
	// 	    { "orderable": false, "targets": [10] }
	// 	  ],
	// 	"paging" : false,
	// 	"info": false,
	// 	dom: 'Bfrtip',
 	//        buttons: [
	//         {
	//             extend: 'collection',
	//             text: 'Экспорт',
	//             className: 'export-btn',
	//             background: false,
	//             buttons: [ 'copy', 'csv', 'excel', 'pdf', 'print' ]
	//         }
	// 	],
 	//        "orderFixed": [ 0, 'desc' ]
 	//    });


function changeExportCols( data, row, column, node ) {
	var res1 = node.querySelector( '.edit-value' );
	if(res1 === null && [1,2,3].indexOf(column) < 0)
		return data
	else if(res1 === null && [1,2,3].indexOf(column) >= 0 )
		return ''
	return res1.innerHTML
}

if($('.deals-table').length > 0 ){
	var expOpts = {
		            // columns: ':not(.d-none, option, .btn)',
		            columns:  [2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26],
		            /*format: {
		                body: changeExportCols
		            }	 */           		
		    	}
}else{
	var expOpts = {
		            // columns: ':not(.d-none, option, .btn)',
		            columns: [1,2,3,4,5,6,7,8,9,10],
		            format: {
		                body: changeExportCols
		            }	            		
		    	}

}


// Экспорт 
 	$('.table').DataTable({
 		  "bSort": true,
    	searching: false,
    	// "order": [ [ 7, "asc" ] ],
    	"columnDefs": [
		    { "orderable": false, "targets": [11] }
		  ],
		"paging" : false,
		"info": false,
		dom: 'Bfrtip',
        buttons: [
	        {
	            extend: 'collection',
	            text: 'Экспорт',
	            className: 'export-btn',
	            background: false,
	            buttons: [ 
	            // 'copy', 
	            // 'csv', 
	            // 'print' , 
	            // 'excel', 
	            // 'pdf', 
	            // 'pdfHtml5', 
	            {
	            	extend: 'csv',
	            	exportOptions: expOpts
	            },
	            {
	            	extend: 'excel',
	            	exportOptions: expOpts
	            },
	            {
	            	extend: 'pdfHtml5', 
	            	// customize: function ( doc ) {
	            	// 	var a1 = doc
	            	// 	console.log(doc)
	            	// 	// alert(doc.content[1].table.body[1][1].text)
	            	// 	// console.log(doc.content[1].table.body[1][1].text)
	            	// 	for(let i=1;i< doc.content[1].table.body.length;i++){
		            // 		doc.content[1].table.body[i][1].text = doc.content[1].table.body[i][1].text.split(/\s{2,}/)[0]
		            // 		doc.content[1].table.body[i][2].text = doc.content[1].table.body[i][2].text.split(/\s{2,}/)[0]
		            // 		doc.content[1].table.body[i][3].text = doc.content[1].table.body[i][3].text.split(/\s{2,}/)[0]
	            	// 	}
	            	// }
        	        exportOptions: expOpts
	        	} 
	            ],
	        },
	        {

	        }
		],
        "orderFixed": [ 0, 'desc' ]
    });



})

