/*jslint browser: true*/
/*global $, jQuery, alert*/
function sleep(milliseconds) {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < milliseconds);
}
//Авторизация
$(document).on('click', '.btn-signin', function (ev) {
	ev.preventDefault()
	const button = $(this);
	button.prop('disabled', true);
	button.find('.button-text').css('display', 'none');
	button.find('.button-wait').css('display', 'block');

	let error = 0;
	button.closest('form').find('.form-control').each(function () {
		if ($(this).prop('required')) {
			$(this).removeClass('is-invalid');
			if ($(this).val().length === 0) {
				$(this).addClass('is-invalid');
				error++;
			}
		}
	});


	if (error === 0) {
		const formData = new FormData($('#auth_form')[0]);
		// setTimeout(function(){
		sleep(1000);
		$.ajax({
			url: '/login/CheckForm/',
			type: 'POST',
			processData: false,
			contentType: false,
			data: formData,
			success: function (data) {
				console.log(data);
				var answer = data.split(';');
				var success = answer[0].split(':');
				var error = answer[0].split(':');

				button.prop('disabled', false);
				button.find('.button-text').css('display', 'block');
				button.find('.button-wait').css('display', 'none');

				$('.form-control').each(function () {
					if ($(this).prop('required')) {
						$(this).removeClass('is-invalid');
					}
				});

				if (error[0] === 'error' && error[1] !== null) {
					if (error[1] === 'auth') {
						button.closest('form').find('.form-control').each(function () {
							if ($(this).prop('required')) {
								$(this).addClass('is-invalid');
							}
						});
					} else {
						$('input[name="' + error[1] + '"]').addClass('is-invalid');
					}
				} else if (success[0] === 'success') {
					button.prop('disabled', true);
					button.find('.button-text').css('display', 'none');
					button.find('.button-wait').css('display', 'block');

					if(success[1] == 3){
                        window.location.href = '/storage/5/';
					}
					else if(success[1] == 4){
                        window.location.href = '/storage/4/';
					}
					else{
                        window.location.href = '/storage/9/';
					}
				}
			}
		});
		// }, 100)
	} else {
		button.prop('disabled', false);
		button.find('.button-text').css('display', 'block');
		button.find('.button-wait').css('display', 'none');
	}
	
});


console.log(111111111)