 	// **************************edit del add
$(function(){
 	$('.edit-guide').on('click', function(){
 		$(this).closest('.list-group-item').find('.edit-inp').removeAttr('disabled')
 		$(this).parent().addClass('d-none')
 		$(this).closest('.list-group-item').find('.edit-mode').removeClass('d-none')
 	})

	$('.edit-cancel').on('click', function(){
 		$(this).parent().parent().find('.edit-inp').attr('disabled', true)
 		$(this).parent().addClass('d-none')
 		$(this).parent().prev().removeClass('d-none')
	    
    })    

    $('.confirm-edit').on('click', function(){
		$(this).parent().parent().find('.edit-inp').removeClass('is-invalid')
    	var that = $(this),
		type =  $(this).attr('data-name'),
		id =  $(this).attr('data-id'),
		new_val =  $(this).parent().parent().find('.edit-inp').val()
		if(new_val == ''){
			$(this).parent().parent().find('.edit-inp').addClass('is-invalid')
			return;
		}
		$.post('/guides/edit', {'id': id, 'name': type, 'action': 'edit', 'new_val': new_val}).done(function(resp){
			console.log(resp)
			if(resp == 'good'){
		 		that.parent().parent().find('.edit-inp').attr('disabled', true)
		 		that.parent().addClass('d-none')
		 		that.parent().prev().removeClass('d-none')
			}
		}).fail(function(err){
			console.log(err)
		})
    })


    $('.del-guide').on('click', function(){
    	$(this).closest('.list-group-item').find('.del-mode').removeClass('d-none')
    	$(this).parent().addClass('d-none')
    })

    $('.cancel-del').on('click', function(){
    	$(this).parent().addClass('d-none')
    	$(this).closest('.list-group-item').find('.primary-mode').removeClass('d-none')
    })

    $('.confirm-del').on('click', function(){
		var that = $(this)
		var name =  $(this).attr('data-name')
		var id =  $(this).attr('data-id')
		$.post('/guides/edit', {'id': id, 'name': name, 'action': 'del'}).done(function(resp){
			console.log(resp)
			if(resp == 'good'){
				that.closest('.list-group-item').remove()
			}
		}).fail(function(err){
			console.log(err)
		})    	
    })

    	// ============ add guides
	$('.add-guide').on('click', function(){
		var type =  $(this).attr('data-type'),
		new_val =  $(this).parent().prev().val(),
		that = $(this)
		if(new_val == false){
			$(this).parent().prev().addClass('is-invalid')
			return;
		}
		$.post('/guides/add', {'table': type, 'name': new_val}).done(function(resp){
			location.reload()
		})
	})

})

