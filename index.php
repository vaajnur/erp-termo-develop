<?php

use \StandartRus\App\Start;

require_once 'config.php'; //подключаем файл конфигурации

spl_autoload_register(function ($class) {
	$prefix = 'StandartRus\\App\\';
	$base_dir = __DIR__ . '/app/';
	$len = strlen($prefix);
	if (strncmp($prefix, $class, $len) !== 0) {
		return;
	}
	$relative_class = substr($class, $len);
	$file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';
	if (file_exists($file)) {
		require $file;
	}
});

/**
 * Libraries
 */

//PHPMailer
spl_autoload_register(function ($class) {
	$prefix = 'PHPMailer\\PHPMailer\\';
	$base_dir = __DIR__ . '/vendor/PHPMailer/';
	$len = strlen($prefix);
	if (strncmp($prefix, $class, $len) !== 0) {
		return;
	}
	$relative_class = substr($class, $len);
	$file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';
	if (file_exists($file)) {
		require $file;
	}
});

//SMSRU
spl_autoload_register(function ($class) {
	$prefix = 'SMSRU\\SMSRU\\';
	$base_dir = __DIR__ . '/vendor/SMSRU/';
	$len = strlen($prefix);
	if (strncmp($prefix, $class, $len) !== 0) {
		return;
	}
	$relative_class = substr($class, $len);
	$file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';
	if (file_exists($file)) {
		require $file;
	}
});

//FPDI
spl_autoload_register(function ($class) {
	$prefix = 'setasign\\Fpdi';
	$base_dir = __DIR__ . '/vendor/FPDI/';
	$len = strlen($prefix);
	if (strncmp($prefix, $class, $len) !== 0) {
		return;
	}
	$relative_class = substr($class, $len);
	$file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';
	if (file_exists($file)) {
		require $file;
	}
});

//Psr
spl_autoload_register(function ($class) {
	$prefix = 'Psr\\Log';
	$base_dir = __DIR__ . '/vendor/Psr/Log/';
	$len = strlen($prefix);
	if (strncmp($prefix, $class, $len) !== 0) {
		return;
	}
	$relative_class = substr($class, $len);
	$file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';
	if (file_exists($file)) {
		require $file;
	}
});

//mPDF
spl_autoload_register(function ($class) {
	$prefix = 'Mpdf';
	$base_dir = __DIR__ . '/vendor/Mpdf/Src/';
	$len = strlen($prefix);
	if (strncmp($prefix, $class, $len) !== 0) {
		return;
	}
	$relative_class = substr($class, $len);
	$file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';
	if (file_exists($file)) {
		require $file;
	}
});

//PHPQrCode
include_once "vendor/PHPQrCode/qrlib.php";

Start::Go(); //запускаем приложение