<section>
	<div class="container-alt">
		<div class="row">
			<div class="col-sm-12">
				<div class="wrapper-page">
					<div class="m-t-40 account-pages">
						<div class="text-center account-logo-box">
							<h4 class="text-uppercase font-bold text-white">Авторизация</h4>
						</div>
						<div class="account-content">
							<form method="post" name="auth_form" id="auth_form" enctype="multipart/form-data">
								<div class="form-group row">
									<div class="col-12">
										<input class="form-control" name="email" type="text" placeholder="E-Mail" required>
									</div>
								</div>
								<div class="form-group row">
									<div class="col-12">
										<input class="form-control" name="password" type="password" placeholder="Пароль" required>
									</div>
								</div>
								<div class="form-group account-btn text-center m-t-10">
									<div class="col-12">
										<button class="btn w-md btn-bordered btn-danger waves-effect waves-light btn-signin" type="submit">
											<span class="button-text">Войти</span>
											<div class="button-wait" style="display: none;">
												<i class="fas fa-spinner fa-spin"></i>
											</div>
										</button>
									</div>
								</div>
							</form>
							<div class="clearfix"></div>
						</div>
					</div>
					<!-- end card-box-->
				</div>
				<!-- end wrapper -->
			</div>
		</div>
	</div>
</section>