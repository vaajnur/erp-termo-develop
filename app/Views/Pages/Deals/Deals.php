<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="page-title-box">
					<h4 class="page-title">Продажи</h4>
					<ol class="breadcrumb p-0 m-0">
						<li class="breadcrumb-item">
							<a href="/">Главная</a>
						</li>
						<li class="breadcrumb-item active">
							Продажи
						</li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>	

<?

$months = ['Январь',
'Февраль',
'Март',
'Апрель',
'Май',
'Июнь',
'Июль',
'Август',
'Сентябрь',
'Октябрь',
'Ноябрь',
'Декабрь',];
?>

<ul class="nav nav-tabs" id="myTab" role="tablist">
  <? for ($i=1; $i < 13; $i++) { ?>
  <li class="nav-item">
    <a class="nav-link <?=($i==1?'show active':'');?>" id="home-tab<?=$i;?>" data-toggle="tab" href="#home<?=$i;?>" role="tab" aria-controls="home" aria-selected="true"><?=$months[$i-1];?></a>
  </li>
  <? } ?>
</ul>
<? //echo '<pre>';print_r($data['deals']);echo '</pre>';?>
<div class="user_id d-none"><?=$_COOKIE['user_id'];?></div>
<?

// var_dump($_COOKIE['user_id']);

?>

<div class="tab-content" id="myTabContent">
  <? for ($i=1; $i < 13; $i++) { 
  	// echo "$i";
  	?>
  <div class="tab-pane fade <?=($i==1?'show active':'');?>" id="home<?=$i;?>" data-month="<?=$i;?>" role="tabpanel" aria-labelledby="home-tab">
  	
		<div class="row">
			<div class="col-12">
			<table class="table table-bordered jtable-editable deals-table">
					<thead>
						<tr>
							<th>id</th>
							<th class="checkbox-td d-none"></th>
							<th>Тип договора</th>
							<th>Менеджер</th>
							<th>Ф.И.О., № договора</th>
							<th>Стоимость по договору</th>
							<th>Оплата первичные</th>
							<th>Себестоимость</th>
							<th>Общая площадь, м2</th>
							<th>Общая длина, пог.м.</th>
							<th>Ширина</th>
							<th>Сечение бруска</th>
							<th>Материал</th>
							<th>Материал от партнеров</th>
							<th>Стоимость крепежа</th>
							<th>Стоимость покраски, ЛКМ</th>
							<th>Комиссия Архитектора</th>
							<th>Стоимость доставки</th>
							<th>Маржа</th>
							<th>Дата оплаты</th>
							<th>Требуется доплата</th>
							<th>Отгрузка по договору</th>
							<th>Отгрузка Факт</th>
							<th>Нал/безнал</th>
							<th>Продукция</th>
							<th>Производитель</th>
							<th>Перевозчик</th>
							<th>Источник рекламы</th>
							<th  class="d-none">parent</th>
							<th  class="d-none">month</th>
						</tr>
					</thead>
					<tbody>
						<? if (!empty($data['deals'])) {
							$cost_by_contract_sum = 0;
							$payment_primary_sum = 0;
							$self_cost_sum = 0;
							$square_sum = 0;
							$length_sum = 0;
							$width_sum = 0;
							?>
							<? foreach ($data['deals'] as $key => $value) {
								if($i != $value['month']) continue;
								$cost_by_contract_sum += (float)$value['cost_by_contract'];
								$payment_primary_sum += (float)$value['payment_primary'];
								$self_cost_sum += (float)$value['self_cost'];
								$square_sum += (float)$value['square'];
								$length_sum += (float)$value['length'];
								$width_sum += (float)$value['width'];
								$margin = (float)$value['cost_by_contract'] - (float)$value['self_cost'] - (float)$value['material_from_partners'] - (float)$value['cost_of_fitting'] - (float)$value['cost_of_painting'] - (float)$value['arch_comission'];
								$need_postpay = (float)$value['cost_by_contract'] - (float)$value['self_cost'];

								?>
								<tr>
								<td><?=$value['id'];?></td>		
								<td  class="checkbox-td d-none"><? if($value['parent_id'] == $value['id']){?><input type="checkbox" name="" value="1" class="select-row"><?}?></td>		
								<td><?=$value['contract_type'];?></td>		
								<td><?=$value['manager'];?></td>		
								<td><?=$value['fio_number'];?></td>		
								<td><?=$value['cost_by_contract'];?></td>		
								<td><?=$value['payment_primary'];?></td>		
								<td><?=$value['self_cost'];?></td>		
								<td><?=$value['square'];?></td>		
								<td><?=$value['length'];?></td>		
								<td><?=$value['width'];?></td>		
								<td><?=$value['cross_section_of_the_bar'];?></td>		
								<td><?=$value['material'];?></td>		
								<td><?=$value['material_from_partners'];?></td>		
								<td><?=$value['cost_of_fitting'];?></td>		
								<td><?=$value['cost_of_painting'];?></td>		
								<td><?=$value['arch_comission'];?></td>		
								<td><?=$value['delivery_sum'];?></td>		
								<td><?=$margin;?></td>		
								<td><?=$value['pay_date'];?></td>		
								<td><?=$need_postpay;?></td>		
								<td><?=$value['shipment'];?></td>		
								<td><?=$value['shipment_fact'];?></td>		
								<td><?=$value['pay_type'];?></td>		
								<td><?=$value['goods'];?></td>		
								<td><?=$value['manufacturer'];?></td>		
								<td><?=$value['сarrier'];?></td>		
								<td><?=$value['ad_source'];?></td>		
								<td class="d-none"><?=$value['parent_id'];?></td>		
								<td class="d-none"><?=$value['month'];?></td>		
								</tr>
							<?}?>
						<?}?>
					</tbody>
					<tfoot style="background: #eee; font-weight: bold;">
						<tr>
							<td>Итог</td>
							<td></td>
							<td></td>
							<td><?=$cost_by_contract_sum;?></td>
							<td><?=$payment_primary_sum;?></td>
							<td><?=$self_cost_sum;?></td>
							<td><?=$square_sum;?></td>
							<td><?=$length_sum;?></td>
							<td><?=$width_sum;?></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</tfoot>
				</table>
				<div class="btn-groups">
					<button class="m-1 btn btn-success add-deal">Добавить сделку</button>
					<button class="m-1 btn btn-success add-deal-line">Добавить строку в сделку</button>
					<button class="m-1 btn btn-primary add-deal-line-confirm d-none">Добавить</button>
					<button class="m-1 btn btn-danger add-deal-line-cancel d-none">отменить</button>
				</div>

			</div>	
		</div>	
  </div>
<? } ?>
</div>


	</div>	
</div>	

<div class="guides_json d-none"><?=json_encode($data['guides']);?></div>