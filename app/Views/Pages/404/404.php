<main id="content" role="main">
	<!-- Hero Section -->
	<div class="d-lg-flex">
		<div class="container d-lg-flex align-items-lg-center min-height-lg-100vh space-4">
			<div class="w-lg-60 w-xl-50">
				<!-- Title -->
				<div class="mb-5">
					<h1 class="text-primary font-weight-normal">Страница не <span class="font-weight-semi-bold">найдена</span></h1>
					<p class="mb-0">Уупс! Похоже, вы перешли по сломанной ссылке.</p>
					<p>Если вы думаете, что это проблема с нами, пожалуйста, <a href="mailto:support@4walls.pro">сообщите нам</a>.</p>
				</div>
				<!-- End Title -->

				<a class="btn btn-primary btn-wide transition-3d-hover" href="/">На главную</a>
			</div>
		</div>
	</div>
	<!-- End Hero Section -->
</main>