		<div class="row">
			<div class="col-sm-4">
				<div class="card">
					<div class="card-body py-0">
						<h2 class="card-title h3">Породы</h2>
					</div>
					<div class="list-group list-group-flush">
						<!-- Обычный элемент, который просто выводится -->
						<li class="list-group-item">
							<div class="input-group">
							  <input type="text" class="edit-inp form-control" disabled="" placeholder="Введите название..." value="Обрезная доска">
							  <!-- 11111111 -->
							  <div class="input-group-append primary-mode">
							    <button class="edit-guide btn btn-outline-secondary" type="button" title="Редактировать"><i class="fas fa-edit fa-fw"></i></button>
							    <button class="btn del-guide btn-outline-secondary" type="button" title="Удалить"><i class="fas fa-trash fa-fw"></i></button>
							  </div>
							  <!-- 222222222 -->
							  <div class="input-group-append edit-mode d-none">
							    <button class="btn edit-cancel btn-outline-danger" type="button" title="Отменить"><i class="fas fa-times fa-fw"></i></button>
							    <button  data-name="profile"  data-id="<?=$profile['id'];?>63" class="btn confirm-edit btn-outline-success" type="button" title="Подтвердить"><i class="fas fa-check fa-fw"></i></button>
							  </div>
							  <!-- 33333333 del -->
							  <div class="input-group-append del-mode d-none">
							    <button   data-name="profile"  data-id="<?=$profile['id'];?>63"  class="btn confirm-del btn-outline-success" type="button" title="Подтвердить"><i class="fas fa-check fa-fw"></i></button>
							    <button class="btn cancel-del btn-outline-danger" type="button" title="Отменить"><i class="fas fa-times fa-fw"></i></button>
							  </div>
							</div>
						</li>



						<li class="list-group-item">
							<div class="input-group">
							  <input type="text" class="form-control " placeholder="Введите название нового элемента..." required="">
							  <div class="input-group-append">
							    <button class="btn btn-outline-success" type="button" title="Добавить"><i class="fas fa-plus fa-fw"></i></button>
							  </div>
							</div>
						</li>


					</div>
				</div>
			</div>
		</div>