<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="page-title-box">
					<h4 class="page-title">Справочники</h4>
					<ol class="breadcrumb p-0 m-0">
						<li class="breadcrumb-item">
							<a href="/">Главная</a>
						</li>
						<li class="breadcrumb-item active">
							Справочники
						</li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>	


		<div class="row">
			<div class="col-sm-3">
				<div class="card">
					<div class="card-body py-0">
						<h2 class="card-title h3">Породы</h2>
					</div>
					<div class="list-group list-group-flush">
						<!-- Обычный элемент, который просто выводится -->
						<? if(!empty($data['breed'])){
							foreach($data['breed'] as $breed){?>
						<li class="list-group-item">
							<div class="input-group">
							  <input type="text" class="edit-inp form-control" disabled="" placeholder="Введите название..." value="<?=$breed['name'];?>">
							  <!-- 11111111 -->
							  <div class="input-group-append primary-mode">
							    <button class="edit-guide btn btn-outline-secondary" type="button" title="Редактировать"><i class="fas fa-edit fa-fw"></i></button>
							    <button class="btn del-guide btn-outline-secondary" type="button" title="Удалить"><i class="fas fa-trash fa-fw"></i></button>
							  </div>
							  <!-- 222222222 -->
							  <div class="input-group-append edit-mode d-none">
							    <button class="btn edit-cancel btn-outline-danger" type="button" title="Отменить"><i class="fas fa-times fa-fw"></i></button>
							    <button  data-name="breed"  data-id="<?=$breed['id'];?>" class="btn confirm-edit btn-outline-success" type="button" title="Подтвердить"><i class="fas fa-check fa-fw"></i></button>
							  </div>
							  <!-- 33333333 del -->
							  <div class="input-group-append del-mode d-none">
							    <button   data-name="breed"  data-id="<?=$breed['id'];?>"  class="btn confirm-del btn-outline-success" type="button" title="Подтвердить"><i class="fas fa-check fa-fw"></i></button>
							    <button class="btn cancel-del btn-outline-danger" type="button" title="Отменить"><i class="fas fa-times fa-fw"></i></button>
							  </div>
							</div>
						</li>
						<?}}?>

						<!-- /////////// ADD ////////// -->
						<li class="list-group-item">
							<div class="input-group">
							  <input type="text" class="form-control " placeholder="Введите название нового элемента..." required="">
							  <div class="input-group-append">
							    <button  data-type="breed" class="add-guide btn btn-outline-success" type="button" title="Добавить"><i class="fas fa-plus fa-fw"></i></button>
							  </div>
							</div>
						</li>

					</div>
				</div>
			</div>

			<div class="col-sm-3">
				<div class="card">
					<div class="card-body py-0">
						<h2 class="card-title h3">Сорта</h2>
					</div>
					<div class="list-group list-group-flush">
						<!-- Обычный элемент, который просто выводится -->
						<? if(!empty($data['cort'])){
							foreach($data['cort'] as $sort){?>
						<li class="list-group-item">
							<div class="input-group">
							  <input type="text" class="edit-inp form-control" disabled="" placeholder="Введите название..." value="<?=$sort['name'];?>">
							  <!-- 11111111 -->
							  <div class="input-group-append primary-mode">
							    <button class="edit-guide btn btn-outline-secondary" type="button" title="Редактировать"><i class="fas fa-edit fa-fw"></i></button>
							    <button class="btn del-guide btn-outline-secondary" type="button" title="Удалить"><i class="fas fa-trash fa-fw"></i></button>
							  </div>
							  <!-- 222222222 -->
							  <div class="input-group-append edit-mode d-none">
							    <button class="btn edit-cancel btn-outline-danger" type="button" title="Отменить"><i class="fas fa-times fa-fw"></i></button>
							    <button  data-name="cort"  data-id="<?=$sort['id'];?>" class="btn confirm-edit btn-outline-success" type="button" title="Подтвердить"><i class="fas fa-check fa-fw"></i></button>
							  </div>
							  <!-- 33333333 del -->
							  <div class="input-group-append del-mode d-none">
							    <button   data-name="cort"  data-id="<?=$sort['id'];?>"  class="btn confirm-del btn-outline-success" type="button" title="Подтвердить"><i class="fas fa-check fa-fw"></i></button>
							    <button class="btn cancel-del btn-outline-danger" type="button" title="Отменить"><i class="fas fa-times fa-fw"></i></button>
							  </div>
							</div>
						</li>
						<?}}?>

						<!-- /////////// ADD ////////// -->
						<li class="list-group-item">
							<div class="input-group">
							  <input type="text" class="form-control " placeholder="Введите название нового элемента..." required="">
							  <div class="input-group-append">
							    <button  data-type="cort" class="add-guide btn btn-outline-success" type="button" title="Добавить"><i class="fas fa-plus fa-fw"></i></button>
							  </div>
							</div>
						</li>

					</div>
				</div>
			</div>

			<div class="col-sm-3">
				<div class="card">
					<div class="card-body py-0">
						<h2 class="card-title h3">Профили</h2>
					</div>
					<div class="list-group list-group-flush">
						<!-- Обычный элемент, который просто выводится -->
						<? if(!empty($data['profile'])){
							foreach($data['profile'] as $profile){?>
						<li class="list-group-item">
							<div class="input-group">
							  <input type="text" class="edit-inp form-control" disabled="" placeholder="Введите название..." value="<?=$profile['name'];?>">
							  <!-- 11111111 -->
							  <div class="input-group-append primary-mode">
							    <button class="edit-guide btn btn-outline-secondary" type="button" title="Редактировать"><i class="fas fa-edit fa-fw"></i></button>
							    <button class="btn del-guide btn-outline-secondary" type="button" title="Удалить"><i class="fas fa-trash fa-fw"></i></button>
							  </div>
							  <!-- 222222222 -->
							  <div class="input-group-append edit-mode d-none">
							    <button class="btn edit-cancel btn-outline-danger" type="button" title="Отменить"><i class="fas fa-times fa-fw"></i></button>
							    <button  data-name="profile"  data-id="<?=$profile['id'];?>" class="btn confirm-edit btn-outline-success" type="button" title="Подтвердить"><i class="fas fa-check fa-fw"></i></button>
							  </div>
							  <!-- 33333333 del -->
							  <div class="input-group-append del-mode d-none">
							    <button   data-name="profile"  data-id="<?=$profile['id'];?>"  class="btn confirm-del btn-outline-success" type="button" title="Подтвердить"><i class="fas fa-check fa-fw"></i></button>
							    <button class="btn cancel-del btn-outline-danger" type="button" title="Отменить"><i class="fas fa-times fa-fw"></i></button>
							  </div>
							</div>
						</li>
						<?}}?>

						<!-- /////////// ADD ////////// -->
						<li class="list-group-item">
							<div class="input-group">
							  <input type="text" class="form-control " placeholder="Введите название нового элемента..." required="">
							  <div class="input-group-append">
							    <button  data-type="profile" class="add-guide btn btn-outline-success" type="button" title="Добавить"><i class="fas fa-plus fa-fw"></i></button>
							  </div>
							</div>
						</li>

					</div>
				</div>
			</div>
		</div>


		<div class="row">
			<div class="col-sm-3">
				<div class="card">
					<div class="card-body py-0">
						<h2 class="card-title h3">Цвета</h2>
					</div>
					<div class="list-group list-group-flush">
						<!-- Обычный элемент, который просто выводится -->
						<? if(!empty($data['color'])){
							foreach($data['color'] as $color){?>
						<li class="list-group-item">
							<div class="input-group">
							  <input type="text" class="edit-inp form-control" disabled="" placeholder="Введите название..." value="<?=$color['name'];?>">
							  <!-- 11111111 -->
							  <div class="input-group-append primary-mode">
							    <button class="edit-guide btn btn-outline-secondary" type="button" title="Редактировать"><i class="fas fa-edit fa-fw"></i></button>
							    <button class="btn del-guide btn-outline-secondary" type="button" title="Удалить"><i class="fas fa-trash fa-fw"></i></button>
							  </div>
							  <!-- 222222222 -->
							  <div class="input-group-append edit-mode d-none">
							    <button class="btn edit-cancel btn-outline-danger" type="button" title="Отменить"><i class="fas fa-times fa-fw"></i></button>
							    <button  data-name="color"  data-id="<?=$color['id'];?>" class="btn confirm-edit btn-outline-success" type="button" title="Подтвердить"><i class="fas fa-check fa-fw"></i></button>
							  </div>
							  <!-- 33333333 del -->
							  <div class="input-group-append del-mode d-none">
							    <button   data-name="color"  data-id="<?=$color['id'];?>"  class="btn confirm-del btn-outline-success" type="button" title="Подтвердить"><i class="fas fa-check fa-fw"></i></button>
							    <button class="btn cancel-del btn-outline-danger" type="button" title="Отменить"><i class="fas fa-times fa-fw"></i></button>
							  </div>
							</div>
						</li>
						<?}}?>

						<!-- /////////// ADD ////////// -->
						<li class="list-group-item">
							<div class="input-group">
							  <input type="text" class="form-control " placeholder="Введите название нового элемента..." required="">
							  <div class="input-group-append">
							    <button  data-type="color" class="add-guide btn btn-outline-success" type="button" title="Добавить"><i class="fas fa-plus fa-fw"></i></button>
							  </div>
							</div>
						</li>

					</div>
				</div>
			</div>

			<div class="col-sm-3">
				<div class="card">
					<div class="card-body py-0">
						<h2 class="card-title h3">Масло</h2>
					</div>
					<div class="list-group list-group-flush">
						<!-- Обычный элемент, который просто выводится -->
						<? if(!empty($data['maslo'])){
							foreach($data['maslo'] as $maslo){?>
						<li class="list-group-item">
							<div class="input-group">
							  <input type="text" class="edit-inp form-control" disabled="" placeholder="Введите название..." value="<?=$maslo['name'];?>">
							  <!-- 11111111 -->
							  <div class="input-group-append primary-mode">
							    <button class="edit-guide btn btn-outline-secondary" type="button" title="Редактировать"><i class="fas fa-edit fa-fw"></i></button>
							    <button class="btn del-guide btn-outline-secondary" type="button" title="Удалить"><i class="fas fa-trash fa-fw"></i></button>
							  </div>
							  <!-- 222222222 -->
							  <div class="input-group-append edit-mode d-none">
							    <button class="btn edit-cancel btn-outline-danger" type="button" title="Отменить"><i class="fas fa-times fa-fw"></i></button>
							    <button  data-name="maslo"  data-id="<?=$maslo['id'];?>" class="btn confirm-edit btn-outline-success" type="button" title="Подтвердить"><i class="fas fa-check fa-fw"></i></button>
							  </div>
							  <!-- 33333333 del -->
							  <div class="input-group-append del-mode d-none">
							    <button   data-name="maslo"  data-id="<?=$maslo['id'];?>"  class="btn confirm-del btn-outline-success" type="button" title="Подтвердить"><i class="fas fa-check fa-fw"></i></button>
							    <button class="btn cancel-del btn-outline-danger" type="button" title="Отменить"><i class="fas fa-times fa-fw"></i></button>
							  </div>
							</div>
						</li>
						<?}}?>

						<!-- /////////// ADD ////////// -->
						<li class="list-group-item">
							<div class="input-group">
							  <input type="text" class="form-control " placeholder="Введите название нового элемента..." required="">
							  <div class="input-group-append">
							    <button  data-type="maslo" class="add-guide btn btn-outline-success" type="button" title="Добавить"><i class="fas fa-plus fa-fw"></i></button>
							  </div>
							</div>
						</li>

					</div>
				</div>
			</div>
		</div>

		<!-- SALES -->
		<div class="row">
			<div class="col-sm-12">
				<h3 class="text-center">Сделки</h4>
				<hr>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-3">
				<div class="card">
					<div class="card-body py-0">
						<h2 class="card-title h3">Тип договора</h2>
					</div>
					<div class="list-group list-group-flush">
						<!-- Обычный элемент, который просто выводится -->
						<? if(!empty($data['contract_type'])){
							foreach($data['contract_type'] as $contract_type){?>
						<li class="list-group-item">
							<div class="input-group">
							  <input type="text" class="edit-inp form-control" disabled="" placeholder="Введите название..." value="<?=$contract_type['name'];?>">
							  <!-- 11111111 -->
							  <div class="input-group-append primary-mode">
							    <button class="edit-guide btn btn-outline-secondary" type="button" title="Редактировать"><i class="fas fa-edit fa-fw"></i></button>
							    <button class="btn del-guide btn-outline-secondary" type="button" title="Удалить"><i class="fas fa-trash fa-fw"></i></button>
							  </div>
							  <!-- 222222222 -->
							  <div class="input-group-append edit-mode d-none">
							    <button class="btn edit-cancel btn-outline-danger" type="button" title="Отменить"><i class="fas fa-times fa-fw"></i></button>
							    <button  data-name="contract_type"  data-id="<?=$contract_type['id'];?>" class="btn confirm-edit btn-outline-success" type="button" title="Подтвердить"><i class="fas fa-check fa-fw"></i></button>
							  </div>
							  <!-- 33333333 del -->
							  <div class="input-group-append del-mode d-none">
							    <button   data-name="contract_type"  data-id="<?=$contract_type['id'];?>"  class="btn confirm-del btn-outline-success" type="button" title="Подтвердить"><i class="fas fa-check fa-fw"></i></button>
							    <button class="btn cancel-del btn-outline-danger" type="button" title="Отменить"><i class="fas fa-times fa-fw"></i></button>
							  </div>
							</div>
						</li>
						<?}}?>

						<!-- /////////// ADD ////////// -->
						<li class="list-group-item">
							<div class="input-group">
							  <input type="text" class="form-control " placeholder="Введите название нового элемента..." required="">
							  <div class="input-group-append">
							    <button  data-type="contract_type" class="add-guide btn btn-outline-success" type="button" title="Добавить"><i class="fas fa-plus fa-fw"></i></button>
							  </div>
							</div>
						</li>

					</div>
				</div>
			</div>

<!-- //// -->
			<div class="col-sm-3">
				<div class="card">
					<div class="card-body py-0">
						<h2 class="card-title h3">Нал/безнал</h2>
					</div>
					<div class="list-group list-group-flush">
						<!-- Обычный элемент, который просто выводится -->
						<? if(!empty($data['pay_type'])){
							foreach($data['pay_type'] as $pay_type){?>
						<li class="list-group-item">
							<div class="input-group">
							  <input type="text" class="edit-inp form-control" disabled="" placeholder="Введите название..." value="<?=$pay_type['name'];?>">
							  <!-- 11111111 -->
							  <div class="input-group-append primary-mode">
							    <button class="edit-guide btn btn-outline-secondary" type="button" title="Редактировать"><i class="fas fa-edit fa-fw"></i></button>
							    <button class="btn del-guide btn-outline-secondary" type="button" title="Удалить"><i class="fas fa-trash fa-fw"></i></button>
							  </div>
							  <!-- 222222222 -->
							  <div class="input-group-append edit-mode d-none">
							    <button class="btn edit-cancel btn-outline-danger" type="button" title="Отменить"><i class="fas fa-times fa-fw"></i></button>
							    <button  data-name="pay_type"  data-id="<?=$pay_type['id'];?>" class="btn confirm-edit btn-outline-success" type="button" title="Подтвердить"><i class="fas fa-check fa-fw"></i></button>
							  </div>
							  <!-- 33333333 del -->
							  <div class="input-group-append del-mode d-none">
							    <button   data-name="pay_type"  data-id="<?=$pay_type['id'];?>"  class="btn confirm-del btn-outline-success" type="button" title="Подтвердить"><i class="fas fa-check fa-fw"></i></button>
							    <button class="btn cancel-del btn-outline-danger" type="button" title="Отменить"><i class="fas fa-times fa-fw"></i></button>
							  </div>
							</div>
						</li>
						<?}}?>

						<!-- /////////// ADD ////////// -->
						<li class="list-group-item">
							<div class="input-group">
							  <input type="text" class="form-control " placeholder="Введите название нового элемента..." required="">
							  <div class="input-group-append">
							    <button  data-type="pay_type" class="add-guide btn btn-outline-success" type="button" title="Добавить"><i class="fas fa-plus fa-fw"></i></button>
							  </div>
							</div>
						</li>

					</div>
				</div>
			</div>

			<!-- /////////// -->
			<div class="col-sm-3">
				<div class="card">
					<div class="card-body py-0">
						<h2 class="card-title h3">Производитель</h2>
					</div>
					<div class="list-group list-group-flush">
						<!-- Обычный элемент, который просто выводится -->
						<? if(!empty($data['manufacturer'])){
							foreach($data['manufacturer'] as $manufacturer){?>
						<li class="list-group-item">
							<div class="input-group">
							  <input type="text" class="edit-inp form-control" disabled="" placeholder="Введите название..." value="<?=$manufacturer['name'];?>">
							  <!-- 11111111 -->
							  <div class="input-group-append primary-mode">
							    <button class="edit-guide btn btn-outline-secondary" type="button" title="Редактировать"><i class="fas fa-edit fa-fw"></i></button>
							    <button class="btn del-guide btn-outline-secondary" type="button" title="Удалить"><i class="fas fa-trash fa-fw"></i></button>
							  </div>
							  <!-- 222222222 -->
							  <div class="input-group-append edit-mode d-none">
							    <button class="btn edit-cancel btn-outline-danger" type="button" title="Отменить"><i class="fas fa-times fa-fw"></i></button>
							    <button  data-name="manufacturer"  data-id="<?=$manufacturer['id'];?>" class="btn confirm-edit btn-outline-success" type="button" title="Подтвердить"><i class="fas fa-check fa-fw"></i></button>
							  </div>
							  <!-- 33333333 del -->
							  <div class="input-group-append del-mode d-none">
							    <button   data-name="manufacturer"  data-id="<?=$manufacturer['id'];?>"  class="btn confirm-del btn-outline-success" type="button" title="Подтвердить"><i class="fas fa-check fa-fw"></i></button>
							    <button class="btn cancel-del btn-outline-danger" type="button" title="Отменить"><i class="fas fa-times fa-fw"></i></button>
							  </div>
							</div>
						</li>
						<?}}?>

						<!-- /////////// ADD ////////// -->
						<li class="list-group-item">
							<div class="input-group">
							  <input type="text" class="form-control " placeholder="Введите название нового элемента..." required="">
							  <div class="input-group-append">
							    <button  data-type="manufacturer" class="add-guide btn btn-outline-success" type="button" title="Добавить"><i class="fas fa-plus fa-fw"></i></button>
							  </div>
							</div>
						</li>

					</div>
				</div>
			</div>


			<!-- *********** -->
			<div class="col-sm-3">
				<div class="card">
					<div class="card-body py-0">
						<h2 class="card-title h3">Перевозчик</h2>
					</div>
					<div class="list-group list-group-flush">
						<!-- Обычный элемент, который просто выводится -->
						<? if(!empty($data['сarrier'])){
							foreach($data['сarrier'] as $сarrier){?>
						<li class="list-group-item">
							<div class="input-group">
							  <input type="text" class="edit-inp form-control" disabled="" placeholder="Введите название..." value="<?=$сarrier['name'];?>">
							  <!-- 11111111 -->
							  <div class="input-group-append primary-mode">
							    <button class="edit-guide btn btn-outline-secondary" type="button" title="Редактировать"><i class="fas fa-edit fa-fw"></i></button>
							    <button class="btn del-guide btn-outline-secondary" type="button" title="Удалить"><i class="fas fa-trash fa-fw"></i></button>
							  </div>
							  <!-- 222222222 -->
							  <div class="input-group-append edit-mode d-none">
							    <button class="btn edit-cancel btn-outline-danger" type="button" title="Отменить"><i class="fas fa-times fa-fw"></i></button>
							    <button  data-name="сarrier"  data-id="<?=$сarrier['id'];?>" class="btn confirm-edit btn-outline-success" type="button" title="Подтвердить"><i class="fas fa-check fa-fw"></i></button>
							  </div>
							  <!-- 33333333 del -->
							  <div class="input-group-append del-mode d-none">
							    <button   data-name="сarrier"  data-id="<?=$сarrier['id'];?>"  class="btn confirm-del btn-outline-success" type="button" title="Подтвердить"><i class="fas fa-check fa-fw"></i></button>
							    <button class="btn cancel-del btn-outline-danger" type="button" title="Отменить"><i class="fas fa-times fa-fw"></i></button>
							  </div>
							</div>
						</li>
						<?}}?>

						<!-- /////////// ADD ////////// -->
						<li class="list-group-item">
							<div class="input-group">
							  <input type="text" class="form-control " placeholder="Введите название нового элемента..." required="">
							  <div class="input-group-append">
							    <button  data-type="сarrier" class="add-guide btn btn-outline-success" type="button" title="Добавить"><i class="fas fa-plus fa-fw"></i></button>
							  </div>
							</div>
						</li>

					</div>
				</div>
			</div>


		</div>

		<div class="row">

			<div class="col-sm-3">
				<div class="card">
					<div class="card-body py-0">
						<h2 class="card-title h3">Источник рекламы</h2>
					</div>
					<div class="list-group list-group-flush">
						<!-- Обычный элемент, который просто выводится -->
						<? if(!empty($data['ad_source'])){
							foreach($data['ad_source'] as $ad_source){?>
						<li class="list-group-item">
							<div class="input-group">
							  <input type="text" class="edit-inp form-control" disabled="" placeholder="Введите название..." value="<?=$ad_source['name'];?>">
							  <!-- 11111111 -->
							  <div class="input-group-append primary-mode">
							    <button class="edit-guide btn btn-outline-secondary" type="button" title="Редактировать"><i class="fas fa-edit fa-fw"></i></button>
							    <button class="btn del-guide btn-outline-secondary" type="button" title="Удалить"><i class="fas fa-trash fa-fw"></i></button>
							  </div>
							  <!-- 222222222 -->
							  <div class="input-group-append edit-mode d-none">
							    <button class="btn edit-cancel btn-outline-danger" type="button" title="Отменить"><i class="fas fa-times fa-fw"></i></button>
							    <button  data-name="ad_source"  data-id="<?=$ad_source['id'];?>" class="btn confirm-edit btn-outline-success" type="button" title="Подтвердить"><i class="fas fa-check fa-fw"></i></button>
							  </div>
							  <!-- 33333333 del -->
							  <div class="input-group-append del-mode d-none">
							    <button   data-name="ad_source"  data-id="<?=$ad_source['id'];?>"  class="btn confirm-del btn-outline-success" type="button" title="Подтвердить"><i class="fas fa-check fa-fw"></i></button>
							    <button class="btn cancel-del btn-outline-danger" type="button" title="Отменить"><i class="fas fa-times fa-fw"></i></button>
							  </div>
							</div>
						</li>
						<?}}?>

						<!-- /////////// ADD ////////// -->
						<li class="list-group-item">
							<div class="input-group">
							  <input type="text" class="form-control " placeholder="Введите название нового элемента..." required="">
							  <div class="input-group-append">
							    <button  data-type="ad_source" class="add-guide btn btn-outline-success" type="button" title="Добавить"><i class="fas fa-plus fa-fw"></i></button>
							  </div>
							</div>
						</li>

					</div>
				</div>
			</div>

		</div>

	</div> <!-- container-fluid -->
</div> <!-- content -->