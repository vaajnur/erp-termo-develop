<!-- Start content -->
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="page-title-box">
					<h6>Настройки</h6>
					<h4 class="page-title">Логи</h4>
					<ol class="breadcrumb p-0 m-0">
						<li class="breadcrumb-item">
							<a href="/">Главная</a>
						</li>
						<li class="breadcrumb-item active">
							Логи
						</li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="card mb-0">
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered m-0 small">
						<thead>
						<tr>
							<th>ID</th>
							<th>Значение</th>
							<th>Дата</th>
							<th>Пользователь</th>
						</tr>
						</thead>
						<tbody>
							<? if(!empty($data)){?>
						<?php
						foreach ($data as $log) {
							?>
							<tr class="text-dark">
								<td><?= $log['id']; ?></td>
								<td><?= $log['value']; ?></td>
								<td><?= $log['date_create_str']; ?></td>
								<td><?= $log['user_name']; ?> <?= $log['user_surname']; ?></td>
							</tr>
							<?php
						}
						}
						?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div> <!-- container-fluid -->
</div> <!-- content -->