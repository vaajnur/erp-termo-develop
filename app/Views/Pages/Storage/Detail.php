<?
use StandartRus\App\Start as st;

?>

<!-- Start content -->
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="page-title-box">
					<h6>Склад</h6>
					<h4 class="page-title"><?= $name_storage; ?></h4>
					<ol class="breadcrumb p-0 m-0">
						<li class="breadcrumb-item">
							<a href="/">Главная</a>
						</li>
						<li class="breadcrumb-item">
							<a href="/storage/">Склады</a>
						</li>
						<li class="breadcrumb-item active">
							<?= $name_storage; ?>
						</li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<?php
		//if ($routes[2] == '1') {
			?>
			<h5 id="title_form_send_raw_material_to_manufacture" class="d-none">Укажите товар и его количество для отправки на склад</h5>
			<?php
		/*} elseif ($routes[2] == '3') {
			?>
			<h5 id="title_form_send_products_to_repair" class="d-none">Укажите товар и его количество для отправки на покраску</h5>
			<?php
		}*/
		?>
		<?php
		//if ($routes[2] != '2' && $routes[2] != '4' && $routes[2] != '7' && $routes[2] != '8') {
			?>
			<!-- <button type="button" class="btn btn-primary waves-effect mb-4 btn-show-form-add-consignment"><span class="mr-2">+</span>Добавить</button> -->


 <? include 'add_consignment.php';?>
			
			<?php
		//}
		?>
		<div class="row mb-4" id="storage_block_control">
    
			<div class="col-12">
			<?php if ($routes[2] != '4') {
        // &tint=<?= $tint_active; ? >
        ?>
				<div class="btn-group m-b-10">
          <a href="?breed=all" class="btn <?= $breed_active == 'all' ? 'btn-dark' : 'btn-outline-dark' ?> waves-effect waves-light">Все</a>
					<?php
          
					if (!empty($data['breed'])) {
					foreach ($data['breed'] as $breed) {
            /*
						$tint_active_menu = 0;
						foreach ($data['tint'] as $tint) {
							if ($tint['breed'] == $breed['id']) {
								$tint_active_menu = $tint['id'];
								break;
							}
						}
            */
            // &tint=<?= $tint_active_menu; ? >
						?>
						<a href="?breed=<?= $breed['id']; ?>" class="btn <?= $breed_active == $breed['id'] ? 'btn-dark' : 'btn-outline-dark' ?> waves-effect waves-light"><?= $breed['name']; ?></a>
						<?php
					}
					}
					?>
				</div>
			</div>
			<!-- div class="col-2">
				<div class="btn-group m-b-10">
					<?php
					if (!empty($data['tint'])) {
						?>
          <a href="?tint=all&breed=<?= $breed_active ?>" class="btn <?= $tint_active == 'all' ? 'btn-dark' : 'btn-outline-dark' ?> waves-effect waves-light">Все</a>
						<?php
					}
					?>
					<?php
					foreach ($data['tint'] as $tint) {
						?>
						<a href="?breed=<?= $breed_active; ?>&tint=<?= $tint['id']; ?>" class="btn <?= $tint_active == $tint['id'] ? 'btn-dark' : 'btn-outline-dark' ?> waves-effect waves-light"><?= $tint['name']; ?></a>
						<?php
					}
					?>
				</div>
      <?php } ?>
			</div -->

		<?php if ($routes[2] == '3') {?>
											<!-- фильтр Профили  -->
			<div class="col-2">
				<div class="btn-group m-b-10">
					<?php

					if (!empty($data['profiles_list'])) {
						// var_dump($_GET);
						$profiles_active = $_GET['profiles'];
						$this->f12($data['profiles_list']);
						// $this->f12($data['profiles']);
						?>
          <a href="<?=$this->requestUriAddGetParams(['profiles'=>'all']);?>" class="btn <?= $profiles_active == 'all' ? 'btn-dark' : 'btn-outline-dark' ?> waves-effect waves-light">Все</a>
						<?php
					foreach ($data['profiles_list'] as $profiles) {
						?>
						<a href="<?=$this->requestUriAddGetParams(['profiles'=>$profiles]);?>" class="btn <?= $profiles_active == $profiles ? 'btn-dark' : 'btn-outline-dark' ?> waves-effect waves-light"><?= $profiles; ?></a>
						<?php
					} } ?>
				</div>
			</div>
		<?}?>
				<div class='table-top-controls-fake'></div>
				<div class="col-12 d-flex mt-4 table-top-controls">
					<div class=''>
						<button type="button" class="btn btn-primary waves-effect btn-show-form-add-consignment">
						<span class="mr-2">+</span>Добавить</button>
					</div>
					<div class="flex-grow-1 text-right">
						<button data-storage="<?=$routes[2];?>" class="btn btn-warning waves-effect join_groups">Объединить группы</button>
					<?php
			// Строгание
			if ($routes[2] == '2') {
				?>
					<button type="button" class="btn btn-warning waves-effect btn-show-form-send-raw-materials-to-manufacture otgruzka-zakazchiku">Отгрузка заказчику</button>
			<? } ?>	
					<button type="button" class="btn btn-warning waves-effect btn-show-form-send-raw-materials-to-manufacture">Отправить</button>
					</div>
				</div>
			<?php
			//if ($routes[2] == '1') {
				?>
				<?php
			/*} elseif ($routes[2] == '3') {
				?>
				<div class="col-5 text-right">
					<button type="button" class="btn btn-warning waves-effect btn-show-form-send-products-to-repair">Отправить на покраску</button>
				</div>
				<?php
			}*/
			?>
		</div>

<!-- <div class="join_groups_block">
	<div class="col-2">
		<button data-storage="<?=$routes[2];?>" class="btn btn-warning waves-effect join_groups">Объединить группы</button>
	</div>
	<div class="d-none mess alert alert-danger"></div>
</div> -->


          <? include 'consignment.php';?>

         

<!-- *************************
Кнопки объединить группы
************************** -->
<div class="sticky-bottom d-none" id="join_groups_sticky_block">
	<div class="card">
		<div class="card-body">
			<div class="row justify-content-between align-items-center">
				<div class="row join_groups_block">
					<div class="col-12">
						<button class="btn btn-success waves-effect join_groups_save d-none">Сохранить</button>
						<button class="btn btn-default waves-effect join_groups_cancel d-none">Отменить</button>
					</div>
					<div class="d-none mess alert alert-danger"></div>
				</div>
			</div>
		</div>
	</div>
</div>

		<?php
		//if ($routes[2] == '1') {
			?>
			<div class="sticky-bottom d-none" id="block_send_raw_materials_to_manufacture">
				<div class="card">
					<div class="card-body">
						<div class="row justify-content-between align-items-end">
							<div class="col-2">
								<div class='mb-3'>Выбрано позиций: <strong id="counter_checked_raw_material">0</strong></div>
								<div>Указано товаров: <strong id="counter_count_checked_raw_material">0</strong></div>
							</div>
							<div class="col-10">
								<div class="row">
									<div class="col d-flex justify-content-between align-items-end flex-wrap">
										<div class="form-group align-items-center mb-0">
											<div class='mb-2'>Куда:</div>
											<div class="">
						                        <select id="send-to-storage-select" name="storage" class="form-control select2-active hide-input" data-placeholder="Выбрать склад" required>
						                            <?php foreach ($storage_list as $storage) { ?>
						                            <?php if ($storage['id'] == 9 and $storage['id'] != $routes[2]) continue; ?>
						                            <?php /*if ($storage['id'] == $routes[2]) continue;*/ ?>
						                                <option value="<?= $storage['id']; ?>"<?= ($storage['id'] == $routes[2]) ? ' selected' : '' ?>><?= $storage['name']; ?></option>
						                            <?php } ?>
						                        </select>
											</div>
										</div>

								<?php
								// Сырье
				                // if ($routes[2] == '1') {
				                ?>
								<div class="form-group align-items-center mb-0">
									<div class="mb-2">Срок производства:</div>
									<div class="">
										<input type="text" class="form-control datepicker" id="date_manufacture" placeholder="<?= date('d') + 1; ?>.<?= date('m'); ?>.<?= date('Y'); ?>" value="<?= date('d') + 1; ?>.<?= date('m'); ?>.<?= date('Y'); ?>">
									</div>
								</div>
				                <?php
				                // }
				                ?>

						<input type="hidden" id="send-to-storage-consignment-select" value="new" name="consignment">										
								<!-- <div class="form-group align-items-center mb-0">
									<div class="mb-2">Группа:</div>
									<div class="">
                        <select id="send-to-storage-consignment-select" name="consignment" class="form-control select2-active hide-input" data-placeholder="Выбрать группу" required>
                            <option value="new" data-storage="new">Новая</option>
                            <?php foreach ($data['consignmentsAllStorage'] as $tmp) {
                                    foreach ($tmp as $cons) {
                                      if (empty($cons['description'])) $cons['description'] = 'Без описания '.$cons['id'].' от '.$cons['date_create_str'];
                                    ?>
                                <option value="<?= $cons['id']; ?>" data-storage="<?= $cons['storage']; ?>" <?= ($cons['storage'] != $routes[2]) ? ' style="display: none;"' : '' ?>><?= $cons['description']; ?></option>
                                    <?php 
                                    }
                                  }
                                  ?>
                        </select>
									</div>
								</div> -->
								<div class="text-right">
									<input type="hidden" id="breed_active" value="<?= $breed_active; ?>">
									<input type="hidden" id="tint_active" value="<?= $tint_active; ?>">
									<button type="button" class="btn btn-danger btn-hide-form-send-raw-materials-to-manufacture">Отмена</button>
									<button type="button" class="btn btn-primary btn-form-send-raw-materials-tocolor d-none">На покраску</button>
									<!-- button type="button" class="btn btn-primary waves-effect btn-form-send-raw-materials-to-manufacture" data-msg="Партия отправлена!" -->
									<button type="button" class="btn btn-primary waves-effect btn-form-sendcheck" data-msg="Партия отправлена!" disabled>
										<span class="button-text">Отправить</span>
										<div class="button-wait" style="display: none;">
											<i class="fas fa-spinner fa-spin"></i>
										</div>
									</button>
							</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php
		/*} elseif ($routes[2] == '3') {
			?>
			<div class="sticky-bottom d-none" id="block_send_products_to_repair">
				<div class="card">
					<div class="card-body">
						<div class="row justify-content-between align-items-center">
							<div class="col-auto">
								Выбрано позиций: <strong id="counter_checked_raw_material">0</strong>
							</div>
							<div class="col-auto">
								Указано товаров: <strong id="counter_count_checked_raw_material">0</strong>
							</div>
							<div class="col-auto">
								<input type="hidden" id="breed_active" value="<?= $breed_active; ?>">
								<input type="hidden" id="tint_active" value="<?= $tint_active; ?>">
								<button type="button" class="btn btn-danger btn-hide-form-send-products-to-repair">Отмена</button>
								<button type="button" class="btn btn-primary waves-effect btn-form-send-products-to-repair" data-msg="Партия отправлена!">
									<span class="button-text">Отправить</span>
									<div class="button-wait" style="display: none;">
										<i class="fas fa-spinner fa-spin"></i>
									</div>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php
		}*/
		?>
	</div> <!-- container-fluid -->
</div> <!-- content -->
<!-- Добавить строку материала при добавлении группы  -->
<? include 'row_raw_materials.php';?>


<!-- добавление строки в модалке Заврешить (old version)  -->
<div class="row d-none" id="row_complete_product">
	<div class="col">
		<div class="form-group">
			<input type="text" class="form-control form-autocomplete-name input-sm input-sm field-input-name-complete-products" name="name[]">
		</div>
	</div>
	<div class="col">
        <div class="form-group">
            <select name="profile[]" class="form-control select2 profile-select" data-placeholder="" required>
				<?php foreach($data['profiles'] as $profile){?>
                    <option value="<?=$profile;?>"><?=$profile;?></option>
				<?}?>
            </select>
        </div>
    </div>
	<div class="col">
		<div class="form-group">
			<input type="number" min="1" class="form-control input-sm input-form-quantity" value="0" name="quantity[]">
		</div>
	</div>
	<div class="col">
		<div class="form-group">
			<input type="text" class="form-control input-sm touchspin-input input-form-thickness" value="0" name="thickness[]">
		</div>
	</div>
	<div class="col">
		<div class="form-group">
			<input type="text" class="form-control input-sm touchspin-input input-form-width" value="0" name="width[]">
		</div>
	</div>
	<div class="col">
		<div class="form-group">
			<input type="text" class="form-control input-sm touchspin-input input-form-length" value="0" name="length[]">
		</div>
	</div>
	<div class="col d-flex">
		<div class="form-group">
			<input type="text" class="form-control input-sm touchspin-input input-form-volume-m3" value="0" name="volume_m3[]">
		</div>
	</div>
</div>
<!-- модалка для кнопки Заврешить (old version) -->
<? include 'modal-form-success-manufacture-raw-material.php'; ?>

<!-- modal На покраску -->
<? include 'modal-form-tocolor-material.php'; ?>

<!-- модалка Отправки на другой склад -->
<? include 'modal-form-sendcheck-material.php'; ?>

<!-- Добавить строку при редактировании -->
<? include 'template_row_raw_material.php';?>
<?
// file_put_contents(__DIR__.'/filename.log', var_export(get_defined_vars(), true));
