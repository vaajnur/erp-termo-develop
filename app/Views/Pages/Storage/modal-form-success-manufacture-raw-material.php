<div class="modal fade modal-form-success-manufacture-raw-material" role="dialog" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title mt-0">Укажите полученные товары</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form name="add_products" method="post" enctype="multipart/form-data">
					<input type="hidden" id="storage_id_success" name="storage" value="<?=$data['storage_success'];?>">
					<input type="hidden" id="consignment_id_success" name="consignment" value="3">
					<div class="row">
						<div class="col-2">
							<div class="form-group">
								<label for="breed_select">Порода</label>
								<select name="breed" id="breed_select_popup" class="form-control select2" data-placeholder="Выбрать породу" required>
									<option></option>
									<?php
									foreach ($data['breed'] as $breed) {
										?>
										<option value="<?= $breed['id']; ?>"><?= $breed['name']; ?></option>
										<?php
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-2">
							<div class="form-group">
								<label for="tint_select">Оттенок</label>
								<select name="tint" id="tint_select_popup" class="form-control select2" data-placeholder="Выбрать оттенок">
									<option></option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col">
							<div class="form-group mb-0">
								<label>Наименование</label>
							</div>
						</div>
						<? if($routes['2'] != 7 && $routes['2'] != 8){?>
                        <div class="col">
                            <div class="form-group mb-0">
                                <label>Профиль</label>
                            </div>
                        </div>
						<? } ?>
						<div class="col">
							<div class="form-group mb-0">
								<label>Кол-во, шт.</label>
							</div>
						</div>
						<div class="col">
							<div class="form-group mb-0">
								<label>Толщина, мм.</label>
							</div>
						</div>
						<div class="col">
							<div class="form-group mb-0">
								<label>Ширина, мм.</label>
							</div>
						</div>
						<div class="col">
							<div class="form-group mb-0">
								<label>Длина, мм.</label>
							</div>
						</div>
						<div class="col">
							<div class="form-group mb-0">
								<label>м<sup>3</sup></label>
							</div>
						</div>
					</div>
					<div class="list-complete-products">
						<?php
						for ($i = 1; $i <= 5; $i++) {
							?>
							<div class="row">
								<div class="col">
									<div class="form-group">
										<input type="text" class="form-control form-autocomplete-name input-sm <?= $i == 5 ? 'field-input-name-complete-products' : '' ?>" name="name[]">
									</div>
								</div>
								<? if($routes['2'] != 7 && $routes['2'] != 8){?>
                                <div class="col">
                                    <div class="form-group">
                                        <select name="profile[]" class="form-control select2 profile-select" data-placeholder="" required>
											<?php foreach($data['profiles'] as $profile){?>
                                                <option value="<?=$profile;?>"><?=$profile;?></option>
											<?}?>
                                        </select>
                                    </div>
                                </div>
                                <? } ?>
								<div class="col">
									<div class="form-group">
										<input type="number" min="1" class="form-control input-sm input-form-quantity" value="0" name="quantity[]">
									</div>
								</div>
								<div class="col">
									<div class="form-group">
										<input type="text" class="form-control input-sm touchspin input-form-thickness" value="0" name="thickness[]">
									</div>
								</div>
								<div class="col">
									<div class="form-group">
										<input type="text" class="form-control input-sm touchspin input-form-width" value="0" name="width[]">
									</div>
								</div>
								<div class="col">
									<div class="form-group">
										<input type="text" class="form-control input-sm touchspin input-form-length" value="0" name="length[]">
									</div>
								</div>
								<div class="col">
									<div class="form-group">
										<input type="text" class="form-control input-sm touchspin input-form-volume-m3" value="0" name="volume_m3[]">
									</div>
								</div>
							</div>
							<?php
						}
						?>
					</div>
					<a href="javascript:void(0);" class="add-more-products-complete">Добавить еще</a>
					<div class="d-flex justify-content-end">
					<? if($routes['2'] == 2){?>
                        <select name="select_color" class="form-control select2-active hide-input" data-placeholder="Выбрать цвет" required>
							<?php
							foreach ($data['color'] as $color) {
								?>
                                <option value="<?= $color['id']; ?>"><?= $color['name']; ?></option>
								<?php
							}
							?>
                        </select>
                        <button type="submit" class="btn btn-warning waves-effect btn-add-consignment-products-paint" data-msg="Товары созданы и отправлены на покраску!">
                            <span class="button-text">Отправить на покраску</span>
                            <div class="button-wait" style="display: none;">
                                <i class="fas fa-spinner fa-spin"></i>
                            </div>
                        </button>
						<? } ?>
						<button type="submit" class="btn btn-success waves-effect btn-add-consignment-products" data-msg="<?=$data['btn_text_post'];?>">
							<span class="button-text"><?=$data['btn_text'];?></span>
							<div class="button-wait" style="display: none;">
								<i class="fas fa-spinner fa-spin"></i>
							</div>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>