<!-- 
	************************* 
			Таблицы
	*************************  -->
<?php
/*echo "<pre>";
print_r($data['consignments']);
echo "</pre>";*/
if (!empty($data['consignments'])) {
foreach ($data['consignments'] as $consignments) {
	if(empty($consignments['raw_materials']))continue;
	?>
	<div class="card-box">

		<input type="hidden" name="consignment_id" value="<?= $consignments['id']; ?>">
		<div class="d-flex justify-content-between align-items-start mb-2">
			<div>
				<!-- h5 class="mt-0 text-body">№ <?= $consignments['id']; ?></h5 -->
				<a href="#" class="show-form-update-description-consignment" data-text="Добавить описание"><?= !empty($consignments['description']) ? $consignments['description'] : 'Добавить описание'; ?></a>
				<div class="row d-none form-update-description-consignment">
					<div class="col-sm-12">
						<form name="update_description_consignment" method="post" class="form-inline" enctype="multipart/form-data">
							<div class="form-group">
								<label class="sr-only" for="consignment_description_<?= $consignments['id']; ?>">Описание</label>
								<textarea  class="form-control input-sm" name="description" id="consignment_description_<?= $consignments['id']; ?>" placeholder="Описание" cols="50" rows="3"><?= $consignments['description']; ?></textarea>
								<!-- <input type="text"> -->
							</div>
							<button type="submit" class="btn btn-sm btn-success waves-effect waves-light m-l-10 btn-md btn-update-description-consignment" data-id="<?= $consignments['id']; ?>">Сохранить</button>
						</form>
					</div>
				</div>
			</div>
			<div <?= $routes[2] == '4' ? 'style="min-width:200px;"' : '' ?> class='select_status_consigment-block'>
				<?php
				// if ($routes[2] == '2' || $routes[2] == '4' || $routes[2] == '7' || $routes[2] == '8') {
				if (false) {
					?>
					<button class="btn btn-icon waves-effect waves-light btn-danger btn-xs btn-delete-consignment" data-id="<?= $consignments['id']; ?>"><i class="fas fa-trash"></i></button>
					<?php
				}
				if ($routes[2] == '4') {
					$status_repair_1 = $status_repair_2 = $status_repair_3 = '';
					if ($consignments['status_repair'] == '1') {
						$status_repair_1 = 'selected';
					} elseif ($consignments['status_repair'] == '2') {
						$status_repair_1 = 'disabled';
						$status_repair_2 = 'selected';
					} elseif ($consignments['status_repair'] == '3') {
						$status_repair_1 = $status_repair_2 = 'disabled';
						$status_repair_3 = 'selected';
					}
					?>
					<select name="select_status_consigment" class="form-control select2 select_status_consigment" data-id="<?= $consignments['id']; ?>" data-placeholder="Выбрать статус" required>
						<option></option>
						<option value="1" <?= $status_repair_1; ?>>Ожидает подтверждения</option>
						<option value="2" <?= $status_repair_2; ?>>В процессе покраски</option>
						<option value="3" <?= $status_repair_3; ?>>Покрашено</option>
					</select>
					<?php
				}
				?>
			</div>
		</div>
		<div class="row p-3 m-3">
			<div class="col-md-3">
				<p><?= $consignments['date_create_str']; ?></p>
			</div>
			  <?php if ($routes[2] == '3') { ?>
		<?= !empty($consignments['title']) ? '<h3 class="production-title text-center col-md-7">'.$consignments['title'].'</h3><input type="text" value="'.$consignments['title'].'" name="" class="production-title-inp d-none col-md-2 offset-md-2">' : '<input type="text" value="" name="" class="production-title-inp d-none col-md-2  offset-md-2">'; ?>
				<? } ?>
		</div>
		<div class="d-flex justify-content-between mb-2 pt-4">
  <?php if ($routes[2] == '9') { ?>
  			<div class="form-block d-none">
  				<form method="post" action="" class="consignment_edit_form">
  					<input type="hidden" name="cons_id" value="<?= $consignments['id']; ?>">
  				<div class="row">
			        <div class="col-2">
			            <div class="form-group">
			                <label for="breed_select">№ партии</label>
			                <input type="text" value="<?=$consignments['postavka_nomer'];?>" class="form-control input-sm postavka_nomer" name="postavka_nomer">
			            </div>
			        </div>
			        <div class="col-2">
			            <div class="form-group">
			                <label for="breed_select">Поставщик</label>
			                <input type="text" value="<?=$consignments['postavka_postavsik'];?>" class="form-control input-sm postavka_postavsik" name="postavka_postavsik">
			            </div>
			        </div>
			        <div class="col-2">
			            <div class="form-group">
			                <label for="breed_select">Диспетчер</label>
			                <input type="text" value="<?=$consignments['postavka_dispecer'];?>" class="form-control input-sm postavka_dispecer" name="postavka_dispecer">
			            </div>
			        </div>
			        <div class="col-3">
			            <div class="form-group">
			                <label for="breed_select">Адрес поставки</label>
			                <input type="text" value="<?=$consignments['postavka_adres'];?>" class="form-control input-sm postavka_adres" name="postavka_adres">
			            </div>
			        </div>
			        <div class="col-3">
			            <div class="form-group">
			                <label for="breed_select">Дата отгрузки план</label>
			                <input type="text" value="<?=$consignments['postavka_dataop'];?>" class="form-control input-sm datepicker postavka_dataop" name="postavka_dataop">
			            </div>
			        </div>
			        <div class="col-3">
			            <div class="form-group">
			                <label for="breed_select">Дата отгрузки факт</label>
			                <input type="text" value="<?=$consignments['postavka_dataof'];?>" class="form-control input-sm datepicker postavka_dataof" name="postavka_dataof">
			            </div>
			        </div>
			        <div class="col-3">
			            <div class="form-group">
			                <label for="breed_select">Дата поставки план</label>
			                <input type="text" value="<?=$consignments['postavka_datapp'];?>" class="form-control input-sm datepicker postavka_datapp" name="postavka_datapp">
			            </div>
			        </div>
			        <div class="col-3">
			            <div class="form-group">
			                <label for="breed_select">Дата поставки факт</label>
			                <input type="text" value="<?=$consignments['postavka_datapf'];?>" class="form-control input-sm datepicker postavka_datapf" name="postavka_datapf">
			            </div>
			        </div>
			        <div class="col-4">
			            <div class="form-group">
			                <label for="breed_select">Марка а/м</label>
			                <input type="text" value="<?=$consignments['postavka_markaam'];?>" class="form-control input-sm postavka_markaam" name="postavka_markaam">
			            </div>
			        </div>
			        <div class="col-4">
			            <div class="form-group">
			                <label for="breed_select">№ а/м</label>
			                <input type="text" value="<?=$consignments['postavka_nomeram'];?>" class="form-control input-sm postavka_nomeram" name="postavka_nomeram">
			            </div>
			        </div>
			        <div class="col-4">
			            <div class="form-group">
			                <label for="breed_select">Объем, м3</label>
			                <input type="text" value="<?=$consignments['postavka_obiem'];?>" class="form-control input-sm postavka_obiem" name="postavka_obiem">
			            </div>
			        </div>
				    </form>
			    </div>
  			</div>

  			<div class="cons-params-block d-flex flex-row">
				<div class="p-3">
					<?= !empty($consignments['postavka_nomer'])?'№ партии: '.$consignments['postavka_nomer'].'<br>':''; ?>
					<?= !empty($consignments['postavka_postavsik'])?'Поставщик: '.$consignments['postavka_postavsik'].'<br>':''; ?>
					<?= !empty($consignments['postavka_dispecer'])?'Диспетчер: '.$consignments['postavka_dispecer'].'<br>':''; ?>
					<?= !empty($consignments['postavka_adres'])?'Адрес поставки: '.$consignments['postavka_adres'].'<br>':''; ?>
					
				</div>
				<div class="p-3">
					<?= !empty($consignments['postavka_dataop'])?'дата отгрузки план: '.$consignments['postavka_dataop'].'<br>':''; ?>
					<?= !empty($consignments['postavka_dataof'])?'дата отгрузки факт: '.$consignments['postavka_dataof'].'<br>':''; ?>
					<?= !empty($consignments['postavka_datapp'])?'дата поставки план: '.$consignments['postavka_datapp'].'<br>':''; ?>
					<?= !empty($consignments['postavka_datapf'])?'дата поставки факт: '.$consignments['postavka_datapf'].'<br>':''; ?>
				</div>
				<div class="p-3">
					<?= !empty($consignments['postavka_markaam'])?'марка а/м: '.$consignments['postavka_markaam'].'<br>':''; ?>
					<?= !empty($consignments['postavka_nomeram'])?'№ а/м: '.$consignments['postavka_nomeram'].'<br>':''; ?>
					<?= !empty($consignments['postavka_obiem'])?'объем, м3: '.$consignments['postavka_obiem'].'<br>':''; ?>
				</div>
			</div>
  <?php } ?>
			<div class="">
				<?= !empty($consignments['tocolor_maslo'])?'Масло: '.$consignments['tocolor_maslo'].'<br>':''; ?>
				<?= !empty($consignments['tocolor_select_color'])?'Цвет: '.$consignments['tocolor_select_color'].'<br>':''; ?>
				<?= !empty($consignments['tocolor_sides'])?'Количество сторон: '.$consignments['tocolor_sides'].'<br>':''; ?>
				<?= !empty($consignments['tocolor_toretc'])?'Торцы: '.$consignments['tocolor_toretc'].'<br>':''; ?>
			</div>
			<div class="">
				<?= !empty($consignments['tocolor_face'])?'Лицевая сторона: '.$consignments['tocolor_face'].'<br>':''; ?>
				<?= !empty($consignments['tocolor_back'])?'Обратная сторона: '.$consignments['tocolor_back'].'<br>':''; ?>
				<?= !empty($consignments['tocolor_zakazchik'])?'Заказчик: '.$consignments['tocolor_zakazchik'].'<br>':''; ?>
				<?= !empty($consignments['tocolor_remont_shpaklevka'])?'Ремонт/Шпаклевка: '.$consignments['tocolor_remont_shpaklevka'].'<br>':''; ?>
			</div>
			<div style="min-width:200px;">
				<!-- изменить дату окончания сушки покраски -->
				<? if(in_array($routes[2], [4, 7])){?>
			    <div class="row edit-etap-date d-none">
			        <div class="col-6">
			            <div class="form-group">
			                <label for="breed_select">Дата завершения</label>
			                <input type="text" class="form-control input-sm datepicker date_finish" name="date_finish">
			            </div>
			        </div>
			    </div>
			    <?}?>

				<?php
				// Строгание Сушка Термирование + На покраске
				if (in_array($routes[2], [2, 7,8 , 4])) {
					$date_finish = ($consignments['date_finish'] - $consignments['date_create']) / (60 * 60 * 24);
					$counter_date = ($consignments['date_finish'] - time()) / (60 * 60 * 24);

					if ($date_finish > 0 && $date_finish < 1) {
						$date_finish = 2;
						$date_finish_str = '< 1';
					} else $date_finish_str = floor($date_finish);

					if ($counter_date > 0 && $counter_date < 1) {
						$counter_date = 2;
						$counter_date_str = '< 1';
					} else $counter_date_str = floor($counter_date);
					?>
					<?= $routes[2] == '7' ? 'Выход из камеры:' : 'Срок производства:' ?> <strong class="text-primary">
						<? 
						// var_dump($date_finish_str);
						// var_dump($date_finish);
						// var_dump(date('Y-m-d H:i:s', $consignments['date_finish']));
						?>
						<?=date('Y-m-d', $consignments['date_finish']);?>
						<?//= $date_finish_str ?> <?//= StandartRus\App\Core\Controller::RusEnding($date_finish, "день", "дня", "дней"); ?></strong>
					<span class="ml-3">Осталось</span>: <strong class="text-primary"><?= $counter_date_str; ?>  <?= StandartRus\App\Core\Controller::RusEnding($counter_date, "день", "дня", "дней"); ?></strong>
					<?php
				}
				?>
			</div>
		</div>


<h3 class="d-none text-center text-danger title_form_consignment_edit"><?= $routes[2] == '2' ? 'Введите получившиеся параметры' : '' ?></h3>

  	<!-- Выбрать все -->
  		<div class="form-group check_all_box d-none">
			<div class="checkbox checkbox-primary pl-2">
				<input id="check_all_<?=$consignments['id'];?>" type="checkbox" class=" check_all_in_group" data-id="<?=$consignments['id'];?>">
				<label for="check_all_<?=$consignments['id'];?>" class="check_all">Выбрать все</label>
			</div>
		</div>

<!-- сама таблица -->
<? include 'consignment_edit.php';?>

		<?php
		//if ($routes[2] == '1') {
			?>
			<!-- <div class="text-right mt-4">
				<button class="btn btn-sm waves-effect waves-light btn-success btn-add-row-table-raw-material d-none" data-id="<?= $consignments['id']; ?>"><i class="fas fa-plus mr-2"></i>Добавить позицию</button>
			</div> -->
			<?php
		//}
		?>
		<?php
		/*if ($routes[2] == '2' || $routes[2] == '7' || $routes[2] == '8' || $routes[2] == '4') {
			?>
			<div class="text-right mt-4">
				<button type="button" class="btn btn-warning waves-effect btn-show-form-success-manufacture-raw-materials" data-id="<?= $consignments['id']; ?>">Завершить</button>
			</div>
			<?php
		}*/
$editTableText = 'Изменить группу';
if ($routes[2] == '2') $editTableText = 'Завершить производство';
		?>
			<button type="button" class="btn btn-edit-table" data-id="<?= $consignments['id']; ?>"><?= $editTableText ?></button>
			<div class="text-right mt-4 edit-table-save d-none">
				<button type="button" class="btn btn-danger waves-effect btn-edit-table-delete" data-id="<?= $consignments['id']; ?>">Удалить</button>
    
				<button type="button" class="btn btn-danger waves-effect btn-edit-table-cancel" data-id="<?= $consignments['id']; ?>">Отменить</button>
				<button type="button" class="btn btn-success waves-effect btn-edit-table-save" data-id="<?= $consignments['id']; ?>">Сохранить изменения</button>
				<button class="btn btn-success waves-effect btn-add-row-table-raw-material" data-id="<?= $consignments['id']; ?>"><i class="fas fa-plus mr-2"></i>Добавить позицию</button>
			</div>
		<?php
		if ($routes[2] == '1') {
			?>
			<div class="row mt-4 d-none">
				<div class="col-sm-12">
					<h6>Стоимость партии</h6>
					<form name="update_consignment" method="post" class="form-inline" enctype="multipart/form-data">
						<div class="form-group">
							<label class="sr-only" for="consignment_cost">Стоимость партии</label>
							<input type="number" class="form-control input-sm" id="consignment_cost" placeholder="Стоимость партии">
						</div>
						<button type="submit" class="btn btn-sm btn-success waves-effect waves-light m-l-10 btn-md btn-update-consignment" data-id="<?= $consignments['id']; ?>" data-msg="Стоимость установлена!">
							<span class="button-text">Сохранить</span>
							<div class="button-wait" style="display: none;">
								<i class="fas fa-spinner fa-spin"></i>
							</div>
						</button>
					</form>
				</div>
			</div>
			<?php
		}
		?>
	</div>
	<?php
}
}
?>