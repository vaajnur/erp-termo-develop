<!-- ***************************** 
	Изменить группу / Завершить производство 
	*****************************  -->

<div class="table-responsive">
	<table data-consignment="<?=$consignments['id'];?>" class="table table-striped table-bordered m-0 small">
		<thead>
		<tr>
			<th class="manufacture-form border-0 bg-white d-none"></th>
            <th>Сорт</th>
			<th>Порода</th>
            <th>Профиль</th>
			<th>Кол-во, шт.</th>
			<th>Толщина, мм.</th>
			<th>Ширина, мм.</th>
			<th>Длина, мм.</th>
			<th>м<sup>2</sup></th>
			<th>м<sup>3</sup></th>
			<th>Погонный метр</th>
			<?php
			if ($routes[2] != '2' && $routes[2] != '7' && $routes[2] != '8') {
				?>
				<th class="bg-transparent border-0 pr-0 td-4-btn" style="width:70px;"></th>
				<?php
			}
			?>
		</tr>
		</thead>
		<tbody>
		<?php
		$itogo = array();
		foreach ($consignments['raw_materials'] as $row_material) {

			$volume_m3 = ($row_material['quantity'] * $row_material['width'] / 1000 * $row_material['length'] / 1000 * $row_material['thickness'] / 1000) / 1000000;
			$volume_m3_x1 = ($row_material['width'] / 1000 * $row_material['length'] / 1000 * $row_material['thickness'] / 1000) / 1000000;

			$volume_m2 = ($row_material['quantity'] * $row_material['width'] / 1000 * $row_material['length'] / 1000) / 1000000;
			$volume_m2_x1 = ( $row_material['width'] / 1000 * $row_material['length'] / 1000) / 1000000;
			if ($row_material['volume_m3'] > 0) $volume_m3 = $row_material['volume_m3'];
			// Погонный метр
			$pogonni_metr = ($row_material['quantity'] * $row_material['length'] / 1000) / 1000;
			$itogo['quantity'] += intval($row_material['quantity']);
			$itogo['volume_m3'] += $volume_m3;
			$itogo['volume_m2'] += $volume_m2;
			$itogo['pogonni_metr'] += $pogonni_metr;
			// $this->f12('123');
			?>
			<tr class="text-dark">
				<?php
				//if ($routes[2] == '1') {
					?>
					<td class="manufacture-form border-0 bg-white d-none pl-0" style="width: 120px;">
						<div class="form-inline">
							<div class="form-group">
								<div class="checkbox checkbox-primary pl-2">
									<input id="input_checkbox_<?= $row_material['id']; ?>" type="checkbox" class="checkbox-raw-material" data-id="<?= $row_material['id']; ?>">
									<label for="input_checkbox_<?= $row_material['id']; ?>">&nbsp;</label>
								</div>
							</div>
							<div class="form-group">
								<input type="number" min="0" max="<?= $row_material['quantity']; ?>" class="form-control input-sm input-count-raw-material" style="width: 60px;" name="input_count_raw_material" value="<?= $row_material['quantity']; ?>" required disabled>
							</div>
						</div>
					</td>
				<td>
					<span class="edit-value"><?= $row_material['name']; ?></span>
					<div class="hide-input d-none">
						<select name="input_name" class="form-control select2 cort-select d-none hide-input" data-placeholder="" required>
	                        <?php 
	                        if(!empty($data['cort'])){
	                        foreach($data['cort'] as $cort){?>
	                            <option <?php if($cort['name'] == $row_material['name']){?>selected<?}?> value="<?=$cort['name'];?>"><?=$cort['name'];?></option>
	                        <?}}?>
	                    </select>
	                </div>
				</td>
                <!-- Порода -->
                <td>
	                <?php 
	                if(!empty($data['breed'])){
	                foreach($data['breed'] as $breed){?>
	                  <?php if($breed['id'] == $row_material['breed']){?>
		                    <span class="edit-value"><?=$breed['name'];?></span>
	                  <?}?>
	                <?}}?>
                    <div class="hide-input d-none">
                        <select name="breed" class="form-control select2 breed-select d-none hide-input" data-placeholder="" required>
                            <?php 
                            if(!empty($data['breed'])){
                            foreach($data['breed'] as $breed){?>
                                <option <?php if($breed['id'] == $row_material['breed']){?>selected<?}?> value="<?=$breed['id'];?>"><?=$breed['name'];?></option>
                            <?}}?>
                        </select>
                    </div>
                </td>				
                <!-- Профиль -->
                <td>
                    <span class="edit-value"><?= $row_material['profile']; ?></span>
                    <div class="hide-input d-none">
                        <select name="profile" class="form-control select2 profile-select d-none hide-input" data-placeholder="">
                            <?php 
                            if(!empty($data['profiles'])){
                            foreach($data['profiles'] as $profile){?>
                                <option <?php if($profile == $row_material['profile']){?>selected<?}?> value="<?=$profile;?>"><?=$profile;?></option>
                            <?}}?>
                        </select>
                    </div>
                </td>
				<td>
					<span class="edit-value"><?= $row_material['quantity']; ?></span>
					<input type="number" min="1" class="form-control input-sm hide-input d-none input-consignment-quantity _req" name="input_quantity" value="<?= $row_material['quantity']; ?>" required>
				</td>
				<td>
					<span class="edit-value"><?= number_format($row_material['thickness'] / 1000, 0, '.', ' '); ?></span>
					<input type="text" class="form-control input-sm hide-input touchspin-input d-none input-consignment-thickness" name="input_thickness" value="<?= $row_material['thickness'] / 1000; ?>" required>
				</td>
				<td>
					<span class="edit-value"><?= number_format($row_material['width'] / 1000, 0, '.', ' '); ?></span>
					<input type="text" class="form-control input-sm hide-input touchspin-input d-none input-consignment-width" name="input_width" value="<?= $row_material['width'] / 1000; ?>" required>
				</td>
				<td>
					<span class="edit-value"><?= number_format($row_material['length'] / 1000, 0, '.', ' '); ?></span>
					<input type="text" class="form-control input-sm hide-input touchspin-input d-none input-consignment-length" name="input_length" value="<?= $row_material['length'] / 1000; ?>" required>
				</td>
				<!-- площадь м2 -->
				<td>
					<div data-volume-m2-x1="<?=$volume_m2_x1;?>" class="edit-value volume_m2"><?= number_format($volume_m2, 3, '.', ' ') ; ?></div>
				</td>
				<!-- объем м3 -->
				<td>
					<span data-volume-m3-x1="<?=$volume_m3_x1 / 1000;?>" class="edit-value volume_m3"><?= number_format($volume_m3 / 1000, 3, '.', ' '); ?></span>
					<!-- <input type="text" class="form-control input-sm hide-input touchspin-input d-none <?= $row_material['volume_m3'] > 0 ? '' : 'input-consignment-volume-m3' ?>" name="input_volume_m3" value="<?= $volume_m3 / 1000; ?>" required> -->
				</td>
				<!-- погонный метр -->
				<td>
					<div class="edit-value pogonni-metr"><?= number_format($pogonni_metr, 3, '.', ' ') ; ?></div>
				</td>
				<td class="bg-white border-0 text-right pr-0 td-4-btn">
					<button class="btn btn-icon waves-effect waves-light btn-danger btn-xs btn-delete-raw-material d-none" data-id="<?= $row_material['id']; ?>"><i class="fas fa-trash"></i></button>
					<button type="button"  data-id="<?= $row_material['id']; ?>" class="btn btn-sm btn-light ml-3 btn-copy-raw-material-inedit d-none"><i class="fas fa-copy"></i></button>
					<!-- button class="btn btn-icon waves-effect waves-light btn-primary btn-xs btn-edit-raw-material" data-id="<?= $row_material['id']; ?>"><i class="fas fa-edit"></i></button>
					<button class="btn btn-icon waves-effect waves-light btn-danger btn-xs btn-cancel-edit-raw-material d-none" data-id="<?= $row_material['id']; ?>"><i class="fas fa-times"></i></button>
					<button class="btn btn-icon waves-effect waves-light btn-success btn-xs btn-save-raw-material d-none" data-id="<?= $row_material['id']; ?>"><i class="fas fa-check"></i></button -->
				</td>
			</tr>
			<?php
		}
		?>
		 <tr class="tr-summ">
		 	<td class="summ-first-cell d-none"></td>
            <td class="font-weight-bold">Итог</td>
			<?php
			if ($routes[2] == '1' || $routes[2] == '2' /*|| $routes[2] == '4'*/) { /*?>
            <td></td>
            <? */} ?>
			<?php
			if ($routes[2] != '1' && $routes[2] != '2') { ?>
                <!-- <td></td> -->
			<?php } ?>
            <td></td>
            <? //if(!in_array($routes[2], [2, 7,8])){?>
            <td></td>            	
            <? //} ?>
            <td><?=$itogo['quantity'];?></td>
            <td></td>
            <td></td>
            <td></td>
            <td><?=number_format($itogo['volume_m2'], 3, '.', ' ');?></td>
            <td><?= number_format($itogo['volume_m3'] / 1000, 3, '.', ' '); ?></td>
            <td><?= number_format($itogo['pogonni_metr'], 3, '.', ' '); ?></td>
            <td style="display:none"></td>
        </tr>
		</tbody>
	</table>
</div>