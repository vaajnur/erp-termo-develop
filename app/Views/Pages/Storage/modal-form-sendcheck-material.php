<!-- модалка Отправки на другой склад -->
<div class="modal fade modal-form-sendcheck-material" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title mt-0">Подтверждение отправки</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
            <table class="table table-striped table-bordered m-0 small table-sendcheck">
            </table>
            <button type="submit" class="btn btn-warning waves-effect btn-form-send-raw-materials-to-manufacture" data-msg="Партия отправлена!">
                <span class="button-text">Отправить</span>
                <div class="button-wait" style="display: none;">
                    <i class="fas fa-spinner fa-spin"></i>
                </div>
            </button>
			</div>
		</div>
	</div>
</div>