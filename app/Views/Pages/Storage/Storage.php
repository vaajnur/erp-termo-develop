<!-- Start content -->
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="page-title-box">
					<h6>Склад</h6>
					<h4 class="page-title">Склады</h4>
					<ol class="breadcrumb p-0 m-0">
						<li class="breadcrumb-item">
							<a href="/">Главная</a>
						</li>
						<li class="breadcrumb-item active">
							Склады
						</li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="row">
			<?php
			foreach ($storage_list as $storage) {
				?>
				<div class="col-3">
					<div class="card">
						<div class="card-header">
							<?= $storage['name']; ?>
						</div>
						<div class="card-body">
							<a href="/storage/<?= $storage['id']; ?>/" class="waves-effect"><span>Перейти</span></a>
						</div>
					</div>
				</div>
				<?php
			}
			?>
		</div>
	</div> <!-- container-fluid -->
</div> <!-- content -->