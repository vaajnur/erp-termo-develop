<!-- Добавить строку при редактировании -->
<table class="d-none">
	<!-- <tr><td colspan=10>template_row_raw_material</td></tr> -->
	<tr class="text-dark d-none" id="template_row_raw_material">
		<?php
		//if ($routes[2] == '1') {
			?>
			<td class="manufacture-form border-0 bg-white d-none pl-0" style="width: 120px;">
				<div class="form-inline">
					<div class="form-group">
						<div class="checkbox checkbox-primary pl-2">
							<input id="input_checkbox" type="checkbox" class="checkbox-raw-material" data-id="0">
							<label for="input_checkbox">&nbsp;</label>
						</div>
					</div>
					<div class="form-group">
						<input type="number" min="0" class="form-control input-sm input-count-raw-material" style="width: 60px;" name="input_count_raw_material" value="0" required disabled>
					</div>
				</div>
			</td>
		<td class="select-td">
			<span class="edit-value d-none"></span>
			<select name="input_name" class="form-control select2 cort-select d-none hide-input" data-placeholder="" required>
				<option value=""></option>
                <?php foreach($data['cort'] as $cort){?>
                    <option value="<?=$cort['name'];?>"><?=$cort['name'];?></option>
                <?}?>
            </select>
		</td>
		<td class='select-td'>
            <select name="breed" class="form-control select2 breed-select" data-placeholder="" required>
                    <option></option>
				<?php foreach($data['breed'] as $breed){?>
                    <option value="<?=$breed['id'];?>"><?=$breed['name'];?></option>
				<?}?>
            </select>
		</td>
		<!-- new added -->
		<td class='select-td'>
			<select name="profile" class="form-control select2 profile-select" data-placeholder="">
			    <option value=""></option>
			    <?php foreach($data['profiles'] as $profile){?>
			        <option value="<?=$profile;?>"><?=$profile;?></option>
			    <?}?>
			</select>
		</td>
		<td>
			<span class="edit-value d-none"></span>
			<input type="number" min="1" class="form-control input-sm hide-input input-consignment-quantity" name="input_quantity" value="0" required>
		</td>
		<td>
			<span class="edit-value d-none"></span>
			<input type="text" class="form-control input-sm hide-input touchspin-input input-consignment-thickness" name="input_thickness" value="0" required>
		</td>
		<td>
			<span class="edit-value d-none"></span>
			<input type="text" class="form-control input-sm hide-input touchspin-input input-consignment-width" name="input_width" value="0" required>
		</td>
		<td>
			<span class="edit-value d-none"></span>
			<input type="text" class="form-control input-sm hide-input touchspin-input input-consignment-length" name="input_length" value="0" required>
		</td>
		<td><span class="volume_m2">0.00</span></td>
		<td><span class="volume_m3">0.00</span></td>
		<!-- <td>
			<span class="edit-value d-none"></span>
			<input disabled="" type="text" class="form-control input-sm hide-input touchspin-input input-consignment-volume-m3" name="input_volume_m3" value="0" required>
		</td> -->
		<td>
			<span class="pogonni-metr">0.00</span>
		</td>

		<td class="bg-white border-0 text-right pr-0">
			<button class="btn btn-icon waves-effect waves-light btn-danger btn-xs btn-delete-raw-material" data-id="0"><i class="fas fa-trash"></i></button>
			<button type="button"  data-id="" class="btn btn-sm btn-light ml-3 btn-copy-raw-material-inedit"><i class="fas fa-copy"></i></button>
			<!-- <button class="btn btn-icon waves-effect waves-light btn-primary btn-xs btn-edit-raw-material d-none" data-id="0"><i class="fas fa-edit"></i></button>
			<button class="btn btn-icon waves-effect waves-light btn-danger btn-xs btn-delete-raw-material d-none" data-id="0"><i class="fas fa-trash"></i></button>
			<button class="btn btn-icon waves-effect waves-light btn-danger btn-xs btn-cancel-edit-raw-material d-none" data-id="0"><i class="fas fa-times"></i></button>
			<button class="btn btn-icon waves-effect waves-light btn-success btn-xs btn-save-raw-material" data-id="0"><i class="fas fa-check"></i></button> -->
		</td>

	</tr>
</table>