	<!-- ********************************
				На покраску 
	**************************** -->
<div class="modal fade modal-form-tocolor-material" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title mt-0">Укажите параметры покраски</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form name="tocolor_products" id="tocolor_products" method="post" enctype="multipart/form-data">
					<div class="row">
						<div class="col-2">
							<div class="form-group">
								<label for="breed_select">Масло</label>
								<select name="maslo" id="tocolor_maslo_popup" class="form-control select2" data-placeholder="Выбрать Масло" required>
									<option></option>
									<?php
									foreach ($data['maslo'] as $maslo) {
										?>
										<option value="<?= $maslo['id']; ?>"><?= $maslo['name']; ?></option>
										<?php
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-2">
							<div class="form-group">
								<label for="tint_select">Цвет</label>
                <select name="select_color" id="tocolor_color_popup" class="form-control select2-active hide-input select2" data-placeholder="Выбрать цвет" required>
                <?php
                foreach ($data['color'] as $color) {
                  ?>
                                  <option value="<?= $color['id']; ?>"><?= $color['name']; ?></option>
                  <?php
                }
                ?>
                </select>
							</div>
						</div>
						<div class="col-4">
							<div class="form-group">
								<label for="breed_select">Количество сторон</label>
								<select name="sides" id="tocolor_sides_popup" class="form-control select2" data-placeholder="Выбрать Количество сторон" required>
									<option></option>
									<?php
									foreach (array(1,2,3,4) as $breed) {
										?>
										<option value="<?= $breed; ?>"><?= $breed; ?></option>
										<?php
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-2">
							<div class="form-group">
								<label for="breed_select">Торцы</label>
								<select name="toretc" id="tocolor_toretc_popup" class="form-control select2" data-placeholder="Выбрать Торцы" required>
									<option></option>
									<?php
									foreach (array('Да','Нет') as $breed) {
										?>
										<option value="<?= $breed; ?>"><?= $breed; ?></option>
										<?php
									}
									?>
								</select>
							</div>
						</div>
          </div>
					<div class="row">
						<div class="col-4">
							<div class="form-group">
								<label for="breed_select">Лицевая сторона</label>
								<select name="face" id="tocolor_face_popup" class="form-control select2" data-placeholder="Выбрать Лицевая сторона" required>
									<option></option>
									<?php
									foreach (array(0,1,2,3,4,5) as $breed) {
										?>
										<option value="<?= $breed; ?>"><?= $breed; ?> слоя</option>
										<?php
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-4">
							<div class="form-group">
								<label for="breed_select">Обратная сторона</label>
								<select name="back" id="tocolor_back_popup" class="form-control select2" data-placeholder="Выбрать Обратная сторона" required>
									<option></option>
									<?php
									foreach (array(0,1,2,3) as $breed) {
										?>
										<option value="<?= $breed; ?>"><?= $breed; ?> слоя</option>
										<?php
									}
									?>
								</select>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-6">
							<div class="form-group">
								<label for="">Заказчик</label>
								<input class="form-control tocolor_zakazchik" type="text" name="zakazchik" value="">
							</div>
						</div>
						<div class="col-6">
							<div class="form-group">
								<label for="">Ремонт/шпаклевка</label>
								<select id="tocolor_remont_shapklevka" class="form-control select2" name="remont_shpaklevka" id="">
									<option value="1">Да</option>
									<option value="0">Нет</option>
								</select>
							</div>
							
						</div>
					</div>

					<div class="d-flex justify-content-end">
              <button type="submit" class="btn btn-warning waves-effect btn-form-send-raw-materials-to-manufacture" data-msg="Товары созданы и отправлены на покраску!">
                  <span class="button-text">Отправить на покраску</span>
                  <div class="button-wait" style="display: none;">
                      <i class="fas fa-spinner fa-spin"></i>
                  </div>
              </button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>