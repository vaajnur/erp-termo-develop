<!-- Добавить строку материала при добавлении группы  -->

<div class="row d-none" id="row_raw_materials">
	<div class="col select-td">
		<div class="form-group">
			<select name="name[]" class="form-control select2 profile-select d-none hide-input" data-placeholder="" required>
                <?php foreach($data['cort'] as $cort){?>
                	<option value=""></option>
                    <option <?php if($cort == $row_material['cort']){?>selected<?}?> value="<?=$cort['name'];?>"><?=$cort['name'];?></option>
                <?}?>
            </select>
		</div>
	</div>
	<!-- Порода (Добавить)-->
    <div class="col select-td">
        <div class="form-group">
            <select name="breed[]" class="form-control select2 breed-select" data-placeholder="" required>
                    <option></option>
				<?php foreach($data['breed'] as $breed){?>
                    <option value="<?=$breed['id'];?>"><?=$breed['name'];?></option>
				<?}?>
            </select>
        </div>
    </div>
    <div class="col select-td">
        <div class="form-group">
            <select name="profile[]" class="form-control select2 profile-select" data-placeholder="">
            	<option value=""></option>
                <?php foreach($data['profiles'] as $profile){?>
                <option value="<?=$profile;?>"><?=$profile;?></option>
                <?}?>
            </select>
        </div>
    </div>
	<div class="col-1">
		<div class="form-group">
			<input type="number" min="1" class="form-control input-sm input-form-quantity" value="0" name="quantity[]" required>
		</div>
	</div>
	<div class="col">
		<div class="form-group">
			<input type="text" class="form-control input-sm touchspin-input input-form-thickness" value="0" name="thickness[]" required>
		</div>
	</div>
	<div class="col">
		<div class="form-group">
			<input type="text" class="form-control input-sm touchspin-input input-form-width" value="0" name="width[]" required>
		</div>
	</div>
	<div class="col">
		<div class="form-group">
			<input type="text" class="form-control input-sm touchspin-input input-form-length" value="0" name="length[]" required>
		</div>
	</div>
	<div class="col-1">
		<div class="form-group">
			<span class="volume_m2">0.00</span>
		</div>
	</div>
	<div class="col-2 d-flex">
		<div class="form-group">
			<span class="volume_m3">0.00</span>
			<!-- <input type="text" class="form-control input-sm touchspin-input input-form-volume-m3" value="0" name="volume_m3[]"> -->
		</div>
		<div class="col">
			<button type="button" class="btn btn-sm btn-light ml-3 btn-copy-raw-material"><i class="fas fa-copy"></i></button>
		</div>
		<div class="col">
			<button type="button" class="btn btn-icon waves-effect waves-light btn-danger btn-xs btn-delete-raw-material-new"><i class="fas fa-trash"></i></button>
		</div>
        <!-- <div>
            <button type="button" class="btn btn-sm btn-light ml-3 btn-copy-raw-material"><i class="fas fa-copy"></i></button>
        </div> -->
	</div>
</div>