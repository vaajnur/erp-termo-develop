<!-- *********************************
		 Добавить новую отгрузку
 ************************************** -->
<form autocomplete="off" name="add_consignment" method="post" class="mb-4 d-none" enctype="multipart/form-data">
    <input type="hidden" name="storage" value="<?= $routes[2]; ?>">
    <?php if ($routes[2] == '3') { ?>
        <div class="row">
            <div class="col-2">
                <div class="form-group">
                    <label for="breed_select">Заголовок</label>
                    <input type="text" class="form-control input-sm title" name="title">
                </div>
            </div>
        </div>
    <? } ?>
    <?php if ($routes[2] == '9') { ?>
    <input type="hidden" name="tint" value="1">
    <div class="row">
        <!-- <div class="col-3">
            <div class="form-group">
                <label for="breed_select">Порода</label>
                <select name="breed" id="breed_select" class="form-control select2 breed-select" data-placeholder="Выбрать породу" required>
                    <option></option>
                    <?php
					foreach ($data['breed'] as $breed) {
						?>
                    <option value="<?= $breed['id']; ?>">
                        <?= $breed['name']; ?>
                    </option>
                    <?php
					}
					?>
                </select>
            </div>
        </div> -->
        <div class="col-2">
            <div class="form-group">
                <label for="breed_select">№ партии</label>
                <input type="text" class="form-control input-sm postavka_nomer" name="postavka_nomer">
            </div>
        </div>
        <div class="col-2">
            <div class="form-group">
                <label for="breed_select">Поставщик</label>
                <input type="text" class="form-control input-sm postavka_postavsik" name="postavka_postavsik">
            </div>
        </div>
        <div class="col-2">
            <div class="form-group">
                <label for="breed_select">Диспетчер</label>
                <input type="text" class="form-control input-sm postavka_dispecer" name="postavka_dispecer">
            </div>
        </div>
        <div class="col-3">
            <div class="form-group">
                <label for="breed_select">Адрес поставки</label>
                <input type="text" class="form-control input-sm postavka_adres" name="postavka_adres">
            </div>
        </div>
        <div class="col-3">
            <div class="form-group">
                <label for="breed_select">Дата отгрузки план</label>
                <input type="text" class="form-control input-sm datepicker postavka_dataop" name="postavka_dataop">
            </div>
        </div>
        <div class="col-3">
            <div class="form-group">
                <label for="breed_select">Дата отгрузки факт</label>
                <input type="text" class="form-control input-sm datepicker postavka_dataof" name="postavka_dataof">
            </div>
        </div>
        <div class="col-3">
            <div class="form-group">
                <label for="breed_select">Дата поставки план</label>
                <input type="text" class="form-control input-sm datepicker postavka_datapp" name="postavka_datapp">
            </div>
        </div>
        <div class="col-3">
            <div class="form-group">
                <label for="breed_select">Дата поставки факт</label>
                <input type="text" class="form-control input-sm datepicker postavka_datapf" name="postavka_datapf">
            </div>
        </div>
        <div class="col-4">
            <div class="form-group">
                <label for="breed_select">Марка а/м</label>
                <input type="text" class="form-control input-sm postavka_markaam" name="postavka_markaam">
            </div>
        </div>
        <div class="col-4">
            <div class="form-group">
                <label for="breed_select">№ а/м</label>
                <input type="text" class="form-control input-sm postavka_nomeram" name="postavka_nomeram">
            </div>
        </div>
        <div class="col-4">
            <div class="form-group">
                <label for="breed_select">Объем, м3</label>
                <input type="text" class="form-control input-sm postavka_obiem" name="postavka_obiem">
            </div>
        </div>
    </div>
    <?php } ?>
    <!-- ///////// ADDED  Срок завершения для складов Производства + На покраске -->
    <? if(in_array($routes[2], [2, 7,8 , 4])){?>
    <div class="row">
        <div class="col-3">
            <div class="form-group">
                <label for="breed_select">Дата завершения</label>
                <input type="text" class="form-control input-sm datepicker date_finish" name="date_finish">
            </div>
        </div>
    </div>
    <?}?>
    <input type="hidden" name="consignment" value="new">
    <div class="row">
        <div class="col">
            <div class="form-group mb-0">
                <label>Сорт</label>
            </div>
        </div>
        <div class="col">
            <div class="form-group mb-0">
                <label>Порода</label>
            </div>
        </div>
        <div class="col">
            <div class="form-group mb-0">
                <label>Профиль</label>
            </div>
        </div>
        <div class="col-1">
            <div class="form-group mb-0">
                <label>Кол-во, шт.</label>
            </div>
        </div>
        <div class="col">
            <div class="form-group mb-0">
                <label>Толщина, мм.</label>
            </div>
        </div>
        <div class="col">
            <div class="form-group mb-0">
                <label>Ширина, мм.</label>
            </div>
        </div>
        <div class="col">
            <div class="form-group mb-0">
                <label>Длина, мм.</label>
            </div>
        </div>
        <div class="col-1">
            <div class="form-group mb-0">
                <label>м<sup>2</sup></label>
            </div>
        </div>
        <div class="col-2">
            <div class="form-group mb-0">
                <label>м<sup>3</sup></label>
            </div>
        </div>
    </div>
    <div class="list-raw-materials">
        <?php
		for ($i = 1; $i <= 5; $i++) {
			?>
        <div class="row">
            <div class="col">
                <div class="form-group">
                        <select name="name[]" id="" class="form-control select2 name-select" data-placeholder="" required>
                            <option value=""></option>
                            <? if(!empty($data['cort'])){
                                    foreach($data['cort'] as $cort){?>
                            <option value="<?=$cort['name'];?>">
                                <?=$cort['name'];?>
                            </option>
                            <?} 
                            }?>
                        </select>
                </div>
            </div>
            <!-- Порода (Добавить)-->
            <div class="col">
                <div class="form-group">
                        <select name="breed[]" class="form-control select2 breed-select" data-placeholder="" required>
                        <option></option>
                        <?php foreach($data['breed'] as $breed){?>
                        <option value="<?=$breed['id'];?>">
                            <?=$breed['name'];?>
                        </option>
                        <?}?>
                    </select>
                </div>
            </div>
            <!-- Профиль (Добавить) -->
            <div class="col">
                <div class="form-group">
                        <select name="profile[]" class="form-control select2 profile-select" data-placeholder="">
                        <option value=""></option>
                        <?php foreach($data['profiles'] as $profile){?>
                        <option value="<?=$profile;?>">
                            <?=$profile;?>
                        </option>
                        <?}?>
                    </select>
                </div>
            </div>
            <div class="col-1">
                <div class="form-group">
                    <input type="number" min="1" class="form-control input-sm input-form-quantity" value="0" name="quantity[]" required="">
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <input type="text" class="form-control input-sm touchspin input-form-thickness" value="0" name="thickness[]" required="">
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <input type="text" class="form-control input-sm touchspin input-form-width" value="0" name="width[]" required="">
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <input type="text" class="form-control input-sm touchspin input-form-length" value="0" name="length[]" required="">
                </div>
            </div>
            <div class="col-1">
                <div class="form-group">
                    <span class="volume_m2">0.00</span>
                </div>
            </div>
            <div class="col-2 d-flex align-items-baseline">
                <div class="form-group">
                    <span class="volume_m3">0.00</span>
                    <!-- <input type="text" disabled="" class="form-control input-sm touchspin input-form-volume-m3 volume_m3" value="0" name="volume_m3[]"> -->
                </div>
                <div class="col">
                    <button type="button" class="btn btn-sm btn-light ml-3 btn-copy-raw-material"><i class="fas fa-copy"></i></button>
                </div>
                <div class="col">
                    <button type="button" class="btn btn-icon waves-effect waves-light btn-danger btn-xs btn-delete-raw-material-new"><i class="fas fa-trash"></i></button>
                </div>
            </div>
        </div>
        <?php
		}
		?>
    </div>
    <a href="javascript:void(0);" class="add-more-products-raw">Добавить еще</a>
    <div class="d-flex justify-content-end">
        <button type="submit" class="btn btn-success waves-effect mr-2 btn-add-consignment" data-msg="Партия создана!">
            <span class="button-text">Добавить</span>
            <div class="button-wait" style="display: none;">
                <i class="fas fa-spinner fa-spin"></i>
            </div>
        </button>
        <button type="button" class="btn btn-danger waves-effect btn-hide-form-add-consignment">Отмена</button>
    </div>
</form>