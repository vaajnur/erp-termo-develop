<?
use \StandartRus\App\Core\Controller;
?>
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="page-title-box">
					<h4 class="page-title">Машины</h4>
					<ol class="breadcrumb p-0 m-0">
						<li class="breadcrumb-item">
							<a href="/">Главная</a>
						</li>
						<li class="breadcrumb-item active">
							Машины
						</li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="card mb-0 card-box">
			
				<? 
$consign_descr = array(
	'postavka_nomer' => '№ партии',
	'postavka_postavsik' => 'Поставщик',
	'postavka_dispecer' => 'Диспетчер',
	'postavka_adres' => 'Адрес',
	'postavka_dataop' => 'Дата отгрузки план',
	'postavka_dataof' => 'Дата отгрузки факт',
	'postavka_datapp' => 'Дата поставки план',
	'postavka_datapf' => 'Дата поставки факт',
	'postavka_markaam' => 'Марка а/м',
	'postavka_nomeram' => '№ а/м',
	'postavka_obiem' => 'Объем, м3',
	'description' => 'Описание',
	// '123' => 'Группа',
	// 'tocolor_breed' => 'Порода',
	// 'tocolor_maslo' => 'Масло',
	// 'tocolor_select_color' => 'Цвет',
	// 'tocolor_sides' => 'Количество сторон',
	// 'tocolor_toretc' => 'Торцы',
	// 'tocolor_face' => 'Лицевая сторона',
	// 'tocolor_back' => 'Обратная сторона',
	// 'tocolor_zakazchik' => 'Заказчик',
	// 'tocolor_remont_shpaklevka' => 'Ремонт/Шпаклевка',
);
				if(!empty($data['cars'])){
					foreach($data['cars'] as $id =>  $car){
						$controller = new Controller();
						$date_create_str = $controller->FormattingDate($car['desc']['date_create'], true);
						// var_dump($id);
					 ?>
					 <div class="row">
					 	<div class="col-float">
					 		<?=$date_create_str;?>
					 	</div>

					<?
						global $counter;
						$counter = 0;
						$car['desc'] = array_filter($car['desc']);
						// echo "<pre>";
						// print_r($car['desc']);
						// echo "</pre>";
						array_walk($car['desc'], function($item, $key) use($consign_descr, $counter, $last){
							global $counter;
							if($item != '' && $item != '0' && isset($consign_descr[$key])){
								// var_dump($counter);
								if($counter == 0) echo '<div class="col-float">';
					 ?>
				 	<?=$consign_descr[$key];?>:    <?=$item;?> </br>
						<? 
						if( ($counter+1)%4==0 && $counter != 0) echo '</div><div class="col-float">';
						$counter++;
						}
					}); ?> 
					</div></div>
				<table data-consignment="<?=$id;?>" class="table table-striped table-bordered m-0 small no-footer">
					<? 
					// echo "<pre>";
					// print_r($car['items']);
					if(!empty($car['items'])){
					array_walk($car['items'], function($item, $k){
						if($k == 0){?>
							<thead>
								<tr>
									<th>Изделие</th>
									<th>Профиль</th>
									<th>Порода</th>
									<th>Кол-во, шт.</th>
									<th>Толщина, мм.</th>
									<th>Ширина, мм.</th>
									<th>Длина, мм.</th>
									<th>м<sup>2</sup></th>
									<th>м<sup>3</sup></th>
								</tr>
							</thead>
							<tbody>
						<?}else{
							$volume_m3 = ($item['quantity'] * $item['width'] / 1000 * $item['length'] / 1000 * $item['thickness'] / 1000) / 1000000;
							$volume_m2 = ($item['quantity'] * $item['width'] / 1000 * $item['length'] / 1000) / 1000000;
						 ?>
							<tr>
								<td><?=$item['name'];?></td>
								<td><?=$item['profile'];?></td>
								<td><?=$item['breed'];?></td>
								<td><?=$item['quantity'];?></td>
								<td><?=$item['thickness'];?></td>
								<td><?=$item['width'];?></td>
								<td><?=$item['length'];?></td>
								<td><?= number_format($volume_m2, 3, '.', ' ') ; ?></td>
								<td><?= number_format($volume_m3 / 1000, 3, '.', ' '); ?></td>
							</tr>
						<?}}); ?>
					<? } ?>
					</tbody>
				</table>
				<br><hr><br>
				<? 	} 
				} ?>
		</div>
	</div> <!-- container-fluid -->
</div> <!-- content -->