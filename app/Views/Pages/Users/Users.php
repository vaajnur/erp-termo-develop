<div class="content">
	<div class="container-fluid">
        <div class="row">
			<div class="col-12">
				<div class="page-title-box">
                    <h4 class="page-title">Пользователи</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li class="breadcrumb-item">
                            <a href="#">Главная</a>
                        </li>
                        <li class="breadcrumb-item">
                            Настройки
                        </li>
                        <li class="breadcrumb-item active">
                            Пользователи
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
			</div>
		</div>
        <!-- end row -->

        <div class="row">
        	<div class="col-sm-4">
        		 <a href="#custom-modal" class="new-user-btn btn btn-danger btn-bordered waves-effect waves-light m-b-20" data-animation="fadein" data-plugin="custommodal" data-overlayspeed="200" data-overlaycolor="#36404a"><i class="md md-add"></i>Добавить пользователя</a>
        	</div><!-- end col -->
        </div>
        <!-- end row -->
		<ul class="nav nav-tabs" role="tablist">
            <? foreach($data['roles'] as $role){?>
                <li class="nav-item">
                    <a class="nav-link <? if($role['id'] == IS_ADMIN){?>active show<?}?>" id="role-<?=$role['id'];?>-tab" data-toggle="pill" href="#role-<?=$role['id'];?>" role="tab" aria-controls="role-<?=$role['id'];?>" aria-selected="true"><?=$role['name'];?></a>
                </li>
            <?}?>
        </ul>
        <div class="card-box border-top-0">
        	<div class="tab-content">
				<? foreach($data['roles'] as $role){?>
        		        <div class="tab-pane <? if($role['id'] == IS_ADMIN){?>active show<?}?>" id="role-<?=$role['id'];?>" role="tabpanel" aria-labelledby="role-<?=$role['id'];?>-tab">
        		            <div class="row">
								<? foreach($data['users'][$role['id']] as $user){?>
        			        	<div class="col-lg-3">
        			        		<div class="text-center card-box">
        			                    <div class="dropdown float-right">
        			                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
        			                            <h3 class="m-0 text-muted"><i class="mdi mdi-dots-horizontal"></i></h3>
        			                        </a>
        			                        <ul class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(24px, 29px, 0px);">
        			                            <li><a data-name="<?=$user['name'];?>" data-surname="<?=$user['surname'];?>" data-patronymic="<?=$user['patronymic'];?>" data-phone="<?=$user['phone'];?>"
                                                       data-email="<?=$user['email'];?>" data-type="<?=$user['type'];?>" data-photo="<?=$user['photo'];?>" data-id="<?=$user['id'];?>" href="#custom-modal" class="dropdown-item edit-user" data-animation="fadein" data-plugin="custommodal" data-overlayspeed="200" data-overlaycolor="#36404a">Редактировать</a></li>
        			                            <? if($user['id'] != 1){?><li><a href="#" data-id="<?=$user['id'];?>" class="dropdown-item delete-user">Удалить</a></li><?}?>
        			                        </ul>
        			                    </div>
        			                    <div class="clearfix"></div>
        			                    <div class="member-card">
        			                        <div class="thumb-xl member-thumb m-b-10 mx-auto d-block">
        			                            <img src="<?=$user['photo'];?>" class="rounded-circle img-thumbnail" alt="profile-image">
        			                        </div>
        			                        <div>
        			                            <h4 class="m-b-5"> 
                                                    <?=$user['surname'];?>
                                                    <?=$user['name'];?> 
                                                    <?=$user['patronymic'];?></h4>
        			                            <p class="text-muted"><?=$role['name'];?></p>
        			                        </div>
        			                    </div>
        			                </div>
        			            </div>
                                <?}?>
        		            </div>
        		        </div>
				<?}?>
        		    </div>
        </div>
    </div>
</div>

<div id="custom-modal" class="modal-demo">
    <button type="button" class="close" onclick="Custombox.close();">
        <span>×</span><span class="sr-only">Закрыть</span>
    </button>
    <h4 class="custom-modal-title">Добавить пользователя</h4>
    <div class="custom-modal-text text-left">
        <form>        	
            <div class="form-row">
            	<div class="col-lg-4">
        			<div class="form-group">
        		        <label for="">Фамилия</label>
        		        <input type="text"  name="surname" class="form-control surname-control" id="" placeholder="Введите фамилию" required>
        		    </div>
            	</div>
            	<div class="col-lg-4">
        			<div class="form-group">
        		        <label for="">Имя</label>
        		        <input type="text"  name="name" class="form-control name-control" id="" placeholder="Введите имя" required>
        		    </div>
            	</div>
            	<div class="col-lg-4">
        			<div class="form-group">
        		        <label for="">Отчество</label>
        		        <input type="text"  name="patronymic" class="form-control patronymic-control" id="" placeholder="Введите отчество" required>
        		    </div>
            	</div>
            </div>
            <div class="form-row">
            	<div class="col-lg-4">
		            <div class="form-group">
		                <label for="">Телефон</label>
		                <input type="text" name="phone" class="form-control phone-control" id="" placeholder="Введите телефон" required>
		            </div>
            	</div>
            	<div class="col-lg-4">
		            <div class="form-group">
		                <label for="">Email</label>
		                <input name="email" type="email" class="form-control email-control" id="" placeholder="Введите email" required>
		            </div>
            	</div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="">Пароль</label>
                        <input name="password" type="password" class="form-control password-control" id="" placeholder="Введите пароль" required>
                    </div>
                </div>
            </div>
            <div class="form-row">
            	<div class="col-lg-6">
        			<div class="form-group">
        		        <label for="name">Роль</label>
        		        <select name="type" id="" class="select2 type-select">
							<? foreach($data['roles'] as $role){?>
                                <option value="<?=$role['id'];?>"><?=$role['name'];?></option>
                            <?}?>
        		        </select>
        		    </div>
            	</div>
            	<div class="col-lg-6">
            		<div class="form-group">
            		    <label for="">Фотография</label>
            		    <br>
            		    <input name="image" class="image-control" type="file" class="" id="">
            		</div>
            	</div>
            </div>
            <div class="text-right">
            	<button data-id="" type="submit" class="save-user btn btn-success waves-effect waves-light">Сохранить</button>
            	<button onclick="Custombox.close();" type="button" class="btn btn-danger waves-effect waves-light m-l-10">Отмена</button>
            </div>
        </form>
    </div>
</div>