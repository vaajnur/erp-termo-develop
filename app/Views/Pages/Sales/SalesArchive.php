<!-- Start content -->
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="page-title-box">
					<h6>Склад</h6>
					<h4 class="page-title">Архив сделок</h4>
					<ol class="breadcrumb p-0 m-0">
						<li class="breadcrumb-item">
							<a href="/">Главная</a>
						</li>
						<li class="breadcrumb-item active">
							Архив сделок
						</li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<?php
		foreach ($data['sales'] as $sales) {
			?>
			<div class="card-box sale-card-box-<?=$sales['id'];?>">
				<h5 class="mt-0 text-body">№ <?= $sales['id']; ?></h5>

				<div class="d-flex justify-content-between mb-2">
					<div>
						<p>Дата отгрузки: <span class="font-weight-bold text-body"><?= $sales['date_create_str']; ?></span></p>
					</div>
					<div style="min-width:150px;">
						<?php foreach($data['status'] as $status){?>
							<?php if($sales['status']==$status['id']) echo $status['name'];?>
						<? } ?>
					</div>
				</div>
				<p>Информация</p>
				<div class="row text-body mb-4">
					<div class="col-2 small">
						<div class="d-flex justify-content-between">
							<span>Ф.И.О.:</span>
							<span class="font-weight-bold"><?= $sales['fio']; ?></span>
						</div>
					</div>
					<div class="col-2 small">
						<div class="d-flex justify-content-between">
							<span>Номер договора:</span>
							<span class="font-weight-bold"><?= $sales['contract']; ?></span>
						</div>
					</div>
					<div class="col-2 small">
						<div class="d-flex justify-content-between">
							<span>Тип договора:</span>
							<span class="font-weight-bold"><?= $ar_type[$sales['type']]; ?></span>
						</div>
					</div>
					<div class="col-2 small">
						<div class="d-flex justify-content-between">
							<span>Стоимость по договору:</span>
							<span class="font-weight-bold"><?= number_format($sales['cost'], 0, '.', ' '); ?> ₽</span>
						</div>
					</div>
					<div class="col-2 small">
						<div class="d-flex justify-content-between">
							<span>Аванс:</span>
							<span class="font-weight-bold"><?= number_format($sales['prepaid'], 0, '.', ' '); ?> ₽</span>
						</div>
					</div>
					<div class="col-2 small">
						<div class="d-flex justify-content-between">
							<span>К оплате:</span>
							<span class="font-weight-bold"><?= number_format($sales['cost'] - $sales['prepaid'], 0, '.', ' '); ?> ₽</span>
						</div>
					</div>
					<div class="col-2 small">
						<div class="d-flex justify-content-between">
							<span>Тип оплаты:</span>
							<span class="font-weight-bold"><?= $ar_payment_type[$sales['payment_type']]; ?></span>
						</div>
					</div>
				</div>
				<div class="accordion" id="accordionExample<?= $sales['id']; ?>">
					<div class="card mb-0">
						<div class="card-header p-0" id="headingTwo<?= $sales['id']; ?>">
							<button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo<?= $sales['id']; ?>" aria-expanded="false" aria-controls="collapseTwo<?= $sales['id']; ?>">Список товаров</button>
						</div>
						<div id="collapseTwo<?= $sales['id']; ?>" class="collapse" aria-labelledby="headingTwo<?= $sales['id']; ?>" data-parent="#accordionExample<?= $sales['id']; ?>">
							<div class="card-body">
								<div class="table-responsive">
									<table class="table table-striped table-bordered m-0 small">
										<thead>
										<tr>
											<th>Склад</th>
											<th>Изделие</th>
											<th>Кол-во, шт.</th>
											<th>Толщина, мм.</th>
											<th>Ширина, мм.</th>
											<th>Длина, мм.</th>
											<th>м<sup>2</sup></th>
											<th>м<sup>3</sup></th>
										</tr>
										</thead>
										<tbody>
										<?php
										$itogo = array();
										foreach ($sales['goods'] as $good) {
											$volume_m3 = ($good['quantity'] * $good['width'] / 1000 * $good['length'] / 1000 * $good['thickness'] / 1000) / 1000000;
											if ($good['volume_m3'] > 0) $volume_m3 = $good['volume_m3'];
											$itogo['quantity'] += intval($good['quantity']);
											$itogo['volume_m3'] += $volume_m3;
											$itogo['volume_m2'] += ($good['quantity'] * $good['width'] / 1000 * $good['length'] / 1000) / 1000000;
											?>
											<tr class="text-dark">
												<td><?= $good['storage_name']; ?></td>
												<td><?= $good['name']; ?></td>
												<td><?= $good['quantity']; ?></td>
												<td><?= number_format($good['thickness'] / 1000, 3, '.', ' '); ?></td>
												<td><?= number_format($good['width'] / 1000, 3, '.', ' '); ?></td>
												<td><?= number_format($good['length'] / 1000, 3, '.', ' '); ?></td>
												<td><?= ($good['quantity'] * $good['width'] / 1000 * $good['length'] / 1000) / 1000000; ?></td>
												<td><?= number_format($volume_m3 / 1000, 3, '.', ' '); ?></td>
											</tr>
											<?php
										}
										?>
										<tr>
                                            <td><b>Итог</b></td>
                                            <td></td>
                                            <td><?=$itogo['quantity'];?></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td><?=$itogo['volume_m2'];?></td>
                                            <td><?= number_format($itogo['volume_m3'] / 1000, 3, '.', ' '); ?></td>
                                        </tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
			<?php
		}
		?>
		<nav>
			<ul class="pagination pagination-sm">
				<?
				for ($i = 1; $i <= $data['pages']; $i++){?>
					<li class="page-item <? if($data['page'] == $i){?>disabled<?}?>">
						<a class="page-link" href="/sales/Archive/<?=$i;?>/"><?=$i;?></a>
					</li>
				<?}?>
			</ul>
		</nav>
	</div> <!-- container-fluid -->
</div> <!-- content -->