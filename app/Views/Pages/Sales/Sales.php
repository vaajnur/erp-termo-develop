<!-- Start content -->
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="page-title-box">
					<h6>Склад</h6>
					<h4 class="page-title">Сделки</h4>
					<ol class="breadcrumb p-0 m-0">
						<li class="breadcrumb-item">
							<a href="/">Главная</a>
						</li>
						<li class="breadcrumb-item active">
							Сделки
						</li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<button type="button" class="btn btn-primary waves-effect mb-4 btn-show-form-add-sale"><span class="mr-2">+</span>Добавить новую сделку</button>
		<form name="add_sale" method="post" class="mb-4 d-none" enctype="multipart/form-data">
			<div class="row">
				<div class="col-2">
					<div class="form-group">
						<label for="contract">Ф.И.О.</label>
						<input type="text" class="form-control input-sm" name="fio" required>
					</div>
				</div>
                <div class="col-2">
                    <div class="form-group">
                        <label for="contract">№ договора</label>
                        <input type="text" class="form-control input-sm" name="contract" required>
                    </div>
                </div>
				<div class="col-2">
					<div class="form-group">
						<label for="type_select">Тип договора</label>
						<select name="type" id="type_select" class="form-control select2" data-placeholder="Выбрать тип договора" required>
							<option></option>
							<option value="1">Поставка</option>
						</select>
					</div>
				</div>
				<div class="col-2">
					<div class="form-group">
						<label for="cost">Стоимость по договору</label>
						<input type="number" class="form-control input-sm" name="cost" required>
					</div>
				</div>
				<div class="col-2">
					<div class="form-group">
						<label for="prepaid">Аванс</label>
						<input type="number" class="form-control input-sm" name="prepaid">
					</div>
				</div>
				<div class="col-2">
                    <div class="form-group">
                        <label for="payment_type_select">Тип оплаты</label>
                        <select name="payment_type" id="payment_type_select" class="form-control select2" data-placeholder="Выбрать тип оплаты" required>
                            <option></option>
                            <option value="1">Банковский перевод</option>
                            <option value="2">Наличные</option>
                        </select>
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        <label for="status_select">Статус сделки</label>
                        <select name="status" id="status_select" class="form-control select2" data-placeholder="Выбрать статус сделки" required>
                            <?php foreach($data['status'] as $status){?>
                            <option value="<?=$status['id'];?>"><?=$status['name'];?></option>
                            <? } ?>
                        </select>
                    </div>
                </div>
			</div>
			<div class="card-box">
				<ul class="nav nav-tabs tabs-bordered" role="tablist">
					<?php
					$i = 0;
					foreach ($storage_list as $storage) {
						$i++;
						?>
						<li class="nav-item">
							<a class="nav-link <?= $i == 1 ? 'active' : '' ?>" id="storage-<?= $storage['id']; ?>-tab" data-toggle="tab" href="#storage-<?= $storage['id']; ?>" role="tab" aria-controls="storage-<?= $storage['id']; ?>" aria-selected="<?= $i == 1 ? 'true' : 'false' ?>">
								<span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
								<span class="d-none d-sm-block"><?= $storage['name']; ?></span>
							</a>
						</li>
						<?php
					}
					?>
				</ul>
				<div class="tab-content">
					<?php
					$i = 0;
					foreach ($storage_list as $storage) {
						$i++;
						?>
						<div class="tab-pane <?= $i == 1 ? 'show active' : '' ?>" id="storage-<?= $storage['id']; ?>" role="tabpanel" aria-labelledby="storage-<?= $storage['id']; ?>-tab">
							<div class="table-responsive">
								<table class="table table-striped table-bordered m-0 small">
									<thead>
									<tr>
										<th class="sale-form border-0 bg-white"></th>
										<th>Партия</th>
										<?php
										if ($storage['id'] == 4) {
											?>
											<th>Цвет</th>
											<?php
										}
										?>
										<th>Изделие</th>
										<th>Кол-во, шт.</th>
										<th>Толщина, мм.</th>
										<th>Ширина, мм.</th>
										<th>Длина, мм.</th>
										<th>м<sup>2</sup></th>
										<th>м<sup>3</sup></th>
									</tr>
									</thead>
									<tbody>
									<?php
									foreach ($data['goods'] as $row_material) {
										if ($row_material['storage'] == $storage['id']) {
											$volume_m3 = ($row_material['quantity'] * $row_material['width'] / 1000 * $row_material['length'] / 1000 * $row_material['thickness'] / 1000) / 1000000;
											if ($row_material['volume_m3'] > 0) $volume_m3 = $row_material['volume_m3'];
											?>
											<tr class="text-dark">
												<td class="repair-form border-0 bg-white pl-0" style="width: 120px;">
													<div class="form-inline">
														<div class="form-group">
															<div class="checkbox checkbox-primary pl-2">
																<input id="input_checkbox_<?= $row_material['id']; ?>" type="checkbox" class="checkbox-raw-material" data-id="<?= $row_material['id']; ?>">
																<label for="input_checkbox_<?= $row_material['id']; ?>">&nbsp;</label>
															</div>
														</div>
														<div class="form-group">
															<input type="number" min="0" max="<?=$row_material['quantity'];?>" class="form-control input-sm hide-input input-count-raw-material" style="width: 60px;" name="input_count_raw_material" value="0" disabled>
														</div>
													</div>
												</td>
												<td><?= $row_material['consignment']; ?></td>
												<?php
												if ($storage['id'] == 4) {
													?>
													<td><?= $row_material['color_name']; ?></td>
													<?php
												}
												?>
												<td><?= $row_material['name']; ?></td>
												<td><?= $row_material['quantity']; ?></td>
												<td><?= number_format($row_material['thickness'] / 1000, 3, '.', ' '); ?></td>
												<td><?= number_format($row_material['width'] / 1000, 3, '.', ' '); ?></td>
												<td><?= number_format($row_material['length'] / 1000, 3, '.', ' '); ?></td>
												<td><?= ($row_material['quantity'] * $row_material['width'] / 1000 * $row_material['length'] / 1000) / 1000000; ?></td>
												<td><?= number_format($volume_m3 / 1000, 3, '.', ' '); ?></td>
											</tr>
											<?php
										}
									}
									?>
									</tbody>
								</table>
							</div>
						</div>
						<?php
					}
					?>
				</div>
			</div>
			<div class="d-flex justify-content-end">
				<button type="submit" class="btn btn-success waves-effect mr-2 btn-add-sale" data-msg="Сделка создана!">
					<span class="button-text">Добавить</span>
					<div class="button-wait" style="display: none;">
						<i class="fas fa-spinner fa-spin"></i>
					</div>
				</button>
				<button type="button" class="btn btn-danger waves-effect btn-hide-form-add-sale">Отмена</button>
			</div>
		</form>
		<?php
		if (!empty($data['sales'])) {
		foreach ($data['sales'] as $sales) {
			$ar_type = array(1 => 'Поставка');
			$ar_payment_type = array(1 => 'Банковский перевод', 2 => 'Наличные');
			?>
            <form name="update_sale_<?=$sales['id'];?>" method="post" data-id="<?=$sales['id'];?>" class="card-box mb-4 d-none form-update-sale form-update-sale-<?=$sales['id'];?>" enctype="multipart/form-data">
                <h5 class="mt-0 text-body">№ <?= $sales['id']; ?></h5>
                <div class="row">
                    <div class="col-2">
                        <div class="form-group">
                            <label for="contract">Ф.И.О.</label>
                            <input type="text" class="form-control input-sm" name="fio" value="<?=$sales['fio'];?>" required>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            <label for="contract">№ договора</label>
                            <input type="text" class="form-control input-sm" name="contract" value="<?=$sales['contract'];?>" required>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            <label for="type_select">Тип договора</label>
                            <select name="type" id="type_select" class="form-control select2" data-placeholder="Выбрать тип договора" required>
                                <option></option>
                                <option <?php if($sales['type']=="1") echo "selected";?> value="1">Поставка</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            <label for="cost">Стоимость по договору</label>
                            <input type="number" class="form-control input-sm" name="cost" value="<?=$sales['cost'];?>" required>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            <label for="prepaid">Аванс</label>
                            <input type="number" class="form-control input-sm" name="prepaid" value="<?=$sales['prepaid'];?>">
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            <label for="payment_type_select">Тип оплаты</label>
                            <select name="payment_type" id="payment_type_select" class="form-control select2" data-placeholder="Выбрать тип оплаты" required>
                                <option></option>
                                <option <?php if($sales['payment_type']=="1") echo "selected";?> value="1">Банковский перевод</option>
                                <option <?php if($sales['payment_type']=="2") echo "selected";?> value="2">Наличные</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            <label for="status_select">Статус сделки</label>
                            <select name="status" id="status_select" class="form-control select2" data-placeholder="Выбрать статус сделки" required>
								<?php foreach($data['status'] as $status){?>
                                    <option <?php if($sales['status']==$status['id']) echo "selected";?> value="<?=$status['id'];?>"><?=$status['name'];?></option>
								<? } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="card-box">
                    <ul class="nav nav-tabs tabs-bordered" role="tablist">
						<?php
						$i = 0;
						foreach ($storage_list as $storage) {
							$i++;
							?>
                            <li class="nav-item">
                                <a class="nav-link <?= $i == $sales['active_storage'] ? 'active' : '' ?>" id="storage-<?= $storage['id']; ?>-<?= $sales['id']; ?>-tab" data-toggle="tab" href="#storage-<?= $storage['id']; ?>-<?= $sales['id']; ?>" role="tab" aria-controls="storage-<?= $storage['id']; ?>-<?= $sales['id']; ?>" aria-selected="<?= $i == 1 ? 'true' : 'false' ?>">
                                    <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                    <span class="d-none d-sm-block"><?= $storage['name']; ?></span>
                                </a>
                            </li>
							<?php
						}
						?>
                    </ul>
                    <div class="tab-content">
						<?php
						$i = 0;
						foreach ($storage_list as $storage) {
							$i++;
							?>
                            <div class="tab-pane <?= $i == $sales['active_storage'] ? 'show active' : '' ?>" id="storage-<?= $storage['id']; ?>-<?= $sales['id']; ?>" role="tabpanel" aria-labelledby="storage-<?= $storage['id']; ?>-<?= $sales['id']; ?>-tab">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered m-0 small">
                                        <thead>
                                        <tr>
                                            <th class="sale-form border-0 bg-white"></th>
                                            <th>Партия</th>
											<?php
											if ($storage['id'] == 4) {
												?>
                                                <th>Цвет</th>
												<?php
											}
											?>
                                            <th>Изделие</th>
                                            <th>Кол-во, шт.</th>
                                            <th>Толщина, мм.</th>
                                            <th>Ширина, мм.</th>
                                            <th>Длина, мм.</th>
                                            <th>м<sup>2</sup></th>
                                            <th>м<sup>3</sup></th>
                                        </tr>
                                        </thead>
                                        <tbody>
										<?php
										foreach ($sales['all_goods'] as $row_material) {
										    $checked_material = "";
										    $quantity_material = 0;
										    foreach($sales['goods'] as $row_material_sales){
												if($row_material_sales['raw_material_id'] == $row_material['id']){
													$checked_material = "checked";
													$quantity_material = $row_material_sales['quantity'];
                                                }
                                            }
											if ($row_material['storage'] == $storage['id']) {
												$volume_m3 = ($row_material['quantity'] * $row_material['width'] / 1000 * $row_material['length'] / 1000 * $row_material['thickness'] / 1000) / 1000000;
												if ($row_material['volume_m3'] > 0) $volume_m3 = $row_material['volume_m3'];
												?>
                                                <tr class="text-dark">
                                                    <td class="repair-form border-0 bg-white pl-0" style="width: 120px;">
                                                        <div class="form-inline">
                                                            <div class="form-group">
                                                                <div class="checkbox checkbox-primary pl-2">
                                                                    <input <?=$checked_material;?> id="input_checkbox_<?= $row_material['id']; ?>_<?= $sales['id']; ?>" type="checkbox" class="checkbox-raw-material" data-id="<?= $row_material['id']; ?>">
                                                                    <label for="input_checkbox_<?= $row_material['id']; ?>_<?= $sales['id']; ?>">&nbsp;</label>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="number" min="0" max="<?=$quantity_material+$row_material['quantity'];?>" class="form-control input-sm hide-input input-count-raw-material" style="width: 60px;" name="input_count_raw_material" value="<?=$quantity_material;?>" <?php if($quantity_material == 0){?>disabled<?}?>>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><?= $row_material['consignment']; ?></td>
													<?php
													if ($storage['id'] == 4) {
														?>
                                                        <td><?= $row_material['color_name']; ?></td>
														<?php
													}
													?>
                                                    <td><?= $row_material['name']; ?></td>
                                                    <td><?= $row_material['quantity']; ?></td>
                                                    <td><?= number_format($row_material['thickness'] / 1000, 3, '.', ' '); ?></td>
                                                    <td><?= number_format($row_material['width'] / 1000, 3, '.', ' '); ?></td>
                                                    <td><?= number_format($row_material['length'] / 1000, 3, '.', ' '); ?></td>
                                                    <td><?= ($row_material['quantity'] * $row_material['width'] / 1000 * $row_material['length'] / 1000) / 1000000; ?></td>
                                                    <td><?= number_format($volume_m3 / 1000, 3, '.', ' '); ?></td>
                                                </tr>
												<?php
											}
										}
										?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
							<?php
						}
						?>
                    </div>
                </div>
                <div class="d-flex justify-content-end">
                    <button type="submit" class="btn btn-success waves-effect mr-2 btn-update-sale" data-msg="Сделка обновлена!">
                        <span class="button-text">Сохранить</span>
                        <div class="button-wait" style="display: none;">
                            <i class="fas fa-spinner fa-spin"></i>
                        </div>
                    </button>
                    <button type="button" class="btn btn-danger waves-effect btn-hide-form-update-sale">Отмена</button>
                </div>
            </form>
			<div class="card-box sale-card-box-<?=$sales['id'];?>">
				<h5 class="mt-0 text-body">№ <?= $sales['id']; ?>
                    <button class="btn btn-icon waves-effect waves-light btn-primary btn-xs btn-edit-sale" data-id="<?= $sales['id']; ?>"><i class="fas fa-edit"></i></button>
                    <button class="btn btn-icon waves-effect waves-light btn-danger btn-xs btn-delete-sale" data-id="<?= $sales['id']; ?>"><i class="fas fa-trash"></i></button>
                </h5>

				<div class="d-flex justify-content-between mb-2">
					<div>
						<p>Дата отгрузки: <span class="font-weight-bold text-body"><?= $sales['date_create_str']; ?></span></p>
					</div>
					<div style="min-width:150px;">
						<select name="select_status_sales" class="form-control select2 select_status_sale" data-id="<?= $sales['id']; ?>" data-placeholder="Выбрать статус" required>
							<?php foreach($data['status'] as $status){?>
                                <option value="<?=$status['id'];?>" <?php if($sales['status'] == $status['id']){ echo "selected"; }?>><?=$status['name'];?></option>
							<? } ?>
						</select>
					</div>
				</div>
				<p>Информация</p>
				<div class="row text-body mb-4">
					<div class="col-2 small">
                        <div class="d-flex justify-content-between">
                            <span>Ф.И.О.:</span>
                            <span class="font-weight-bold"><?= $sales['fio']; ?></span>
                        </div>
                    </div>
                    <div class="col-2 small">
                        <div class="d-flex justify-content-between">
                            <span>Номер договора:</span>
                            <span class="font-weight-bold"><?= $sales['contract']; ?></span>
                        </div>
                    </div>
					<div class="col-2 small">
						<div class="d-flex justify-content-between">
							<span>Тип договора:</span>
							<span class="font-weight-bold"><?= $ar_type[$sales['type']]; ?></span>
						</div>
					</div>
					<div class="col-2 small">
						<div class="d-flex justify-content-between">
							<span>Стоимость по договору:</span>
							<span class="font-weight-bold"><?= number_format($sales['cost'], 0, '.', ' '); ?> ₽</span>
						</div>
					</div>
					<div class="col-2 small">
						<div class="d-flex justify-content-between">
							<span>Аванс:</span>
							<span class="font-weight-bold"><?= number_format($sales['prepaid'], 0, '.', ' '); ?> ₽</span>
						</div>
					</div>
					<div class="col-2 small">
						<div class="d-flex justify-content-between">
							<span>К оплате:</span>
							<span class="font-weight-bold"><?= number_format($sales['cost'] - $sales['prepaid'], 0, '.', ' '); ?> ₽</span>
						</div>
					</div>
					<div class="col-2 small">
						<div class="d-flex justify-content-between">
							<span>Тип оплаты:</span>
							<span class="font-weight-bold"><?= $ar_payment_type[$sales['payment_type']]; ?></span>
						</div>
					</div>
				</div>
				<div class="accordion" id="accordionExample<?= $sales['id']; ?>">
					<div class="card mb-0">
						<div class="card-header p-0" id="headingTwo<?= $sales['id']; ?>">
							<button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo<?= $sales['id']; ?>" aria-expanded="false" aria-controls="collapseTwo<?= $sales['id']; ?>">Список товаров</button>
						</div>
						<div id="collapseTwo<?= $sales['id']; ?>" class="collapse" aria-labelledby="headingTwo<?= $sales['id']; ?>" data-parent="#accordionExample<?= $sales['id']; ?>">
							<div class="card-body">
								<div class="table-responsive">
									<table class="table table-striped table-bordered m-0 small">
										<thead>
										<tr>
											<th>Склад</th>
											<th>Изделие</th>
											<th>Кол-во, шт.</th>
											<th>Толщина, мм.</th>
											<th>Ширина, мм.</th>
											<th>Длина, мм.</th>
											<th>м<sup>2</sup></th>
											<th>м<sup>3</sup></th>
										</tr>
										</thead>
										<tbody>
										<?php
										$itogo = array();
										foreach ($sales['goods'] as $good) {
											$volume_m3 = ($good['quantity'] * $good['width'] / 1000 * $good['length'] / 1000 * $good['thickness'] / 1000) / 1000000;
											if ($good['volume_m3'] > 0) $volume_m3 = $good['volume_m3'];
											$itogo['quantity'] += intval($good['quantity']);
											$itogo['volume_m3'] += $volume_m3;
											$itogo['volume_m2'] += ($good['quantity'] * $good['width'] / 1000 * $good['length'] / 1000) / 1000000;
											?>
											<tr class="text-dark">
												<td><?= $good['storage_name']; ?></td>
												<td><?= $good['name']; ?></td>
												<td><?= $good['quantity']; ?></td>
												<td><?= number_format($good['thickness'] / 1000, 3, '.', ' '); ?></td>
												<td><?= number_format($good['width'] / 1000, 3, '.', ' '); ?></td>
												<td><?= number_format($good['length'] / 1000, 3, '.', ' '); ?></td>
												<td><?= ($good['quantity'] * $good['width'] / 1000 * $good['length'] / 1000) / 1000000; ?></td>
												<td><?= number_format($volume_m3 / 1000, 3, '.', ' '); ?></td>
											</tr>
											<?php
										}
										?>
										 <tr>
                                            <td><b>Итог</b></td>
                                            <td></td>
                                            <td><?=$itogo['quantity'];?></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td><?=$itogo['volume_m2'];?></td>
                                            <td><?= number_format($itogo['volume_m3'] / 1000, 3, '.', ' '); ?></td>
                                        </tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php
		}
	}
		?>
		<? if($data['archive'] !== true){?>
            <a href="/sales/Archive/" class="btn form-control btn-dark">Архив сделок</a>
        <?}?>
	</div> <!-- container-fluid -->
</div> <!-- content -->