<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">

	<!-- App favicon -->
	<link rel="shortcut icon" href="/assets/images/favicon.ico">
	<!-- App title -->
	<title>Стандарт Термодерево - <?= ($name_storage != '') ? $name_storage : $mess[$active_folder[0] . '_TITLE'] ?></title>
	<? //var_dump($name_storage); ?>
	<? 
	// var_dump($active_folder);
	// echo "<pre>";
	// print_r($mess);
	// echo "</pre>";
	?>

	<!--Morris Chart CSS -->
	<link rel="stylesheet" href="/assets/vendor/morris/morris.css">

	<!-- Plugins css-->
	<link href="/assets/vendor/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
	<link href="/assets/vendor/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />	
	<link href="/assets/vendor/custombox/css/custombox.min.css" rel="stylesheet" />	

	<!-- App css -->
	<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="/assets/css/icons.css"/>
	<link rel="stylesheet" type="text/css" href="/assets/css/datatables.min.css"/>
	<link rel="stylesheet" type="text/css" href="/assets/css/style.css"/>

	<link rel="stylesheet" href="/assets/vendor/switchery/switchery.min.css">
	<link rel="stylesheet" href="/assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css">

	<script src="/assets/js/modernizr.min.js"></script>
</head>
<body class="fixed-left">
<div id="wrapper">
	<!-- Top Bar Start -->
	<div class="topbar">
		<!-- LOGO -->
		<div class="topbar-left">
			<a href="/main/" class="logo">
				<span class="px-4">
					<svg xmlns="http://www.w3.org/2000/svg" width="177" viewBox="0 0 209.46 24.6"><defs><style>.cls-1 {
									fill: #fe0e02;
								}

								.cls-1, .cls-2 {
									fill-rule: evenodd;
								}

								.cls-2 {
									fill: white;
								}</style></defs><g id="sloy-2" data-name="sloy-2"><g id="cdr_layer2"><path class="cls-1"
																										   d="M42.22,21.13a11.09,11.09,0,1,0-15.51-2.3A11.09,11.09,0,0,0,42.22,21.13Zm-6.7-5.85a2.9,2.9,0,1,0-2.9-2.9A2.9,2.9,0,0,0,35.52,15.28ZM37,12.38a1.44,1.44,0,1,1-1.44-1.44A1.44,1.44,0,0,1,37,12.38Zm-1.44,6.17a6.17,6.17,0,1,0-6.17-6.17A6.17,6.17,0,0,0,35.52,18.55ZM40,12.38a4.51,4.51,0,1,1-4.52-4.51A4.52,4.52,0,0,1,40,12.38Zm3.24,0a7.75,7.75,0,1,1-7.75-7.75A7.75,7.75,0,0,1,43.27,12.38Zm1.58,0a9.33,9.33,0,1,0-9.33,9.33A9.33,9.33,0,0,0,44.85,12.38Z"></path><path
										class="cls-2" d="M4.15,23.78a33.36,33.36,0,0,1-2-10H0a35.53,35.53,0,0,0,1.88,10Z"></path><path class="cls-2" d="M1.84.82A35.55,35.55,0,0,0,0,10.71H2.14A33.35,33.35,0,0,1,4.1.82Z"></path><path class="cls-2" d="M3.85,13.75A31.66,31.66,0,0,0,6,23.78H9.22a28.66,28.66,0,0,1-2.37-10Z"></path><path class="cls-2" d="M5.93.82a31.66,31.66,0,0,0-2.07,9.89h3A28.63,28.63,0,0,1,9.16.82Z"></path><path class="cls-2"
																																																																																																											  d="M8.14,13.75a27.37,27.37,0,0,0,2.49,10H13a25.23,25.23,0,0,1-2.73-10Z"></path><path
										class="cls-2" d="M10.57.82a27.39,27.39,0,0,0-2.43,9.89h2.14A25.23,25.23,0,0,1,12.94.82Z"></path><path class="cls-2" d="M11.78,13.75a23.73,23.73,0,0,0,2.93,10h3a21.21,21.21,0,0,1-3.34-10Z"></path><path class="cls-2" d="M14.63.82a23.74,23.74,0,0,0-2.85,9.89h2.57A21.2,21.2,0,0,1,17.61.82Z"></path><path class="cls-2" d="M15.64,13.75a19.92,19.92,0,0,0,3.61,10h3.44a17.28,17.28,0,0,1-4.34-10Z"></path><path class="cls-2"
																																																																																																														   d="M19.15.82a19.91,19.91,0,0,0-3.51,9.89h2.7A17.27,17.27,0,0,1,22.56.82Z"></path><path
										class="cls-2" d="M19.63,13.75a16,16,0,0,0,4.83,10h3.78a13.7,13.7,0,0,1-6.24-10Z"></path><path class="cls-2" d="M24.32.82a16,16,0,0,0-4.69,9.89H22A13.7,13.7,0,0,1,28,.82Z"></path><path class="cls-2" d="M69.09,13.75a33.36,33.36,0,0,1-2,10h2.27a35.53,35.53,0,0,0,1.88-10Z"></path><path class="cls-2" d="M67.13.82a33.35,33.35,0,0,1,2,9.89h2.14A35.52,35.52,0,0,0,69.4.82Z"></path><path class="cls-2"
																																																																																																									 d="M65.25,23.78a31.66,31.66,0,0,0,2.13-10h-3a28.66,28.66,0,0,1-2.37,10Z"></path><path
										class="cls-2" d="M67.38,10.71A31.66,31.66,0,0,0,65.3.82H62.07a28.63,28.63,0,0,1,2.31,9.89Z"></path><path class="cls-2" d="M60.6,23.78a27.37,27.37,0,0,0,2.49-10H61a25.23,25.23,0,0,1-2.73,10Z"></path><path class="cls-2" d="M63.1,10.71A27.39,27.39,0,0,0,60.67.82H58.29A25.21,25.21,0,0,1,61,10.71Z"></path><path class="cls-2" d="M56.53,23.78a23.75,23.75,0,0,0,2.93-10H56.88a21.21,21.21,0,0,1-3.34,10Z"></path><path class="cls-2"
																																																																																																																   d="M59.45,10.71A23.72,23.72,0,0,0,56.6.82h-3a21.19,21.19,0,0,1,3.26,9.89Z"></path><path
										class="cls-2" d="M52,23.78a19.92,19.92,0,0,0,3.61-10h-2.7a17.27,17.27,0,0,1-4.34,10Z"></path><path class="cls-2" d="M55.59,10.71A19.92,19.92,0,0,0,52.09.82H48.67a17.26,17.26,0,0,1,4.22,9.89Z"></path><path class="cls-2" d="M46.77,23.78a16,16,0,0,0,4.83-10H49.24a13.7,13.7,0,0,1-6.24,10Z"></path><path class="cls-2" d="M51.6,10.71A16,16,0,0,0,46.91.82h-3.7a13.7,13.7,0,0,1,6,9.89Z"></path><path class="cls-2"
																																																																																																												 d="M77,7.13a6.65,6.65,0,0,0,2,5,8.4,8.4,0,0,0,5.93,2.11A10.65,10.65,0,0,0,90,13.08l-.87-2a9.8,9.8,0,0,1-4.06.83,5.32,5.32,0,0,1-4-1.56,4.8,4.8,0,0,1-1.17-3.25A4.67,4.67,0,0,1,81.32,3.6a5.44,5.44,0,0,1,3.76-1.28,10.73,10.73,0,0,1,4,.8l.87-2A10.72,10.72,0,0,0,84.89,0,8.61,8.61,0,0,0,79.3,1.79,6.5,6.5,0,0,0,77,7.13ZM93.12,2.52h5.22V14h3V2.52h5.25V.25H93.12ZM113.9.25,107.73,14h3l1-2.25h7.56l1,2.25h3.09L117.22.25ZM115.36,3h.3l2.66,6.46h-5.59ZM137,.25V5.5H130V.25H127V14H130V7.84H137V14H140V.25Zm10.17,0-1.26,9.56a2.45,2.45,0,0,1-.62,1.54,1.72,1.72,0,0,1-1.31.41h-.37v5h2.41l.25-2.77h10.1l.25,2.77h2.45v-5h-1.4V.25Zm1.49,9.88,1-7.61h5.09v9.21h-6.62A4.4,4.4,0,0,0,148.7,10.13ZM168.17.25,162,14h3l1-2.25h7.56l1,2.25h3.09L171.5.25ZM169.64,3h.3l2.66,6.46H167Zm18.21,7.29a6.27,6.27,0,0,0,4.35-1.56,4.76,4.76,0,0,0,1.38-3.46,4.71,4.71,0,0,0-1.47-3.55,5.68,5.68,0,0,0-4-1.47h-6.8V14h3V10.29Zm2.73-5a2.56,2.56,0,0,1-.8,2,3,3,0,0,1-2.13.73h-3.39V2.52h3.57a2.76,2.76,0,0,1,2.13.83A2.82,2.82,0,0,1,190.58,5.27ZM196,2.52h5.22V14h3V2.52h5.25V.25H196Z"></path><path
										class="cls-1"
										d="M83.81,19.33H82.22v4.1H80.9v-4.1H79.31v-1h4.49Zm11.12,4.1H91.25V18.34h3.68v1H92.57v.88h2.2v1h-2.2v1.25h2.37Zm12-3.48a1.93,1.93,0,0,1-.12.67,1.48,1.48,0,0,1-.34.55,2.09,2.09,0,0,1-.68.45,2.44,2.44,0,0,1-.93.15H104v1.66h-1.31V18.34h2.16a3.41,3.41,0,0,1,.82.08,1.91,1.91,0,0,1,.59.25,1.35,1.35,0,0,1,.47.52A1.62,1.62,0,0,1,106.88,19.95Zm-1.37,0a.6.6,0,0,0-.11-.37.64.64,0,0,0-.27-.22,1.22,1.22,0,0,0-.4-.09H104v1.52h.37a3,3,0,0,0,.55,0,.76.76,0,0,0,.36-.16.63.63,0,0,0,.18-.26A1.06,1.06,0,0,0,105.52,20Zm14.28,3.45h-1.32V20l-.94,2.21h-.9L115.7,20v3.41h-1.24V18.34H116l1.14,2.55,1.13-2.55h1.53Zm12.9-2.54a2.65,2.65,0,0,1-.7,1.93,3,3,0,0,1-3.86,0,2.65,2.65,0,0,1-.7-1.93,2.65,2.65,0,0,1,.7-1.94,3,3,0,0,1,3.85,0A2.64,2.64,0,0,1,132.7,20.89ZM131,22.18a1.52,1.52,0,0,0,.28-.55,2.66,2.66,0,0,0,.09-.75,2.5,2.5,0,0,0-.11-.78,1.51,1.51,0,0,0-.28-.52,1,1,0,0,0-.4-.3,1.27,1.27,0,0,0-.94,0,1.09,1.09,0,0,0-.41.29,1.43,1.43,0,0,0-.28.53,2.93,2.93,0,0,0,0,1.56,1.54,1.54,0,0,0,.28.52,1.07,1.07,0,0,0,.4.3,1.24,1.24,0,0,0,.48.09,1.22,1.22,0,0,0,.48-.1A1.06,1.06,0,0,0,131,22.18Zm14.38,2.42h-1.2V23.42h-3.09V24.6h-1.2V22.44h.38a8.06,8.06,0,0,0,.6-1.87,11.42,11.42,0,0,0,.21-2.23h3.79v4.1h.5Zm-1.82-2.16V19.33h-1.27A9.45,9.45,0,0,1,142,21a7.81,7.81,0,0,1-.5,1.4Zm13.05,1H152.9V18.34h3.68v1h-2.37v.88h2.2v1h-2.2v1.25h2.37Zm12-3.48a1.93,1.93,0,0,1-.12.67,1.48,1.48,0,0,1-.34.55,2.09,2.09,0,0,1-.68.45,2.44,2.44,0,0,1-.93.15h-.82v1.66h-1.32V18.34h2.16a3.41,3.41,0,0,1,.82.08,1.93,1.93,0,0,1,.59.25,1.35,1.35,0,0,1,.47.52A1.62,1.62,0,0,1,168.53,19.95Zm-1.36,0a.6.6,0,0,0-.12-.37.64.64,0,0,0-.27-.22,1.22,1.22,0,0,0-.4-.09h-.74v1.52H166a3,3,0,0,0,.55,0,.76.76,0,0,0,.36-.16.63.63,0,0,0,.18-.26A1.06,1.06,0,0,0,167.16,20Zm12.63,3.45h-3.68V18.34h3.68v1h-2.37v.88h2.2v1h-2.2v1.25h2.37Zm12.11-1.56a1.43,1.43,0,0,1-.15.66,1.38,1.38,0,0,1-.41.48,1.94,1.94,0,0,1-.66.32,3.65,3.65,0,0,1-.92.09h-2.22V18.34h2a7.12,7.12,0,0,1,.9,0,1.87,1.87,0,0,1,.57.18,1,1,0,0,1,.43.39,1.19,1.19,0,0,1-.05,1.22,1.27,1.27,0,0,1-.55.44v0a1.43,1.43,0,0,1,.79.41A1.15,1.15,0,0,1,191.9,21.87Zm-1.68-2.09a.57.57,0,0,0-.06-.25.39.39,0,0,0-.22-.19,1.06,1.06,0,0,0-.36-.06h-.72v1.08h.73a.92.92,0,0,0,.34-.07.42.42,0,0,0,.23-.2A.72.72,0,0,0,190.22,19.78Zm.31,2.07a.65.65,0,0,0-.09-.38.57.57,0,0,0-.32-.2,1.65,1.65,0,0,0-.42,0h-.84V22.5h.88a1.14,1.14,0,0,0,.44-.09.54.54,0,0,0,.28-.23A.69.69,0,0,0,190.53,21.85Zm13.94-1a2.65,2.65,0,0,1-.7,1.93,3,3,0,0,1-3.85,0,2.65,2.65,0,0,1-.7-1.93,2.65,2.65,0,0,1,.7-1.94,3,3,0,0,1,3.85,0A2.64,2.64,0,0,1,204.48,20.89Zm-1.74,1.29a1.53,1.53,0,0,0,.28-.55,2.66,2.66,0,0,0,.09-.75,2.5,2.5,0,0,0-.11-.78,1.5,1.5,0,0,0-.28-.52,1,1,0,0,0-.4-.3,1.27,1.27,0,0,0-.94,0,1.1,1.1,0,0,0-.41.29,1.45,1.45,0,0,0-.28.53,2.91,2.91,0,0,0,0,1.56,1.55,1.55,0,0,0,.28.52,1.09,1.09,0,0,0,.4.3,1.24,1.24,0,0,0,.48.09,1.21,1.21,0,0,0,.48-.1A1.06,1.06,0,0,0,202.74,22.18Z"></path></g></g></svg>
				</span>
				<i>
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 209.46 24.6">
						<defs>
							<style>.cls-1 {
									fill: #fe0e02;
								}

								.cls-1, .cls-2 {
									fill-rule: evenodd;
								}

								.cls-2 {
									fill: white;
								}</style>
						</defs>
						<g id="sloy-2" data-name="sloy-2">
							<g id="cdr_layer2">
								<path class="cls-1"
									  d="M42.22,21.13a11.09,11.09,0,1,0-15.51-2.3A11.09,11.09,0,0,0,42.22,21.13Zm-6.7-5.85a2.9,2.9,0,1,0-2.9-2.9A2.9,2.9,0,0,0,35.52,15.28ZM37,12.38a1.44,1.44,0,1,1-1.44-1.44A1.44,1.44,0,0,1,37,12.38Zm-1.44,6.17a6.17,6.17,0,1,0-6.17-6.17A6.17,6.17,0,0,0,35.52,18.55ZM40,12.38a4.51,4.51,0,1,1-4.52-4.51A4.52,4.52,0,0,1,40,12.38Zm3.24,0a7.75,7.75,0,1,1-7.75-7.75A7.75,7.75,0,0,1,43.27,12.38Zm1.58,0a9.33,9.33,0,1,0-9.33,9.33A9.33,9.33,0,0,0,44.85,12.38Z"></path>
								<path class="cls-2" d="M4.15,23.78a33.36,33.36,0,0,1-2-10H0a35.53,35.53,0,0,0,1.88,10Z"></path>
								<path class="cls-2" d="M1.84.82A35.55,35.55,0,0,0,0,10.71H2.14A33.35,33.35,0,0,1,4.1.82Z"></path>
								<path class="cls-2" d="M3.85,13.75A31.66,31.66,0,0,0,6,23.78H9.22a28.66,28.66,0,0,1-2.37-10Z"></path>
								<path class="cls-2" d="M5.93.82a31.66,31.66,0,0,0-2.07,9.89h3A28.63,28.63,0,0,1,9.16.82Z"></path>
								<path class="cls-2" d="M8.14,13.75a27.37,27.37,0,0,0,2.49,10H13a25.23,25.23,0,0,1-2.73-10Z"></path>
								<path class="cls-2" d="M10.57.82a27.39,27.39,0,0,0-2.43,9.89h2.14A25.23,25.23,0,0,1,12.94.82Z"></path>
								<path class="cls-2" d="M11.78,13.75a23.73,23.73,0,0,0,2.93,10h3a21.21,21.21,0,0,1-3.34-10Z"></path>
								<path class="cls-2" d="M14.63.82a23.74,23.74,0,0,0-2.85,9.89h2.57A21.2,21.2,0,0,1,17.61.82Z"></path>
								<path class="cls-2" d="M15.64,13.75a19.92,19.92,0,0,0,3.61,10h3.44a17.28,17.28,0,0,1-4.34-10Z"></path>
								<path class="cls-2" d="M19.15.82a19.91,19.91,0,0,0-3.51,9.89h2.7A17.27,17.27,0,0,1,22.56.82Z"></path>
								<path class="cls-2" d="M19.63,13.75a16,16,0,0,0,4.83,10h3.78a13.7,13.7,0,0,1-6.24-10Z"></path>
								<path class="cls-2" d="M24.32.82a16,16,0,0,0-4.69,9.89H22A13.7,13.7,0,0,1,28,.82Z"></path>
								<path class="cls-2" d="M69.09,13.75a33.36,33.36,0,0,1-2,10h2.27a35.53,35.53,0,0,0,1.88-10Z"></path>
								<path class="cls-2" d="M67.13.82a33.35,33.35,0,0,1,2,9.89h2.14A35.52,35.52,0,0,0,69.4.82Z"></path>
								<path class="cls-2" d="M65.25,23.78a31.66,31.66,0,0,0,2.13-10h-3a28.66,28.66,0,0,1-2.37,10Z"></path>
								<path class="cls-2" d="M67.38,10.71A31.66,31.66,0,0,0,65.3.82H62.07a28.63,28.63,0,0,1,2.31,9.89Z"></path>
								<path class="cls-2" d="M60.6,23.78a27.37,27.37,0,0,0,2.49-10H61a25.23,25.23,0,0,1-2.73,10Z"></path>
								<path class="cls-2" d="M63.1,10.71A27.39,27.39,0,0,0,60.67.82H58.29A25.21,25.21,0,0,1,61,10.71Z"></path>
								<path class="cls-2" d="M56.53,23.78a23.75,23.75,0,0,0,2.93-10H56.88a21.21,21.21,0,0,1-3.34,10Z"></path>
								<path class="cls-2" d="M59.45,10.71A23.72,23.72,0,0,0,56.6.82h-3a21.19,21.19,0,0,1,3.26,9.89Z"></path>
								<path class="cls-2" d="M52,23.78a19.92,19.92,0,0,0,3.61-10h-2.7a17.27,17.27,0,0,1-4.34,10Z"></path>
								<path class="cls-2" d="M55.59,10.71A19.92,19.92,0,0,0,52.09.82H48.67a17.26,17.26,0,0,1,4.22,9.89Z"></path>
								<path class="cls-2" d="M46.77,23.78a16,16,0,0,0,4.83-10H49.24a13.7,13.7,0,0,1-6.24,10Z"></path>
								<path class="cls-2" d="M51.6,10.71A16,16,0,0,0,46.91.82h-3.7a13.7,13.7,0,0,1,6,9.89Z"></path>
								<path class="cls-2"
									  d="M77,7.13a6.65,6.65,0,0,0,2,5,8.4,8.4,0,0,0,5.93,2.11A10.65,10.65,0,0,0,90,13.08l-.87-2a9.8,9.8,0,0,1-4.06.83,5.32,5.32,0,0,1-4-1.56,4.8,4.8,0,0,1-1.17-3.25A4.67,4.67,0,0,1,81.32,3.6a5.44,5.44,0,0,1,3.76-1.28,10.73,10.73,0,0,1,4,.8l.87-2A10.72,10.72,0,0,0,84.89,0,8.61,8.61,0,0,0,79.3,1.79,6.5,6.5,0,0,0,77,7.13ZM93.12,2.52h5.22V14h3V2.52h5.25V.25H93.12ZM113.9.25,107.73,14h3l1-2.25h7.56l1,2.25h3.09L117.22.25ZM115.36,3h.3l2.66,6.46h-5.59ZM137,.25V5.5H130V.25H127V14H130V7.84H137V14H140V.25Zm10.17,0-1.26,9.56a2.45,2.45,0,0,1-.62,1.54,1.72,1.72,0,0,1-1.31.41h-.37v5h2.41l.25-2.77h10.1l.25,2.77h2.45v-5h-1.4V.25Zm1.49,9.88,1-7.61h5.09v9.21h-6.62A4.4,4.4,0,0,0,148.7,10.13ZM168.17.25,162,14h3l1-2.25h7.56l1,2.25h3.09L171.5.25ZM169.64,3h.3l2.66,6.46H167Zm18.21,7.29a6.27,6.27,0,0,0,4.35-1.56,4.76,4.76,0,0,0,1.38-3.46,4.71,4.71,0,0,0-1.47-3.55,5.68,5.68,0,0,0-4-1.47h-6.8V14h3V10.29Zm2.73-5a2.56,2.56,0,0,1-.8,2,3,3,0,0,1-2.13.73h-3.39V2.52h3.57a2.76,2.76,0,0,1,2.13.83A2.82,2.82,0,0,1,190.58,5.27ZM196,2.52h5.22V14h3V2.52h5.25V.25H196Z"></path>
								<path class="cls-1"
									  d="M83.81,19.33H82.22v4.1H80.9v-4.1H79.31v-1h4.49Zm11.12,4.1H91.25V18.34h3.68v1H92.57v.88h2.2v1h-2.2v1.25h2.37Zm12-3.48a1.93,1.93,0,0,1-.12.67,1.48,1.48,0,0,1-.34.55,2.09,2.09,0,0,1-.68.45,2.44,2.44,0,0,1-.93.15H104v1.66h-1.31V18.34h2.16a3.41,3.41,0,0,1,.82.08,1.91,1.91,0,0,1,.59.25,1.35,1.35,0,0,1,.47.52A1.62,1.62,0,0,1,106.88,19.95Zm-1.37,0a.6.6,0,0,0-.11-.37.64.64,0,0,0-.27-.22,1.22,1.22,0,0,0-.4-.09H104v1.52h.37a3,3,0,0,0,.55,0,.76.76,0,0,0,.36-.16.63.63,0,0,0,.18-.26A1.06,1.06,0,0,0,105.52,20Zm14.28,3.45h-1.32V20l-.94,2.21h-.9L115.7,20v3.41h-1.24V18.34H116l1.14,2.55,1.13-2.55h1.53Zm12.9-2.54a2.65,2.65,0,0,1-.7,1.93,3,3,0,0,1-3.86,0,2.65,2.65,0,0,1-.7-1.93,2.65,2.65,0,0,1,.7-1.94,3,3,0,0,1,3.85,0A2.64,2.64,0,0,1,132.7,20.89ZM131,22.18a1.52,1.52,0,0,0,.28-.55,2.66,2.66,0,0,0,.09-.75,2.5,2.5,0,0,0-.11-.78,1.51,1.51,0,0,0-.28-.52,1,1,0,0,0-.4-.3,1.27,1.27,0,0,0-.94,0,1.09,1.09,0,0,0-.41.29,1.43,1.43,0,0,0-.28.53,2.93,2.93,0,0,0,0,1.56,1.54,1.54,0,0,0,.28.52,1.07,1.07,0,0,0,.4.3,1.24,1.24,0,0,0,.48.09,1.22,1.22,0,0,0,.48-.1A1.06,1.06,0,0,0,131,22.18Zm14.38,2.42h-1.2V23.42h-3.09V24.6h-1.2V22.44h.38a8.06,8.06,0,0,0,.6-1.87,11.42,11.42,0,0,0,.21-2.23h3.79v4.1h.5Zm-1.82-2.16V19.33h-1.27A9.45,9.45,0,0,1,142,21a7.81,7.81,0,0,1-.5,1.4Zm13.05,1H152.9V18.34h3.68v1h-2.37v.88h2.2v1h-2.2v1.25h2.37Zm12-3.48a1.93,1.93,0,0,1-.12.67,1.48,1.48,0,0,1-.34.55,2.09,2.09,0,0,1-.68.45,2.44,2.44,0,0,1-.93.15h-.82v1.66h-1.32V18.34h2.16a3.41,3.41,0,0,1,.82.08,1.93,1.93,0,0,1,.59.25,1.35,1.35,0,0,1,.47.52A1.62,1.62,0,0,1,168.53,19.95Zm-1.36,0a.6.6,0,0,0-.12-.37.64.64,0,0,0-.27-.22,1.22,1.22,0,0,0-.4-.09h-.74v1.52H166a3,3,0,0,0,.55,0,.76.76,0,0,0,.36-.16.63.63,0,0,0,.18-.26A1.06,1.06,0,0,0,167.16,20Zm12.63,3.45h-3.68V18.34h3.68v1h-2.37v.88h2.2v1h-2.2v1.25h2.37Zm12.11-1.56a1.43,1.43,0,0,1-.15.66,1.38,1.38,0,0,1-.41.48,1.94,1.94,0,0,1-.66.32,3.65,3.65,0,0,1-.92.09h-2.22V18.34h2a7.12,7.12,0,0,1,.9,0,1.87,1.87,0,0,1,.57.18,1,1,0,0,1,.43.39,1.19,1.19,0,0,1-.05,1.22,1.27,1.27,0,0,1-.55.44v0a1.43,1.43,0,0,1,.79.41A1.15,1.15,0,0,1,191.9,21.87Zm-1.68-2.09a.57.57,0,0,0-.06-.25.39.39,0,0,0-.22-.19,1.06,1.06,0,0,0-.36-.06h-.72v1.08h.73a.92.92,0,0,0,.34-.07.42.42,0,0,0,.23-.2A.72.72,0,0,0,190.22,19.78Zm.31,2.07a.65.65,0,0,0-.09-.38.57.57,0,0,0-.32-.2,1.65,1.65,0,0,0-.42,0h-.84V22.5h.88a1.14,1.14,0,0,0,.44-.09.54.54,0,0,0,.28-.23A.69.69,0,0,0,190.53,21.85Zm13.94-1a2.65,2.65,0,0,1-.7,1.93,3,3,0,0,1-3.85,0,2.65,2.65,0,0,1-.7-1.93,2.65,2.65,0,0,1,.7-1.94,3,3,0,0,1,3.85,0A2.64,2.64,0,0,1,204.48,20.89Zm-1.74,1.29a1.53,1.53,0,0,0,.28-.55,2.66,2.66,0,0,0,.09-.75,2.5,2.5,0,0,0-.11-.78,1.5,1.5,0,0,0-.28-.52,1,1,0,0,0-.4-.3,1.27,1.27,0,0,0-.94,0,1.1,1.1,0,0,0-.41.29,1.45,1.45,0,0,0-.28.53,2.91,2.91,0,0,0,0,1.56,1.55,1.55,0,0,0,.28.52,1.09,1.09,0,0,0,.4.3,1.24,1.24,0,0,0,.48.09,1.21,1.21,0,0,0,.48-.1A1.06,1.06,0,0,0,202.74,22.18Z"></path>
							</g>
						</g>
					</svg>
				</i>
			</a>
		</div>
		<!-- Button mobile view to collapse sidebar menu -->
		<div class="navbar navbar-default" role="navigation">
			<div class="container-fluid">
				<div class="clearfix">
					<!-- Navbar-left -->
					<ul class="nav navbar-left">
						<li>
							<button class="button-menu-mobile open-left waves-effect">
								<i class="mdi mdi-menu"></i>
							</button>
						</li>
					</ul>
					<!-- Right(Notification) -->
					<ul class="nav navbar-right">
						<li class="dropdown user-box">
							<a href="" class="dropdown-toggle waves-effect user-link d-flex align-items-center" data-toggle="dropdown" aria-expanded="true">
								<img src="<?= $user_photo; ?>" alt="user-img" class="rounded-circle user-img"> <span class="ml-2"><?= $user_name; ?> <?= $user_surname; ?><br><?= $user_email; ?></span>
							</a>
							<ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list">
								<li><a href="/login/logout" class="dropdown-item pl-2"><i class="ti-power-off m-r-5"></i> Выйти</a></li>
							</ul>
						</li>
					</ul> <!-- end navbar-right -->
				</div>
			</div><!-- end container -->
		</div><!-- end navbar -->
	</div>
	<!-- Top Bar End -->

	<!-- ========== Left Sidebar Start ========== -->
	<div class="left side-menu">
		<div class="sidebar-inner slimscrollleft">
			<!--- Sidemenu -->
			<div id="sidebar-menu">
				<ul>
					<? foreach (array('Склады','Сырье', 'Производство') as $g) { ?>
            <li class="menu-title"><?= $g ?></li>
            <?php
            /*echo "<pre>STORAGE123";
            print_r($storage_menu);
            echo "</pre>";*/
            // foreach ($storage_list as $storage) {
            if(!empty($storage_menu[$g])){
            foreach ($storage_menu[$g] as $storage) {
              ?>
                <? if(\StandartRus\App\Core\Model::GetUserType() == IS_ADMIN || \StandartRus\App\Core\Model::GetUserType() == IS_MANAGER || \StandartRus\App\Core\Model::GetUserType() == IS_MANAGER_BIKOS && ($storage['id'] == 5 ||$storage['id'] == 6) || \StandartRus\App\Core\Model::GetUserType() == IS_PAINTER && $storage['id'] == 4){ ?>
              <li class="<?= $routes[2] == $storage['id'] ? 'active' : '' ?>">
                <a href="/storage/<?= $storage['id']; ?>/" class="waves-effect <?= $routes[2] == $storage['id'] ? 'active' : '' ?>"><i class="fa fa-<?= $storage['icon']; ?>"></i><span><?= $storage['name']; ?></span></a>
              </li>
              <? } ?>
              <?php
            }
            }
					}
					?>
                    <? if(\StandartRus\App\Core\Model::GetUserType() == IS_ADMIN || \StandartRus\App\Core\Model::GetUserType() == IS_MANAGER ){ ?>
					<li class="menu-title">Продажи</li>

					<li><a href="/deals/"><i class="fa fa-sticky-note"></i><span>Продажи</span></a></li>


	              <li class="<?= $routes[2] == $storage_menu['Продажи'][0]['id'] ? 'active' : '' ?>">
	                <a href="/storage/<?= $storage_menu['Продажи'][0]['id']; ?>/" class="waves-effect <?= $routes[2] == $storage_menu['Продажи'][0]['id'] ? 'active' : '' ?>"><i class="fa fa-<?= $storage_menu['Продажи'][0]['icon']; ?>"></i><span><?= $storage_menu['Продажи'][0]['name']; ?></span></a>
	              </li>
					<li>
						<a href="/sales/" class="waves-effect"><i class="fa fa-receipt"></i><span>Сделки</span></a>
					</li>
					<li><a href="/guides/"><i class="fa fa-sticky-note"></i><span>Справочники</span></a></li>
					<li><a href="/cars/"><i class="fa fa-sticky-note"></i><span>Машины</span></a></li>
					<li class="menu-title">Настройки</li>
					<li>
						<a href="/logs/" class="waves-effect"><i class="fa fa-clipboard-list"></i><span>Логи</span></a>
					</li>
                    <? } ?>
					<? if(\StandartRus\App\Core\Model::GetUserType() == IS_ADMIN){ ?>
					<li>
                        <a href="/users/" class="waves-effect"><i class="fa fa-receipt"></i><span>Пользователи</span></a>
                    </li>
					<? } ?>
				</ul>
			</div>
			<!-- Sidebar -->
			<div class="clearfix"></div>
			<div class="help-box">
				<h5 class="text-muted m-t-0">Тех. поддержка</h5>
				<p class=""><span class="text-custom">E-Mail:</span> <br/> support@support.com</p>
				<p class="m-b-0"><span class="text-custom">Телефон:</span> <br/> +7 (999) 999-9999</p>
			</div>

		</div>
		<!-- Sidebar -left -->
	</div>
	<!-- Left Sidebar End -->

	<div class="content-page">
		<?php include 'app/Views/Pages/' . $path_page . '/' . $name_page; ?>
		<footer class="footer d-flex justify-content-between align-items-center">
			<?= date('Y') ?> © Стандарт Термодерево
			<span style="max-width: 300px;" class="d-none d-sm-inline-block small text-dark">Сделано с умом <a href="http://arkvision.pro" target="_blank"><svg id="Слой_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 816 74"><style>.arklogo .st0 {
							enable-background: new;
							fill: white;
						}

						.stark {
							fill: #ef0001
						}</style><g class="st0"><path
								d="M4.3 70.3L42.4 3h15.1L96 70.3H78.4l-8.1-14.7H27.7l-7.8 14.7H4.3zm30.1-26.9h29.4L49.4 16.1l-15 27.3zM117.6 70.3V3h57.5c5.3 0 9.2 1 11.6 3.1 2.4 2.1 3.6 5.4 3.6 10v15.8c0 4.5-1.2 7.8-3.6 9.9-2.4 2.1-6.3 3.1-11.6 3.1h-8.8l31 25.4h-22.5l-27.1-25.4H133v25.4h-15.4zm50.6-55.6H133v18.7h35.2c2.7 0 4.6-.4 5.6-1.2 1-.8 1.5-2.2 1.5-4.2v-8.1c0-2-.5-3.3-1.5-4.1-1.1-.7-2.9-1.1-5.6-1.1zM222 70.3V3h15.8v28l37.3-28h22.6l-44.5 31.7 50 35.6h-25.1l-40.3-29.6v29.6H222zM348.2 70.3L309.3 3h17.9l27.2 48.6 26-48.6H396l-37 67.2h-10.8zM414.2 70.3V3h15.6v67.2h-15.6zM522.4 14.8h-44v14.4h41.9c6.6 0 11 1.1 13.3 3.4 2.3 2.2 3.5 6.4 3.5 12.5v9.2c0 6.1-1.2 10.3-3.5 12.5-2.3 2.2-6.8 3.4-13.3 3.4h-41.5c-6.6 0-11-1.1-13.3-3.4-2.3-2.2-3.5-6.4-3.5-12.5v-1.8l13.8-2.9v8h47.5V42.3h-41.9c-6.5 0-10.9-1.1-13.2-3.4-2.3-2.2-3.4-6.4-3.4-12.5V19c0-6.1 1.1-10.3 3.4-12.5 2.3-2.2 6.7-3.4 13.2-3.4h38.2c6.3 0 10.7 1.1 13.1 3.2 2.4 2.2 3.6 6 3.6 11.5v1.4l-13.8 3.2v-7.6z"></path></g><g
							class="st0"><path class="stark" d="M569.6 70.3V3h15.6v67.2h-15.6z"></path></g><g class="st0"><path
									d="M618.2 18.9c0-6.1 1.1-10.3 3.4-12.5 2.3-2.2 6.7-3.4 13.2-3.4h49.5c6.5 0 10.9 1.1 13.2 3.4 2.3 2.2 3.4 6.4 3.4 12.5v35.4c0 6.1-1.1 10.3-3.4 12.6-2.3 2.2-6.7 3.3-13.2 3.3h-49.5c-6.5 0-10.9-1.1-13.2-3.3-2.3-2.2-3.4-6.4-3.4-12.6V18.9zm15.6 38.7h51.6V15h-51.6v42.6zM733.8 70.3V3h10.7l47.3 39.6c1.6 1.3 3 2.6 4.3 3.7 1.2 1.2 2.4 2.4 3.4 3.6-.3-3.8-.4-6.4-.5-8-.1-1.6-.1-2.8-.1-3.6V3H813v67.2h-10.7l-49.4-41.5c-1.2-1.1-2.2-2-3.1-2.9-.9-.8-1.7-1.7-2.5-2.6.2 2.1.4 4 .5 5.9.1 1.8.2 3.4.2 4.8v36.3h-14.2z"></path></g></svg></a>
			</span>
		</footer>
	</div>
</div>

<script>
	var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/bootstrap.bundle.min.js"></script>
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
<!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/> -->
<!-- <script src="/assets/js/bootstrap.js"></script> -->
<script src="/assets/js/detect.js"></script>
<script src="/assets/js/fastclick.js"></script>
<script src="/assets/js/jquery.blockUI.js"></script>
<script src="/assets/js/waves.js"></script>
<script src="/assets/js/jquery.slimscroll.js"></script>
<script src="/assets/js/jquery.scrollTo.min.js"></script>
<script src="/assets/vendor/switchery/switchery.min.js"></script>
<script src="/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/assets/vendor/custombox/js/custombox.min.js"></script>
<!-- tables -->
<script src="/assets/js/jquery.tabledit.js"></script>


<!-- Counter js  -->
<script src="/assets/vendor/waypoints/jquery.waypoints.min.js"></script>
<script src="/assets/vendor/counterup/jquery.counterup.min.js"></script>

<!--Morris Chart-->
<script src="/assets/vendor/morris/morris.min.js"></script>
<script src="/assets/vendor/raphael/raphael-min.js"></script>

<!-- Dashboard init -->
<script src="/assets/pages/jquery.dashboard.js"></script>
<script src="/assets/vendor/select2/js/select2.full.js"></script>
<script src="/assets/vendor/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js"></script>

<!-- App js -->
<script src="/assets/js/jquery.core.js"></script>
<script src="/assets/js/jquery.app.js"></script>

<!-- <script src="/assets/js/datatables.min.js"></script> -->
<script src="/assets/js/datatables.js"></script>

<!-- libs -->
<script src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
<!-- <script src="/assets/js/dataTables.buttons.js"></script> -->
<script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js"></script>
<!-- <script src="/assets/js/buttons.html5.js"></script> -->
<script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.print.min.js"></script>
<!-- <script src="/assets/js/buttons.print.js"></script> -->


<script src="/assets/js/app.js"></script>
<script src="/assets/js/guides.js"></script>
<script src="/assets/js/deals.js"></script>
<? if(isset($data['names'])){?>
    <script src="/assets/js/jquery-ui.js"></script>
    <link rel="stylesheet" href="/assets/css/jquery-ui.css">
<script>
    var names = [
		<?php
		$name_str = '';
		if(!empty($data['names'])){
		foreach($data['names'] as $name){
			$name_str.='"'.$name.'",';
    }}
    echo trim($name_str,",");
        ?>
    ];
    /*$( ".form-autocomplete-name" ).attr("autocomplete","off");
    $( ".form-autocomplete-name" ).autocomplete({
        source: names
    });*/
</script>
<?}?>
</body>
</html>