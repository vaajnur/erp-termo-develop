<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">

	<!-- App favicon -->
	<link rel="shortcut icon" href="/assets/images/favicon.ico">
	<!-- App title -->
	<title>Стандарт ERP - Авторизация</title>

	<!-- App css -->
	<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="/assets/css/icons.css"/>
	<link rel="stylesheet" type="text/css" href="/assets/css/style.css"/>

	<script src="/assets/js/modernizr.min.js"></script>
</head>
<body class="bg-transparent">

<?php include 'app/Views/Pages/' . $path_page . '/' . $name_page; ?>

<script>
	var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/bootstrap.bundle.min.js"></script>
<script src="/assets/js/detect.js"></script>
<script src="/assets/js/fastclick.js"></script>
<script src="/assets/js/jquery.blockUI.js"></script>
<script src="/assets/js/waves.js"></script>
<script src="/assets/js/jquery.slimscroll.js"></script>
<script src="/assets/js/jquery.scrollTo.min.js"></script>

<!-- App js -->
<script src="/assets/js/jquery.core.js"></script>
<script src="/assets/js/jquery.app.js"></script>

<script src="/assets/js/script.js"></script>

</body>
</html>