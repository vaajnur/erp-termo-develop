<!DOCTYPE html>
<html>
<head>
	<!-- Google Tag Manager -->
	<script>(function (w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start':
					new Date().getTime(), event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-KVNHNJC');</script>
	<!-- End Google Tag Manager -->

	<!-- Title -->
	<title><?= $mess[$active_folder[0] . '_TITLE'] ?></title>

	<!-- Required Meta Tags Always Come First -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="description" content="<?= $mess[$active_folder[0] . '_DESCRIPTION'] ?>">

	<!-- Favicon -->
	<link rel="shortcut icon" href="/favicon.ico">

	<!-- Google Fonts -->
	<!--<link href="//fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">-->

	<!-- Favicon -->
	<link href="/favicon.ico" rel="icon" type="image/x-icon">
	<link rel="apple-touch-icon" sizes="180x180" href="/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="57x57" href="/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/assets/img/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/assets/img/favicon/favicon-16x16.png">
	<link rel="manifest" href="/site.webmanifest">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="theme-color" content="#ffffff">

	<!-- Facebook -->
	<meta property="og:url" content="https://4walls.pro">
	<meta property="og:title" content="4 Walls">
	<meta property="og:description" content="Дизайн и ремонт квартир в Санкт-Петербурге и области">
	<meta property="og:image" content="http://4walls.pro/assets/img/og-image.jpg">
	<meta property="og:image:secure_url" content="https://4walls.pro/assets/img/og-image.jpg">
	<meta property="og:image:type" content="image/jpg">
	<meta property="og:image:width" content="1080">
	<meta property="og:image:height" content="1080">
	<meta property="og:type" content="website">

	<!-- Twitter -->
	<meta name="twitter:site" content="@4wallspro">
	<meta name="twitter:creator" content="@4wallspro">
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:title" content="4Walls">
	<meta name="twitter:description" content="Дизайн и ремонт квартир в Санкт-Петербурге и области">
	<meta name="twitter:image" content="https://4walls.pro/assets/img/og-image.jpg">

	<!-- CSS Implementing Plugins -->
	<link rel="stylesheet" href="/assets/vendor/font-awesome/css/all.min.css">

	<!-- CSS Front Template -->
	<link rel="stylesheet" href="/assets/css/theme.min.css">
	<link rel="stylesheet" href="/assets/css/style.css">
</head>
<body class="bg-img-hero-fixed" style="background-image: url(../../assets/svg/illustrations/error-404.svg);">
<!-- Google Tag Manager (noscript) -->
<noscript>
	<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KVNHNJC" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- ========== HEADER ========== -->
<header id="header" class="u-header u-header--bg-transparent u-header--abs-top">
	<div class="u-header__section">
		<div id="logoAndNav" class="container">
			<!-- Nav -->
			<nav class="navbar navbar-expand u-header__navbar">
				<!-- Logo -->
				<a class="navbar-brand u-header__navbar-brand u-header__navbar-brand-center" href="/" aria-label="4 Walls">
					<img src="/assets/svg/logos/logo.svg" class="mt-2 mb-2 mt-md-0 mb-md-0 space-top-1" alt="4 Walls">
				</a>
				<!-- End Logo -->
			</nav>
			<!-- End Nav -->
		</div>
	</div>
</header>
<!-- ========== END HEADER ========== -->

<!-- ========== MAIN ========== -->
<?php include 'app/Views/Pages/' . $path_page . '/' . $name_page; ?>
<!-- ========== END MAIN ========== -->

<!-- ========== FOOTER ========== -->
<footer class="position-md-absolute right-md-0 bottom-md-0 left-md-0">
	<div class="container">
		<div class="d-flex justify-content-between align-items-center space-1">
			<!-- Copyright -->
			<p class="small text-muted mb-0">4Walls <span class="copyright_simbol">&copy;</span> Все права защищены, <?= date('Y') ?> г.</p>
			<!-- End Copyright -->

			<!-- Social Networks -->
			<ul class="list-inline mb-0">
				<li class="list-inline-item">
					<a class="btn btn-sm btn-icon btn-soft-secondary btn-bg-transparent" href="https://www.instagram.com/4wallspro/" target="_blank">
						<span class="fab fa-instagram btn-icon__inner"></span>
					</a>
				</li>
				<li class="list-inline-item">
					<a class="btn btn-sm btn-icon btn-soft-secondary btn-bg-transparent" href="https://vk.com/4walls_pro" target="_blank">
						<span class="fab fa-vk btn-icon__inner"></span>
					</a>
				</li>
				<li class="list-inline-item">
					<a class="btn btn-sm btn-icon btn-soft-secondary btn-bg-transparent" href="https://www.facebook.com/4wallsprogroup/" target="_blank">
						<span class="fab fa-facebook-f btn-icon__inner"></span>
					</a>
				</li>
			</ul>
			<!-- End Social Networks -->
		</div>
	</div>
</footer>
<!-- ========== END FOOTER ========== -->

<!-- JS Global Compulsory -->
<script src="/assets/vendor/jquery/dist/jquery.min.js"></script>
<script src="/assets/vendor/jquery-migrate/dist/jquery-migrate.min.js"></script>
<script src="/assets/vendor/popper.js/dist/umd/popper.min.js"></script>
<script src="/assets/vendor/bootstrap/bootstrap.min.js"></script>

<!-- JS Implementing Plugins -->

<!-- JS Front -->
<script src="/assets/js/hs.core.js"></script>
</body>