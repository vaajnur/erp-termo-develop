<?php
if (isset($data['pagination'])) {
	?>
	<!-- Pagination -->
	<nav aria-label="Page navigation">
		<ul class="pagination mb-0">
			<?php
			foreach ($data['pagination'] as $pagination) {
				if (isset($pagination['arrow_prev']) && $pagination['arrow_prev'] === true) {
					?>
					<li class="page-item ml-0">
						<a class="page-link" href="/<?= $pagination['url']; ?>/page/<?= $pagination['id']; ?>/" aria-label="Назад">
							<span aria-hidden="true">&laquo;</span>
							<span class="sr-only">Назад</span>
						</a>
					</li>
					<?php
				}
				?>

				<?php
				if (isset($pagination['active'])) {
					?>
					<li class="page-item <?= $pagination['active'] === true ? 'active' : '' ?>"><a class="page-link" href="/<?= $pagination['url']; ?>/page/<?= $pagination['id']; ?>/"><?= $pagination['id']; ?></a></li>
					<?php
				}
				?>

				<?php
				if (isset($pagination['space']) && $pagination['space'] === true) {
					?>
					<li class="page-item disabled"><a class="page-link">...</a></li>
					<?php
				}
				?>

				<?php
				if (isset($pagination['arrow_next']) && $pagination['arrow_next'] === true) {
					?>
					<li class="page-item ml-0">
						<a class="page-link" href="/<?= $pagination['url']; ?>/page/<?= $pagination['id']; ?>/" aria-label="Вперед">
							<span aria-hidden="true">&raquo;</span>
							<span class="sr-only">Вперед</span>
						</a>
					</li>
					<?php
				}
			}
			?>
		</ul>
	</nav>
	<!-- End Pagination -->
	<?php
}