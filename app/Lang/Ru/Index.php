<?php
$mess['PLACEHOLDER_SEARCH'] = "Поиск";

$mess['MENU_PROFILE'] = "Мой профиль";
$mess['MENU_SETTINGS'] = "Настройки";
$mess['MENU_EXIT'] = "Выход";

$mess['MENU_NAV_TITLE'] = "Навигация";



$mess['DETAIL_TITLE'] = "Склады";
$mess['LOGIN_TITLE'] = "Авторизация";
$mess['CARS_TITLE'] = "Машины";
$mess['404_TITLE'] = "404";
$mess['GUIDES_TITLE'] = "Справочники";
$mess['LOGS_TITLE'] = "Логи";
$mess['SALES_TITLE'] = "Сделки";
$mess['SALESARCHIVE_TITLE'] = "Архив сделок";
$mess['STORAGE_TITLE'] = "Склады";
$mess['TEST_TITLE'] = "TEST";
$mess['USERS_TITLE'] = "Пользователи";

$mess['JANUARY'] = "января";
$mess['FEBRUARY'] = "февраля";
$mess['MARCH'] = "марта";
$mess['APRIL'] = "апреля";
$mess['MAY'] = "мая";
$mess['JUNE'] = "июня";
$mess['JULY'] = "июля";
$mess['AUGUST'] = "августа";
$mess['SEPTEMBER'] = "сентября";
$mess['OCTOBER'] = "октября";
$mess['NOVEMBER'] = "ноября";
$mess['DECEMBER'] = "декабря";

$mess['JANUARY_NOM'] = "Январь";
$mess['FEBRUARY_NOM'] = "Февраль";
$mess['MARCH_NOM'] = "Март";
$mess['APRIL_NOM'] = "Апрель";
$mess['MAY_NOM'] = "Май";
$mess['JUNE_NOM'] = "Июнь";
$mess['JULY_NOM'] = "Июль";
$mess['AUGUST_NOM'] = "Август";
$mess['SEPTEMBER_NOM'] = "Сентябрь";
$mess['OCTOBER_NOM'] = "Октябрь";
$mess['NOVEMBER_NOM'] = "Ноябрь";
$mess['DECEMBER_NOM'] = "Декабрь";

$mess['NAME_DAY_NOM_ONLY'] = "день";
$mess['NAME_DAY_NOM_MULTI'] = "дня";
$mess['NAME_DAY_PAR_MULTI'] = "дней";
$mess['MONTH_DAY_NOM_ONLY'] = "месяц";
$mess['MONTH_DAY_NOM_MULTI'] = "месяца";
$mess['MONTH_DAY_PAR_MULTI'] = "месяцев";
$mess['YEAR_DAY_NOM_ONLY'] = "год";
$mess['YEAR_DAY_NOM_MULTI'] = "года";
$mess['YEAR_DAY_PAR_MULTI'] = "лет";
$mess['DATE_NAME_BACK'] = "назад";
$mess['DATE_NAME_TODAY'] = "Сегодня";
$mess['DATE_NAME_AND'] = "и";