<?php

namespace StandartRus\App\Models;

use \StandartRus\App\Core\Controller;
use \StandartRus\App\Core\Model;

/**
 * Class Storage
 *
 * Модель складов.
 *
 * @author Матовников Александр matovnikovaleks@mail.ru
 * @package StandartRus\App\Models
 */
class Storage extends Model
{

	/**
	 * ID элемента
	 *
	 * Используется в различных ситуациях. Например для передачи ID партии в функцию ее изменения
	 *
	 * @var
	 */
	var $id;
	/**
	 * ID склада
	 *
	 * @var
	 */
	var $storage;
	/**
	 * ID группы из таблицы consignment
	 *
	 * @var
	 */
	var $consignment;
	/**
	 * ID породы из таблицы breed_list
	 *
	 * @var
	 */
	var $breed;
	/**
	 * ID оттенка породы из таблицы tint_list
	 *
	 * @var
	 */
	var $tint;
	/**
	 * Номер упаковки
	 *
	 * @var
	 */
	var $pack;
	/**
	 * Наименования продукции
	 *
	 * @var
	 */
	var $name;
	/**
	 * Профиль
	 *
	 * @var
	 */
	var $profile;
	/**
	 * Количество
	 *
	 * @var
	 */
	var $quantity;
	/**
	 * Толщина
	 *
	 * @var
	 */
	var $thickness;
	/**
	 * Ширина
	 *
	 * @var
	 */
	var $width;
	/**
	 * Длина
	 *
	 * @var
	 */
	var $length;
	/**
	 * Объем
	 *
	 * Высчитывается автоматически по формуле, но если изменен вручную, то передается в модель для фиксации изменений.
	 * Т.е. иногда формула неверно высчитывает объем и менеджер вручную заносит правильное значение.
	 *
	 * @var
	 */
	var $volume_m3;
	/**
	 * Массив с данными
	 *
	 * Используется для передачи выбранных позиций из партий для перемещения по складам
	 *
	 * @var
	 */
	var $data;
	/**
	 * Массив с данными на покраску
	 *
	 * Используется для передачи параметров покраски
	 *
	 * @var
	 */
	var $tocolor;
	var $postavka_nomer;
	var $postavka_postavsik;
	var $postavka_dispecer;
	var $postavka_adres;
	var $postavka_dataop;
	var $postavka_dataof;
	var $postavka_datapp;
	var $postavka_datapf;
	var $postavka_markaam;
	var $postavka_nomeram;
	var $postavka_obiem;
	/**
	 * Дата завершения производства
	 *
	 * @var
	 */
	var $date;
	var $date_finish;
	/**
	 * ID партии
	 *
	 * @var
	 */
	var $consignment_id_success;
	/**
	 * ID цвета покраски
	 *
	 * @var
	 */
	var $color;
	/**
	 * Статус партии
	 *
	 * @var
	 */
	var $status;
	var $description;

	/**
	 * Получение списка партий на выбранном складе
	 *
	 * @return array|bool
	 */
	public function GetConsignments()
	{
		// $result = mysqli_query($this->connection, "SELECT * FROM `consignment` WHERE `id` > '0' && `storage` = '{$this->storage}' && `breed` = '{$this->breed}' && `tint` = '{$this->tint}' && `status` = '0' && `delete` = '0' ORDER BY `date_create` DESC");
		$tintsql = "";
		// if ($this->tint != 'all') $tintsql = " && `tint` = '{$this->tint}'";
		$subsql = "`id` > 0";
		if ($this->breed != 'all') $subsql = "`id` IN (SELECT `consignment` FROM `raw_materials` WHERE `breed` = '{$this->breed}' ".($this->profile != 'all'?" AND `profile` = '{$this->profile}'":"")."    )";
		// var_dump($subsql);
		// var_dump("SELECT * FROM `consignment` WHERE {$subsql} {$tintsql} && `storage` = '{$this->storage}' && `status` = '0' && `delete` = '0' ORDER BY `date_create` DESC");
		$result = mysqli_query($this->connection, "SELECT * FROM `consignment` WHERE {$subsql} {$tintsql} && `storage` = '{$this->storage}' && `status` = '0' && `delete` = '0' ORDER BY `date_create` DESC");
		if (mysqli_num_rows($result) > 0) {
			$array = array();
			while ($row = mysqli_fetch_array($result)) {
				$storage_info = array();
				$raw_materials = array();

				$storage_info['id'] = $row['id'];
				$storage_info['date_create'] = $row['date_create'];
				$controller = new Controller();
				$storage_info['date_create_str'] = $controller->FormattingDate($row['date_create'], true);
				$storage_info['date_finish'] = $row['date_finish'];
				$storage_info['status'] = $row['status'];
				$storage_info['status_repair'] = $row['status_repair'];
				$storage_info['description'] = $row['description'];
        
        		// Масло
        		if((int)$row['tocolor_maslo'] > 0)
					$storage_info['tocolor_maslo'] = $this->GetMasloById($row['tocolor_maslo']);
				else
					$storage_info['tocolor_maslo'] = $row['tocolor_maslo'];

				// Цвет
        		if((int)$row['tocolor_select_color'] > 0)
					$storage_info['tocolor_select_color'] = $this->GetColorById($row['tocolor_select_color']);
				else
					$storage_info['tocolor_select_color'] = $row['tocolor_select_color'];

				$storage_info['tocolor_sides'] = $row['tocolor_sides'];
				$storage_info['tocolor_toretc'] = $row['tocolor_toretc'];
				$storage_info['tocolor_face'] = $row['tocolor_face'];
				$storage_info['tocolor_back'] = $row['tocolor_back'];
				$storage_info['tocolor_zakazchik'] = $row['tocolor_zakazchik'];
				$storage_info['tocolor_remont_shpaklevka'] = $row['tocolor_remont_shpaklevka'];
        
				$storage_info['postavka_nomer'] = $row['postavka_nomer'];
				$storage_info['postavka_postavsik'] = $row['postavka_postavsik'];
				$storage_info['postavka_dispecer'] = $row['postavka_dispecer'];
				$storage_info['postavka_adres'] = $row['postavka_adres'];
				$storage_info['postavka_dataop'] = $row['postavka_dataop'];
				$storage_info['postavka_dataof'] = $row['postavka_dataof'];
				$storage_info['postavka_datapp'] = $row['postavka_datapp'];
				$storage_info['postavka_datapf'] = $row['postavka_datapf'];
				$storage_info['postavka_markaam'] = $row['postavka_markaam'];
				$storage_info['postavka_nomeram'] = $row['postavka_nomeram'];
				$storage_info['postavka_obiem'] = $row['postavka_obiem'];
				$storage_info['title'] = $row['title'];

				// $result_raw_materials = mysqli_query($this->connection, "SELECT `raw_materials`.*, `color_list`.`name` AS `color_name` FROM `raw_materials` LEFT JOIN `color_list` ON `raw_materials`.`color` = `color_list`.`id` WHERE `raw_materials`.`id` > '0' && `raw_materials`.`quantity` > '0' && `raw_materials`.`consignment` = '{$row['id']}'");
		        $subsql = "";
		        // фильтр по породе в продукции
		        if ($this->breed != 'all') $subsql = " && `raw_materials`.`breed` = '{$this->breed}'";
		        // фильтр по профилю в продукции
		        if ($this->profile != 'all') $subsql .= " && `raw_materials`.`profile` = '{$this->profile}'";
		        // сортировка по длине
		        $order_by = " ORDER BY `raw_materials`.`length` ASC";

				$result_raw_materials = mysqli_query($this->connection, "SELECT `raw_materials`.*, `color_list`.`name` AS `color_name` FROM `raw_materials` LEFT JOIN `color_list` ON `raw_materials`.`color` = `color_list`.`id` WHERE `raw_materials`.`id` > '0' && `raw_materials`.`quantity` > '0' && `raw_materials`.`consignment` = '{$row['id']}' {$subsql}" . $order_by);
				if (mysqli_num_rows($result_raw_materials) > 0) {
					while ($row_raw_materials = mysqli_fetch_assoc($result_raw_materials)) {
						$raw_materials[] = $row_raw_materials;
					}
				}

				$storage_info['raw_materials'] = $raw_materials;

				$array[] = $storage_info;
			}
			return $array;
		}
		return false;
	}
  
	/**
	 * Получение списка групп на всех складах
	 *
	 * @return array|bool
	 */
	public function GetConsignmentsAllStorage()
	{
		$result = mysqli_query($this->connection, "SELECT * FROM `consignment` WHERE `id` > '0' && `status` = '0' && `delete` = '0' ORDER BY `date_create` DESC");
		if (mysqli_num_rows($result) > 0) {
			$array = array();
			while ($row = mysqli_fetch_array($result)) {
				$storage_info = array();
				$raw_materials = array();

				$storage_info['id'] = $row['id'];
				$storage_info['storage'] = $row['storage'];
				$storage_info['date_create'] = $row['date_create'];
				$controller = new Controller();
				$storage_info['date_create_str'] = $controller->FormattingDate($row['date_create'], true);
				$storage_info['date_finish'] = $row['date_finish'];
				$storage_info['status'] = $row['status'];
				$storage_info['status_repair'] = $row['status_repair'];
				$storage_info['description'] = $row['description'];

				$array[$storage_info['storage']][] = $storage_info;
			}
			return $array;
		}
		return false;
	}

	/**
	 * Создание новой партии
	 *
	 * @return bool
	 */
	public function AddConsignments()
	{
	    if ($this->consignment=='new') {
	      $time = time();
	      mysqli_query($this->connection, "INSERT INTO `consignment`(`storage`, `breed`, `tint`, `date_create`,`date_finish`, `user_create`,`postavka_nomer`, `postavka_postavsik`, `postavka_dispecer`, `postavka_adres`, `postavka_dataop`, `postavka_dataof`, `postavka_datapp`, `postavka_datapf`, `postavka_markaam`, `postavka_nomeram`, `postavka_obiem`, `title`) VALUES ('{$this->storage}', '{$this->breed}', '{$this->tint}', '$time', '{$this->date_finish}', '{$_COOKIE['user_id']}' , '{$this->postavka_nomer}', '{$this->postavka_postavsik}', '{$this->postavka_dispecer}', '{$this->postavka_adres}', '{$this->postavka_dataop}', '{$this->postavka_dataof}', '{$this->postavka_datapp}', '{$this->postavka_datapf}', '{$this->postavka_markaam}', '{$this->postavka_nomeram}', '{$this->postavka_obiem}', '{$this->title}')");
	      $id_consignments = mysqli_insert_id($this->connection);
	    } else {
	      $id_consignments = $this->consignment;
	    }

		foreach ($this->name as $k => $name) {
			if (!empty($name)) {
				$thickness = $this->thickness[$k] * 1000;
				$width = $this->width[$k] * 1000;
				$length = $this->length[$k] * 1000;
				$volume_m3 = $this->volume_m3[$k] * 1000;

				$volume_m3_calc = number_format($this->quantity[$k] * $this->width[$k] / 1000 * $this->length[$k] / 1000 * $this->thickness[$k] / 1000, 3, '.', ' ');

				//Если рассчитанный автоматически объем равен введенному объему, то оставляем поле пустым, чтобы в шаблоне выводилось рассчитанное поле. Если же поле отличается, т.е. менеджер сам ввел данные, то выводим менеджерское число
				if ($this->volume_m3[$k] == $volume_m3_calc) $volume_m3 = 0;

				$this->pack[$k] = intval($this->pack[$k]);
				  $_breed = 0;
				if (is_array($this->breed)) {
					$_breed = $this->breed[$k];
				}elseif (is_string($this->breed)){
					$_breed = $this->breed;
				} 


				// Новые профили
				if($this->profile[$k] != false && $this->profile[$k] != ''){
					$profiles =  mysqli_query($this->connection, "SELECT * FROM `profile_list` WHERE `name` = '{$this->profile[$k]}'");
					if (mysqli_num_rows($profiles) == 0) {
						mysqli_query($this->connection, "INSERT INTO profile_list(name) VALUES('{$this->profile[$k]}')");
					}
				}
				// Новые породы
				if($_breed != '' && (int)$_breed == false){
					$breeds =  mysqli_query($this->connection, "SELECT * FROM `breed_list` WHERE `name` = '{$_breed}'");
					if (mysqli_num_rows($breeds) == 0) {
						mysqli_query($this->connection, "INSERT INTO breed_list(name) VALUES('$_breed')");
						$_breed = mysqli_insert_id($this->connection);
					}else{
						$res1 = mysqli_query($this->connection, "SELECT id FROM breed_list WHERE name = '$_breed'");
						$row = mysqli_fetch_array($res1);
						$_breed  = $row['id'];
					}
				}
				// Новые сорты
				if($name != false && $name != ''){
					$names =  mysqli_query($this->connection, "SELECT * FROM `cort_list` WHERE `name` = '$name'");
					if (mysqli_num_rows($names) == 0) {
						mysqli_query($this->connection, "INSERT INTO cort(name) VALUES('$name')");
					}
				}

				if(strlen($this->profile[$k]) > 0)
					mysqli_query($this->connection, "INSERT INTO `raw_materials`(`consignment`, `breed`, `pack`, `name`, `quantity`, `thickness`, `width`, `length`, `volume_m3`, `date_create`, `user_create`, `profile`) VALUES ('$id_consignments', '{$_breed}', '{$this->pack[$k]}', '{$this->name[$k]}', '{$this->quantity[$k]}', '$thickness', '$width', '$length', '$volume_m3', '$time', '{$_COOKIE['user_id']}', '{$this->profile[$k]}')");
				else
					mysqli_query($this->connection, "INSERT INTO `raw_materials`(`consignment`, `breed`, `pack`, `name`, `quantity`, `thickness`, `width`, `length`, `volume_m3`, `date_create`, `user_create`) VALUES ('$id_consignments', '{$_breed}', '{$this->pack[$k]}', '{$this->name[$k]}', '{$this->quantity[$k]}', '$thickness', '$width', '$length', '$volume_m3', '$time', '{$_COOKIE['user_id']}')");
			}
		}

		Model::AddLog('Добавил партию № ' . $id_consignments);

		return true;
	}

	public function editConsignments($id)
	{
		mysqli_query($this->connection, "UPDATE `consignment`  SET postavka_nomer = '{$this->postavka_nomer}', postavka_postavsik = '{$this->postavka_postavsik}', postavka_dispecer = '{$this->postavka_dispecer}', postavka_adres = '{$this->postavka_adres}', postavka_dataop = '{$this->postavka_dataop}', postavka_dataof = '{$this->postavka_dataof}', postavka_datapp = '{$this->postavka_datapp}', postavka_datapf = '{$this->postavka_datapf}', postavka_markaam = '{$this->postavka_markaam}', postavka_nomeram = '{$this->postavka_nomeram}', postavka_obiem = '{$this->postavka_obiem}' WHERE id = '$id'");
		return true;
	}

	/**
	 * Удаление товара из партии по ID
	 *
	 * @return bool
	 */
	public function DeleteRawMaterial()
	{
		$result = mysqli_query($this->connection, "SELECT `consignment` FROM `raw_materials` WHERE `id` = '{$this->id}'");
		if (mysqli_num_rows($result) > 0) {
			$row = mysqli_fetch_array($result);

			mysqli_query($this->connection, "DELETE FROM `raw_materials` WHERE `id` = '{$this->id}'");

			$res = mysqli_query($this->connection, "SELECT COUNT(*) FROM `raw_materials` WHERE `consignment` = '{$row['consignment']}'");
			$total = mysqli_fetch_row($res)[0];

			if ($total < 1) {
				mysqli_query($this->connection, "DELETE FROM `consignment` WHERE `id` = '{$row['consignment']}'");
			}

			Model::AddLog('Удалил продукцию № ' . $this->id . ' из партии № ' . $row['consignment']);
			return true;
		}
		return false;
	}

	/**
	 * Удаление партии по ID
	 *
	 * @return bool
	 */
	public function DeleteAllMaterial()
	{
		mysqli_query($this->connection, "DELETE FROM `raw_materials` WHERE `consignment` = '{$this->id}'");
		mysqli_query($this->connection, "DELETE FROM `consignment` WHERE `id` = '{$this->id}'");

    Model::AddLog('Удалил партию № ' . $this->id);
    return true;
	}

	/**
	 * Сохранение изменений товара из партии по ID
	 *
	 * @return bool | int
	 */
	public function SaveRawMaterial()
	{
		$thickness = $this->thickness * 1000;
		$width = $this->width * 1000;
		$length = $this->length * 1000;
		$volume_m3 = $this->volume_m3 * 1000;

		$volume_m3_calc = number_format($this->quantity * $this->width / 1000 * $this->length / 1000 * $this->thickness / 1000, 3, '.', ' ');

		//Если рассчитанный автоматически объем равен введенному объему, то оставляем поле пустым, чтобы в шаблоне выводилось рассчитанное поле. Если же поле отличается, т.е. менеджер сам ввел данные, то выводим менеджерское число
		if ($this->volume_m3 == $volume_m3_calc) $volume_m3 = 0;

		// Новые профили
		if($this->profile != ''){
			$profiles =  mysqli_query($this->connection, "SELECT * FROM `profile_list` WHERE `name` = '{$this->profile}'");
			if (mysqli_num_rows($profiles) == 0) {
				mysqli_query($this->connection, "INSERT INTO profile_list(name) VALUES('{$this->profile}')");
			}
		}

		// Новые сорта
		if($this->name != ''){
			$corts =  mysqli_query($this->connection, "SELECT * FROM `cort_list` WHERE `name` = '{$this->name}'");
			if (mysqli_num_rows($corts) == 0) {
				mysqli_query($this->connection, "INSERT INTO cort_list(name) VALUES('{$this->name}')");
			}
		}

		// Новые породы
		if($this->breed != '' && (int)$this->breed == false){
			$breeds =  mysqli_query($this->connection, "SELECT * FROM `breed_list` WHERE `name` = '{$this->breed}'");
			if (mysqli_num_rows($breeds) == 0) {
				mysqli_query($this->connection, "INSERT INTO breed_list(name) VALUES('{$this->breed}')");
				$this->breed = mysqli_insert_id($this->connection);
			}else{
				$res1 = mysqli_query($this->connection, "SELECT id FROM breed_list WHERE name = '{$this->breed}'");
				$row = mysqli_fetch_array($res1);
				$this->breed  = $row['id'];
			}
		}


		// дата сушки
		if($this->date_finish != false){
			mysqli_query($this->connection, "UPDATE `consignment` SET `date_finish`='{$this->date_finish}' WHERE `id` = '{$this->consignment_id_success}'");
			var_dump("UPDATE `consignment` SET `date_finish`='{$this->date_finish}' WHERE `id` = '{$this->consignment_id_success}'");
		}

		$result = mysqli_query($this->connection, "SELECT `id` FROM `raw_materials` WHERE `id` = '{$this->id}'");
		if (mysqli_num_rows($result) > 0) {
			$row = mysqli_fetch_array($result);
			// $row['pack'] = intval($row['pack']);
			$row['pack'] = intval($this->pack);
			mysqli_query($this->connection, "UPDATE `raw_materials` SET `pack`='{$row['pack']}',`name`='{$this->name}',`breed`='{$this->breed}',`quantity`='{$this->quantity}',`thickness`='$thickness',`width`='$width',`length`='$length',`volume_m3`='$volume_m3',`profile`='{$this->profile}' WHERE `id` = '{$this->id}'");


			if (!empty($this->color)) {
				mysqli_query($this->connection, "UPDATE `raw_materials` SET `color`='{$this->color}' WHERE `id` = '{$this->id}'");
			}

			Model::AddLog('Изменил продукцию № ' . $this->id);
			return true;
		} else {
			$time = time();
			mysqli_query($this->connection, "INSERT INTO `raw_materials`(`consignment`, `pack`, `name`, `quantity`, `profile`, `breed`, `thickness`, `width`, `length`, `volume_m3`, `date_create`,  `user_create`) VALUES ('{$this->consignment_id_success}', '{$this->pack}', '{$this->name}', '{$this->quantity}', '{$this->profile}', '{$this->breed}', '$thickness', '$width', '$length', '$volume_m3', '$time',  '{$_COOKIE['user_id']}')");
			$id = mysqli_insert_id($this->connection);

			Model::AddLog('Добавил продукцию № ' . $id . ' в партию № ' . $this->consignment_id_success);
			return $id;
		}
	}

	/**
	 * Отправка выбранных материалов с склада Сырье на склад Производство
	 *
	 * @return bool
	 */
	public function SendRawMaterialsToManufacture()
	{
		$time = time();
	    /** На покраску */
	    $tocolorF = array();
	    $tocolorV = array();
	    if(!empty($this->tocolor)){
			foreach ($this->tocolor as $v) {
				if (!empty($v[0]) && !empty($v[1])) {
					// Новое масло
					if($v[0] == 'maslo' && $v[1] != '' && (int)$v[1] == false){
						$oils =  mysqli_query($this->connection, "SELECT * FROM `maslo_list` WHERE `name` = '{$v[1]}'");
						if (mysqli_num_rows($oils) == 0) {
							mysqli_query($this->connection, "INSERT INTO maslo_list(name) VALUES('{$v[1]}')");
						}
					}
					// Новые цвета
					if($v[0] == 'select_color' && $v[1] != '' && (int)$v[1] == false){
						$colors1 =  mysqli_query($this->connection, "SELECT * FROM `color_list` WHERE `name` = '{$v[1]}'");
						if (mysqli_num_rows($colors1) == 0) {
							mysqli_query($this->connection, "INSERT INTO color_list(name) VALUES('{$v[1]}')");
						}					
					}
			        $tocolorF[] = $v[0];
			        $tocolorV[] = $v[1];
			      }
		    }
		}
	    $tocolorF = implode('` ,`tocolor_',$tocolorF);
	    $tocolorV = implode("' ,'",$tocolorV);
	    if (!empty($tocolorF)) $tocolorF = ' ,`tocolor_'.$tocolorF.'`';
	    if (!empty($tocolorV)) $tocolorV = " ,'".$tocolorV."'";
	    /** finish На покраску */
			// mysqli_query($this->connection, "INSERT INTO `consignment` (`storage`, `breed`, `tint`, `date_finish`, `date_create`, `user_create`) VALUES ('7', '{$this->breed}', '{$this->tint}', '{$this->date}', '$time', '{$_COOKIE['user_id']}')");

			// var_dump($this->connection, "INSERT INTO `consignment` (`storage`, `breed`, `tint`, `date_finish`, `date_create`, `user_create` {$tocolorF}) VALUES ('{$this->storage}', '{$this->breed}', '{$this->tint}', '{$this->date}', '$time', '{$_COOKIE['user_id']}' {$tocolorV} )");
	    // exit;
    
	    if ($this->consignment == 'new') {
	      mysqli_query($this->connection, "INSERT INTO `consignment` (`storage`, `breed`, `tint`, `date_finish`, `date_create`, `user_create` {$tocolorF}) VALUES ('{$this->storage}', '{$this->breed}', '{$this->tint}', '{$this->date}', '$time', '{$_COOKIE['user_id']}' {$tocolorV} )");
	      $id_consignments = mysqli_insert_id($this->connection);
	    } else {
	      $id_consignments = $this->consignment;
	    }
		$list_id = 0;

		foreach ($this->data as $v) {
			if (!empty($v[0]) && !empty($v[1])) {
				$result = mysqli_query($this->connection, "SELECT * FROM `raw_materials` WHERE `id` = '{$v[0]}' LIMIT 1");
				if (mysqli_num_rows($result) > 0) {
					$row = mysqli_fetch_array($result);
					if ($v[1] <= $row['quantity']) {
						mysqli_query($this->connection, "INSERT INTO `raw_materials`(`consignment`, `pack`, `breed`, `profile`, `name`, `quantity`, `thickness`, `width`, `length`, `parent_raw_material`, `date_create`, `user_create`) VALUES ('$id_consignments', '{$row['pack']}', '{$row['breed']}', '{$row['profile']}', '{$row['name']}', '{$v[1]}', '{$row['thickness']}', '{$row['width']}', '{$row['length']}', '{$row['id']}', '$time', '{$_COOKIE['user_id']}')");
						if ($row['volume_m3'] > 0) {
							$new_quantity = $row['quantity'] - $v[1];
							$new_volume = round(($row['volume_m3'] / $row['quantity']) * $new_quantity);
							mysqli_query($this->connection, "UPDATE `raw_materials` SET `quantity`=`quantity`-{$v[1]}, `volume_m3`='$new_volume' WHERE `id` = {$row['id']}");
						} else {
							mysqli_query($this->connection, "UPDATE `raw_materials` SET `quantity`=`quantity`-{$v[1]} WHERE `id` = '{$row['id']}'");
						}
						if (empty($list_id)) $list_id = "№ " . $row['id'] . " в количестве " . $v[1];
						else $list_id = $list_id . ", № " . $row['id'] . " в количестве " . $v[1];
					}
				}
			}
		}

		Model::AddLog('Отправил продукцию ' . $list_id . ' на склад "Производство" под партией № ' . $id_consignments);

		return true;
	}

	/**
	 * Создании партии для склада Продукция с конечными товарами после производства
	 *
	 * @return bool
	 */
	public function AddConsignmentsProducts()
	{
		$ids = [];
		$time = time();
		mysqli_query($this->connection, "INSERT INTO `consignment`(`storage`, `breed`, `tint`, `parent_consignment`, `date_create`, `user_create`) VALUES ('{$this->storage}', '{$this->breed}', '{$this->tint}', '{$this->consignment_id_success}', '$time', '{$_COOKIE['user_id']}')");
		$id_consignments = mysqli_insert_id($this->connection);
		$result = mysqli_query($this->connection, "SELECT * FROM `consignment` WHERE `id` = '{$this->consignment_id_success}' LIMIT 1");
		if (mysqli_num_rows($result) > 0) {
			$row = mysqli_fetch_array($result);
			if(intval($row['date_finish']) > 0){
				mysqli_query($this->connection, "UPDATE `consignment` SET `date_finish`='{$row['date_finish']}' WHERE `id` = '{$id_consignments}'");
			}
		}
		foreach ($this->name as $k => $v) {
			if (!empty($v)) {
				$thickness = $this->thickness[$k] * 1000;
				$width = $this->width[$k] * 1000;
				$length = $this->length[$k] * 1000;
				$volume_m3 = $this->volume_m3[$k] * 1000;

				$volume_m3_calc = number_format($this->quantity[$k] * $this->width[$k] / 1000 * $this->length[$k] / 1000 * $this->thickness[$k] / 1000, 3, '.', ' ');

				//Если рассчитанный автоматически объем равен введенному объему, то оставляем поле пустым, чтобы в шаблоне выводилось рассчитанное поле. Если же поле отличается, т.е. менеджер сам ввел данные, то выводим менеджерское число
				if ($this->volume_m3[$k] == $volume_m3_calc) $volume_m3 = 0;

				$this->pack[$k] = intval($this->pack[$k]);
				if(strlen($this->profile[$k]) > 0)
					mysqli_query($this->connection, "INSERT INTO `raw_materials`(`consignment`,`pack`, `name`, `quantity`, `thickness`, `width`, `length`, `volume_m3`, `date_create`, `user_create`, `profile`) VALUES ('$id_consignments', '{$this->pack[$k]}', '{$this->name[$k]}', '{$this->quantity[$k]}', '$thickness', '$width', '$length', '$volume_m3', '$time', '{$_COOKIE['user_id']}', '{$this->profile[$k]}')");
				else
					mysqli_query($this->connection, "INSERT INTO `raw_materials`(`consignment`,`pack`, `name`, `quantity`, `thickness`, `width`, `length`, `volume_m3`, `date_create`, `user_create`) VALUES ('$id_consignments', '{$this->pack[$k]}', '{$this->name[$k]}', '{$this->quantity[$k]}', '$thickness', '$width', '$length', '$volume_m3', '$time', '{$_COOKIE['user_id']}')");
				
				$ids[] = mysqli_insert_id($this->connection);
			}
		}

		Model::AddLog('Создал партию продукции № ' . $id_consignments);

		mysqli_query($this->connection, "UPDATE `consignment` SET `status`='1' WHERE `id` = '{$this->consignment_id_success}'");

		Model::AddLog('У партии производства № ' . $this->consignment_id_success . ' изменился статус на "Выполнена"');

		return $ids;
	}

	/**
	 * Отправка товаров с склада Продукция на склад Покраска
	 *
	 * @return bool
	 */
	public function SendProductsToRepair()
	{
		$time = time();
		mysqli_query($this->connection, "INSERT INTO `consignment`(`storage`, `breed`, `tint`, `date_finish`, `date_create`, `user_create`) VALUES ('4', '{$this->breed}', '{$this->tint}', '{$this->date}', '$time', '{$_COOKIE['user_id']}')");
		$id_consignments = mysqli_insert_id($this->connection);

		$list_id = 0;

		foreach ($this->data as $v) {
			if (!empty($v[0]) && !empty($v[1])) {
				$result = mysqli_query($this->connection, "SELECT * FROM `raw_materials` WHERE `id` = '{$v[0]}' LIMIT 1");
				if (mysqli_num_rows($result) > 0) {
					$row = mysqli_fetch_array($result);
					if ($v[1] <= $row['quantity']) {
						mysqli_query($this->connection, "INSERT INTO `raw_materials`(`consignment`,  `color`, `name`, `quantity`, `thickness`, `width`, `length`, `parent_raw_material`, `date_create`, `user_create`, `profile`) VALUES ('$id_consignments',  '{$v[2]}', '{$row['name']}', '{$v[1]}', '{$row['thickness']}', '{$row['width']}', '{$row['length']}', '{$row['id']}', '$time', '{$_COOKIE['user_id']}', '{$row['profile']}')");
						if ($row['volume_m3'] > 0) {
							$new_quantity = $row['quantity'] - $v[1];
							$new_volume = round(($row['volume_m3'] / $row['quantity']) * $new_quantity);
							mysqli_query($this->connection, "UPDATE `raw_materials` SET `quantity`=`quantity`-{$v[1]}, `volume_m3`='$new_volume' WHERE `id` = '{$row['id']}'");
						} else {
							mysqli_query($this->connection, "UPDATE `raw_materials` SET `quantity`=`quantity`-{$v[1]} WHERE `id` = '{$row['id']}'");
						}
						if (empty($list_id)) $list_id = "№ " . $row['id'] . " в количестве " . $v[1];
						else $list_id = $list_id . ", № " . $row['id'] . " в количестве " . $v[1];
					}
				}
			}
		}

		Model::AddLog('Отправил продукцию ' . $list_id . ' на склад "Покраска" под партией № ' . $id_consignments);

		return true;
	}

	/**
	 * Изменение статуса покраски партии на складе Покраска
	 *
	 * @return bool
	 */
	public function UpdateStatusConsignment()
	{
		mysqli_query($this->connection, "UPDATE `consignment` SET `status_repair`='{$this->status}' WHERE `id` = '{$this->id}'");
		Model::AddLog('У партии покраски № ' . $this->id . ' изменился статус');
		return true;
	}

	public function DeleteConsignment()
	{
		mysqli_query($this->connection, "UPDATE `consignment` SET `delete`='1' WHERE `id` = '{$this->id}'");

		$result = mysqli_query($this->connection, "SELECT * FROM `raw_materials` WHERE `consignment` = '{$this->id}'");
		if (mysqli_num_rows($result) > 0) {
			while ($row = mysqli_fetch_array($result)) {
				$parent_raw_material = $row['parent_raw_material'];
				mysqli_query($this->connection, "UPDATE `raw_materials` SET `quantity`='0' WHERE `id` = '{$row['id']}'");

				$result_raw_material = mysqli_query($this->connection, "SELECT * FROM `raw_materials` WHERE `id` = '$parent_raw_material'");
				$row_raw_material = mysqli_fetch_array($result_raw_material);

				if ($row_raw_material['volume_m3'] > 0) {
					$new_quantity = $row_raw_material['quantity'] + $row['quantity'];
					$new_volume = round(($row_raw_material['volume_m3'] / $row_raw_material['quantity']) * $new_quantity);

					mysqli_query($this->connection, "UPDATE `raw_materials` SET `quantity`=`quantity`+{$row['quantity']}, `volume_m3`='$new_volume' WHERE `id` = '$parent_raw_material'");
				} else {
					mysqli_query($this->connection, "UPDATE `raw_materials` SET `quantity`=`quantity`+{$row['quantity']} WHERE `id` = '$parent_raw_material'");
				}
			}
		}

		Model::AddLog('Партия № ' . $this->id . ' удалена');

		return true;
	}

	public function UpdateDescriptionConsignment()
	{
		mysqli_query($this->connection, "UPDATE `consignment` SET `description`='{$this->description}' WHERE `id` = '{$this->id}'");
		Model::AddLog('Изменено описание у партии № ' . $this->id);
		return true;
	}
	public function UpdateTitleConsignment()
	{
		mysqli_query($this->connection, "UPDATE `consignment` SET `title`='{$this->title}' WHERE `id` = '{$this->id}'");
		Model::AddLog('Изменено тайтл у продукции № ' . $this->id);
		return true;
	}

	/**
	 * объединить все
	 * @param  [type] $consign_ids [description]
	 * @return [type]              [description]
	 */
	public function join_consigns($consign_ids){
		$first_group = array_shift($consign_ids);
		$res = [];
		foreach ($consign_ids as $key => $value) {
			mysqli_query($this->connection, "UPDATE raw_materials SET consignment='$first_group' WHERE consignment='$value'");
			$res[] = "UPDATE raw_materials SET consignment='$first_group' WHERE consignment='$value'";
		}
		return $res;
	}

	/**
	 * объединить некоторые строки
	 * @param  [type] $raw_materials_ids [description]
	 * @param  [type] $storage           [description]
	 * @return [type]                    [description]
	 */
	public function join_raws($raw_materials_ids, $storage){
		
		$queries = [];
		$raw_info = [];
		
		foreach($raw_materials_ids as $raw1){
			$id = key($raw1);
			$res1 = mysqli_query($this->connection, "SELECT con.* FROM raw_materials r LEFT JOIN consignment con ON r.consignment=con.id WHERE r.id='$id'");
			if($row = mysqli_fetch_assoc($res1)){
				$raw_info[] = $row;
			}
		}
		$raw_info = array_reduce($raw_info, function($carry, $item){
			$carry = array_merge_recursive($carry, $item);
			array_walk($carry, function(&$item){
				if(is_array($item)){
					$item = array_unique(array_filter($item))[0];
				}
			});
			return ($carry);
		}, []);
		
		
		$time = time();
		mysqli_query($this->connection, "INSERT INTO `consignment`(
			`storage`, 
			description, 
		 `breed`, 
		 `tint`, 
		`date_create`, 
		`date_finish`, 
		`user_create`,
		`postavka_nomer`, 
		`postavka_postavsik`, 
		`postavka_dispecer`, 
		`postavka_adres`, 
		`postavka_dataop`, 
		`postavka_dataof`, 
		`postavka_datapp`, 
		`postavka_datapf`, 
		`postavka_markaam`, 
		`postavka_nomeram`, 
		`postavka_obiem`,
		tocolor_maslo,
		tocolor_select_color,
		tocolor_sides,
		tocolor_toretc,
		tocolor_face,
		tocolor_back
		) 
		VALUES (
		'$storage', 
		'{$raw_info['description']}', 
		'{$raw_info['breed']}', 
		'{$raw_info['tint']}', 
		'$time', 
		'{$raw_info['date_finish']}', 
		'{$_COOKIE['user_id']}' , 
		'{$raw_info['postavka_nomer']}', 
		'{$raw_info['postavka_postavsik']}', 
		'{$raw_info['postavka_dispecer']}', 
		'{$raw_info['postavka_adres']}', 
		'{$raw_info['postavka_dataop']}', 
		'{$raw_info['postavka_dataof']}', 
		'{$raw_info['postavka_datapp']}', 
		'{$raw_info['postavka_datapf']}', 
		'{$raw_info['postavka_markaam']}', 
		'{$raw_info['postavka_nomeram']}', 
		'{$raw_info['postavka_obie']}',
		'{$raw_info['tocolor_maslo']}',
		'{$raw_info['tocolor_select_color']}',
		'{$raw_info['tocolor_sides']}',
		'{$raw_info['tocolor_toretc']}',
		'{$raw_info['tocolor_face']}',
		'{$raw_info['tocolor_back']}'
		)
		");
	      $id_new_consignments = mysqli_insert_id($this->connection);

		foreach($raw_materials_ids as $raw){
			$id = key($raw);
			$quant = $raw[$id];
			$res = mysqli_query($this->connection, "SELECT * FROM raw_materials WHERE id='$id'");
			// $queries[] = "SELECT * FROM raw_materials WHERE id='$id'";
			if (mysqli_num_rows($res) > 0) {
				while ($row = mysqli_fetch_assoc($res)) {

					$volume_m3_common = ($row['thickness']/1000 * $row['width']/1000 * $row['length']/1000)/1000000 ;
					$new_volume_m3 = $volume_m3_common * $quant;
					
					$time = time();
					mysqli_query($this->connection, "INSERT INTO `raw_materials`(`consignment`, `pack`, `name`, `quantity`, `breed`, `thickness`, `width`, `length`, `volume_m3`, `date_create`, `user_create`, `profile`) VALUES (
								'$id_new_consignments', 
								'{$row['pack']}', 
								'{$row['name']}', 
								'$quant', 
								'{$row['breed']}', 
								'{$row['thickness']}', 
								'{$row['width']}', 
								'{$row['length']}', 
								'$new_volume_m3', 
								'$time', 
								'{$_COOKIE['user_id']}', 
								'{$row['profile']}'
							)"
						);
					$id = mysqli_insert_id($this->connection);

					$old_quant = $row['quantity'] - $quant;
					$old_volume_m3 = $volume_m3_common * $old_quant;
					mysqli_query($this->connection, "UPDATE `raw_materials` SET `quantity`='$old_quant',`volume_m3`='$old_volume_m3' WHERE `id` = '{$row['id']}'");

				}
			}



		}

		return $queries;
	}

	public function GetColorById($id){
		$result = mysqli_query($this->connection, "SELECT * FROM `color_list` WHERE `id` = '$id' LIMIT 1");
		if (mysqli_num_rows($result) > 0) {
			$row = mysqli_fetch_array($result);
		}
		return $row['name'];
	}

	public function GetMasloById($id){
		$result = mysqli_query($this->connection, "SELECT * FROM `maslo_list` WHERE `id` = '$id' LIMIT 1");
		if (mysqli_num_rows($result) > 0) {
			$row = mysqli_fetch_array($result);
		}
		return $row['name'];	
	}

}