<?php

namespace StandartRus\App\Models;

use \StandartRus\App\Core\Controller;
use \StandartRus\App\Core\Model;

/**
 * Class Storage
 *
 * Модель складов.
 *
 * @author Матовников Александр matovnikovaleks@mail.ru
 * @package StandartRus\App\Models
 */
class Cars extends Model
{

	public function GetCars(){
		$res1 = mysqli_query($this->connection, "SELECT * FROM consignment WHERE postavka_markaam <> '' OR postavka_nomeram <> '' ");
		while($row = mysqli_fetch_assoc($res1)){
			$rows[$row['id']]['desc'] = $row;
			$res2 = mysqli_query($this->connection, "SELECT  rw.* , b.name as breed_name , c.name as color_name  FROM raw_materials rw LEFT JOIN breed_list b ON b.id=rw.breed LEFT JOIN color_list c ON c.id=rw.breed
				WHERE rw.consignment='{$row['id']}'");
			while($row2 = mysqli_fetch_assoc($res2)){
				$rows[$row['id']]['items'][] = $row2;
			}
		}
		return $rows;
	}


}