<?php

namespace StandartRus\App\Models;

use \StandartRus\App\Core\Controller;
use \StandartRus\App\Core\Model;

class Sales extends Model
{
	var $id;
	var $contract;
	var $fio;
	var $type;
	var $cost;
	var $prepaid;
	var $payment_type;
	var $status;
	var $data;

	public function CheckArchive(){
		$result = mysqli_query($this->connection, "SELECT `sales`.*, `status_list`.`name` as `status_name` FROM `sales` LEFT JOIN `status_list` ON `sales`.`status` = `status_list`.`id` WHERE `sales`.`id` > 0 AND `sales`.`status` = 6 ORDER BY `date_create` DESC");
		if (mysqli_num_rows($result) > 0) {
			return mysqli_num_rows($result);
		}
		return false;
	}
	public function GetSales($type = "",$page = "N", $limit = 10)
	{
		$limitQuery = "";
		if($page != "N"){
			$limitQuery = " LIMIT 0,".$limit;
			if($page > 1){
				$limitQuery = " LIMIT ".($limit*($page-1)).",".(($limit*($page-1))+$limit);
			}

		}
		if($type == "archive"){
			$result = mysqli_query($this->connection, "SELECT `sales`.*, `status_list`.`name` as `status_name` FROM `sales` LEFT JOIN `status_list` ON `sales`.`status` = `status_list`.`id` WHERE `sales`.`id` > 0 AND `sales`.`status` = 6 ORDER BY `date_create` DESC".$limitQuery);
		}
		else{
			$result = mysqli_query($this->connection, "SELECT `sales`.*, `status_list`.`name` as `status_name` FROM `sales` LEFT JOIN `status_list` ON `sales`.`status` = `status_list`.`id` WHERE `sales`.`id` > 0 AND `sales`.`status` <> 6 ORDER BY `date_create` DESC".$limitQuery);
		}
		$array = array();
		if (mysqli_num_rows($result) > 0) {
			while ($row = mysqli_fetch_assoc($result)) {
				$controller = new Controller();
				$row['date_create_str'] = $controller->FormattingDate($row['date_create'], true);
				$goods_ids="";
				$result_goods = mysqli_query($this->connection, "SELECT `sales_goods`.*, `raw_materials`.`name` as `name`, `raw_materials`.`thickness` as `thickness`, `raw_materials`.`width` as `width`, `raw_materials`.`length` as `length`, `raw_materials`.`volume_m3` as `volume_m3`, `consignment`.`id` AS `consignment_id`, `storage`.`id` AS `storage_id`, `storage`.`name` AS `storage_name` FROM `sales_goods` LEFT JOIN `raw_materials` ON `sales_goods`.`raw_material_id` = `raw_materials`.`id` LEFT JOIN `consignment` ON `consignment`.`id` = `raw_materials`.`consignment` LEFT JOIN `storage` ON `storage`.`id` = `consignment`.`storage` WHERE `sales_goods`.`id` > 0 && `sales_goods`.`sale_id` = '{$row['id']}'");
				$goods = array();
				if (mysqli_num_rows($result_goods) > 0) {
					while ($row_goods = mysqli_fetch_assoc($result_goods)) {
						$goods[] = $row_goods;
						$row['active_storage'] = $row_goods['storage_id'];
						$goods_ids .= $row_goods['raw_material_id'].",";
					}
					$goods_ids = trim($goods_ids,",");
					$row['all_goods'] = array_merge($this->GetAllGoods("`raw_materials`.`id` IN ({$goods_ids})"),$this->GetAllGoods());
					$row['goods'] = $goods;
				}
				$array[] = $row;
			}
			return $array;
		}
		return false;
	}

	public function GetStatusList()
	{
		$result = mysqli_query($this->connection, "SELECT * FROM `status_list`");
		$array = array();
		if (mysqli_num_rows($result) > 0) {
			while ($row = mysqli_fetch_assoc($result)) {
				$array[] = $row;
			}
			return $array;
		}
		return false;
	}
	/**
	 * Достает товары в нужном формате для сделок, со складами и т.п.
	 * @param string $condition - условие WHERE для запроса
	 * @return array|bool
	 */
	public function GetAllGoods( $condition = "`raw_materials`.`id` > 0 && `raw_materials`.`quantity` > 0")
	{
		$result = mysqli_query($this->connection, "SELECT `raw_materials`.*, `consignment`.`storage`, `storage`.`name` AS `storage_name`, `color_list`.`name` AS `color_name` FROM `raw_materials` LEFT JOIN `consignment` ON `raw_materials`.`consignment` = `consignment`.`id` LEFT JOIN `storage` ON `consignment`.`storage` = `storage`.`id` LEFT JOIN `color_list` ON `raw_materials`.`color` = `color_list`.`id` WHERE {$condition}");
		$array = array();
		if (mysqli_num_rows($result) > 0) {
			while ($row = mysqli_fetch_assoc($result)) {
				$array["good_".$row['id']] = $row;
			}
			return $array;
		}
		return false;
	}
	public function Add()
	{
		$time = time();
		mysqli_query($this->connection, "INSERT INTO `sales`(`type`, `contract`, `fio`, `cost`, `prepaid`, `payment_type`, `status`, `date_create`, `user_create`) VALUES ('{$this->type}', '{$this->contract}', '{$this->fio}', '{$this->cost}', '{$this->prepaid}', '{$this->payment_type}','{$this->status}', '$time', '{$_COOKIE['user_id']}')");
		$id_sale = mysqli_insert_id($this->connection);
		$this->AddMaterials($id_sale);
		return true;
	}

	public function Update()
	{
		mysqli_query($this->connection, "UPDATE `sales` SET `type` = '{$this->type}', `contract` = '{$this->contract}', `fio` = '{$this->fio}', `cost` = '{$this->cost}', `prepaid` = '{$this->prepaid}', `payment_type` = '{$this->payment_type}', `status` = '{$this->status}' WHERE `id` = {$this->id}");
		$this->ReturnMaterials($this->id);
		$this->AddMaterials($this->id);
		return true;
	}

	function Delete(){
		mysqli_query($this->connection, "DELETE FROM `sales` WHERE id = '{$this->id}'");
		$this->ReturnMaterials($this->id);
		return true;
	}

	public function AddMaterials($id_sale){
		$res = false;
		foreach ($this->data as $v) {
			if (!empty($v[0]) && !empty($v[1])) {
				$result = mysqli_query($this->connection, "SELECT * FROM `raw_materials` WHERE `id` = '{$v[0]}' LIMIT 1");
				if (mysqli_num_rows($result) > 0) {
					$row = mysqli_fetch_array($result);
					if ($v[1] <= $row['quantity']) {
						mysqli_query($this->connection, "INSERT INTO `sales_goods`(`sale_id`, `raw_material_id`, `quantity`) VALUES ('$id_sale', '{$v[0]}', '{$v[1]}')");
						if ($row['volume_m3'] > 0) {
							$new_quantity = $row['quantity'] - $v[1];
							$new_volume = round(($row['volume_m3'] / $row['quantity']) * $new_quantity);
							mysqli_query($this->connection, "UPDATE `raw_materials` SET `quantity`=`quantity`-{$v[1]}, `volume_m3`='$new_volume' WHERE `id` = {$row['id']}");
							$res = true;
						} else {
							mysqli_query($this->connection, "UPDATE `raw_materials` SET `quantity`=`quantity`-{$v[1]} WHERE `id` = '{$row['id']}'");
							$res = true;
						}
					}
				}
			}
		}
		return $result;
	}

	public function ReturnMaterials($id_sale)
	{
		$res = false;
		$result = mysqli_query($this->connection, "SELECT * FROM `sales_goods` WHERE `sale_id` = '{$id_sale}'");
		if (mysqli_num_rows($result) > 0) {
			while ($row = mysqli_fetch_assoc($result)) {
				$result_good = mysqli_query($this->connection, "SELECT * FROM `raw_materials` WHERE `id` = '{$row['raw_material_id']}' LIMIT 1");
				if (mysqli_num_rows($result_good) > 0) {
					while ($row_good = mysqli_fetch_assoc($result_good)) {
						if ($row_good['volume_m3'] > 0) {
							$new_quantity = $row_good['quantity'] + $row['quantity'];
							$new_volume = round(($row_good['volume_m3'] / $row_good['quantity']) * $new_quantity);
							mysqli_query($this->connection, "UPDATE `raw_materials` SET `quantity`=`quantity`+{$row['quantity']}, `volume_m3`='$new_volume' WHERE `id` = {$row_good['id']}");
							mysqli_query($this->connection, "DELETE FROM `sales_goods` WHERE id = '{$row['id']}'");
							$res = true;
						} else {
							mysqli_query($this->connection, "UPDATE `raw_materials` SET `quantity`=`quantity`+{$row['quantity']} WHERE `id` = '{$row_good['id']}'");
							mysqli_query($this->connection, "DELETE FROM `sales_goods` WHERE id = '{$row['id']}'");
							$res = true;
						}
					}
				}
			}
		}
		return $res;
	}

	public function UpdateStatusSale()
	{
		return mysqli_query($this->connection, "UPDATE `sales` SET `status`={$this->status} WHERE `id` = {$this->id}");
	}
}