<?php

namespace StandartRus\App\Models;

use \StandartRus\App\Core\Controller;
use \StandartRus\App\Core\Model;

/**
 * Class Storage
 *
 *
 */
class Deals extends Model
{
	public function addDeal($data){
		// extract($data);
		$year = date('Y');
		$str_fields = $str_fields_val = '';
		foreach ($data as $key => $value) {
			if($key == 'action' || $key == 'id')continue;
			$str_fields .= "$key ,";
			$str_fields_val .= "'$value',";
		}
		$str_fields .= "year ,";
		$str_fields_val .= "'$year',";
		$str_fields = rtrim($str_fields, ',');
		$str_fields_val = rtrim($str_fields_val, ',');
		mysqli_query($this->connection, "INSERT INTO deals(
			$str_fields
		) 
		VALUES(
			$str_fields_val
		)");
		$parent_id = $data['parent_id'];
		if($parent_id == 0){
			$parent_id = mysqli_insert_id($this->connection);
			mysqli_query($this->connection, "UPDATE deals SET parent_id = '$parent_id' WHERE id='$parent_id'");
		}

	}

	public function editDeal($data){
		// extract($data);
		$parent_id = isset($parent_id)&&$parent_id!==0?$parent_id:$id;
		$str_fields = '';
		foreach ($data as $key => $value) {
			if($key == 'action' || $key == 'id')continue;
			$str_fields .= "$key = '$value',";
		}
		$str_fields = rtrim($str_fields, ',');
		// file_put_contents(__DIR__.'/filename.log', var_export($str_fields, true));
		mysqli_query($this->connection, "UPDATE deals SET
			$str_fields
			WHERE id='{$data['id']}'
		");
	}

	public function getDeals(){
		$res1 = mysqli_query($this->connection, 
		"SELECT d.*, ct.name as contract_type, m.name as manufacturer, car.name as  сarrier, ad.name as ad_source, p.name as pay_type, concat(u.surname, ' ', u.name) as manager
		FROM deals d 
		LEFT JOIN contract_type_list ct ON ct.id=d.contract_type 
		LEFT JOIN manufacturer_list m ON m.id=d.manufacturer 
		LEFT JOIN сarrier_list car ON car.id=d.сarrier  
		LEFT JOIN ad_source_list ad ON ad.id=d.ad_source  
		LEFT JOIN pay_type_list p ON p.id=d.pay_type  
		LEFT JOIN users u ON u.id=d.manager  
		ORDER BY parent_id"
	);
		if($res1 == false) return;
		$rows = mysqli_fetch_all($res1, MYSQLI_ASSOC);
		return $rows;
	}

	public function getGuides(){
		$tbls = [
				'breed',
				'profile',
				'color',
				'maslo',
				'cort',
				'contract_type',
				'manufacturer',
				'pay_type',
				'сarrier',
				'ad_source',
				];
				$data = [];
				foreach ($tbls as $key => $tbl) {
					# code...
					$res1 = mysqli_query($this->connection, "SELECT * FROM {$tbl}_list");
					if($res1 != false)
						$data[$tbl] = mysqli_fetch_all($res1, MYSQLI_ASSOC);
				}
		return $data;
	}


}