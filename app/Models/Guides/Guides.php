<?php

namespace StandartRus\App\Models;

use \StandartRus\App\Core\Controller;
use \StandartRus\App\Core\Model;

/**
 * Class Storage
 *
 * Модель складов.
 *
 */
class Guides extends Model
{

	public function getGuides(){
		$tbls = [
				'breed',
				'profile',
				'color',
				'maslo',
				'cort',
				'contract_type',
				'manufacturer',
				'pay_type',
				'сarrier',
				'ad_source',
				];
				$data = [];
				foreach ($tbls as $key => $tbl) {
					# code...
					$res1 = mysqli_query($this->connection, "SELECT * FROM {$tbl}_list");
					if($res1 != false)
						$data[$tbl] = mysqli_fetch_all($res1, MYSQLI_ASSOC);
				}
		return $data;
	}


	public function DeleteRawGuide($id, $name){
		mysqli_query($this->connection, "DELETE FROM {$name}_list WHERE id='$id'");
		// return "DELETE FROM $name_list WHERE id='$id'";
	}
	
	public function EditRawGuide($id, $name, $new_val){
		mysqli_query($this->connection, "UPDATE {$name}_list SET name='$new_val' WHERE id='$id'");
		//return "UPDATE {$name}_list SET name='$new_val' WHERE id='$id'";
	}

	public function addGuide($table, $name){
		mysqli_query($this->connection, "INSERT INTO {$table}_list(name) VALUES('{$name}')");
	}


}