<?php
/**
 * Пользователи
 */

namespace StandartRus\App\Models;

use \StandartRus\App\Core\Controller;
use \StandartRus\App\Core\Model;

class Users extends Model
{
	var $id;
	var $email;
	var $phone;
	var $password;
	var $role;
	var $photo;
	var $name;
	var $surname;
	var $patronymic;

	static function getUsers(){
		$result = mysqli_query(Model::DB(), "SELECT * FROM `users`");
		if (mysqli_num_rows($result) > 0) {
			$array = array();
			while ($row = mysqli_fetch_assoc($result)) {
				$array[] = $row;
			}
			return $array;
		} else {
			return false;
		}
	}
	
	static function getUsersByType($type_id){
		$result = mysqli_query(Model::DB(), "SELECT * FROM `users` WHERE `type` = '{$type_id}'");
		if (mysqli_num_rows($result) > 0) {
			$array = array();
			while ($row = mysqli_fetch_assoc($result)) {
				if($row['photo'] == "/" || $row['photo'] == "")
					$row['photo'] = PHOTO_EMPTY;
				$array[] = $row;
			}
			return $array;
		} else {
			return false;
		}
	}

	static function getRoles(){
		$result = mysqli_query(Model::DB(), "SELECT * FROM `role_list` WHERE `id` > '0'");
		if (mysqli_num_rows($result) > 0) {
			$array = array();
			while ($row = mysqli_fetch_assoc($result)) {
				$array[] = $row;
			}
			return $array;
		} else {
			return false;
		}
	}

	public function Add()
	{
		$time = time();
		$this->role = intval($this->role);
		mysqli_query($this->connection, "INSERT INTO `users`(`name`, `surname`, `patronymic`, `email`, `phone`,`photo`, `password`, `type`, `date_create`) VALUES ('{$this->name}', '{$this->surname}', '{$this->patronymic}', '{$this->email}', '{$this->phone}','{$this->photo}', '{$this->password}','{$this->role}', '$time')");
		$id_user = mysqli_insert_id($this->connection);
		Model::AddLog('Добавил пользователя № ' . $id_user);
		return true;
	}

	public function Update()
	{
		if(strlen($this->photo) > 1)
			$adv.=",`photo` = '{$this->photo}'";
		if(strlen($this->password) > 0)
			$adv.=",`password` = '{$this->password}'";
		mysqli_query($this->connection, "UPDATE `users` SET `name` = '{$this->name}',`surname` = '{$this->surname}',`patronymic` = '{$this->patronymic}',`email` = '{$this->email}',`phone` = '{$this->phone}'".$adv.",`type` = '{$this->role}' WHERE `id` = {$this->id}");
		Model::AddLog('Изменил пользователя № ' . $this->id);
		return true;
	}

	function Delete(){
		mysqli_query($this->connection, "DELETE FROM `users` WHERE id = '{$this->id}'");
		Model::AddLog('Удалил пользователя № ' . $this->id);
		return true;
	}
}