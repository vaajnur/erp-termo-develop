<?php

namespace StandartRus\App\Models;

use \StandartRus\App\Core\Controller;
use \StandartRus\App\Core\Model;

class Logs extends Model
{

	public function GetListLogs()
	{
		$result = mysqli_query($this->connection, "SELECT `logs`.*, `users`.`name` AS `user_name`, `users`.`surname` AS `user_surname` FROM `logs` LEFT JOIN `users` ON `logs`.`user_create` = `users`.`id` WHERE `logs`.`id` > 0 ORDER BY `logs`.`date_create` DESC");
		$array = array();
		if (mysqli_num_rows($result) > 0) {
			while ($row = mysqli_fetch_assoc($result)) {
				$controller = new Controller();
				$row['date_create_str'] = $controller->FormattingDate($row['date_create'], true);

				$array[] = $row;
			}
			return $array;
		}
		return false;
	}

}