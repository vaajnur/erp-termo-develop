<?php

namespace StandartRus\App\Core;

class Auth
{

	static function Check()
	{
		$routes = explode('/', $_SERVER['REQUEST_URI']);

		if (isset($_COOKIE['user_id']) && !empty($_COOKIE['user_id'])) {
			$user_id = $_COOKIE['user_id'];
			$user_hash = md5($_COOKIE['user_hash']);

			$ip_user = $_SERVER["REMOTE_ADDR"];

			$user_hash_db = Model::GetUserHash();
			$user_ip_db = Model::GetUserIp();

			if ((empty($user_id) || empty($user_hash)) || $user_hash_db != $user_hash || $user_ip_db != $ip_user) {
				/*setcookie("user_id", "", time() - 3600, "/");
				setcookie("user_hash", "", time() - 3600, "/");
				header("Location:/");*/
			} else {
				if (empty($routes[1]) || ($routes[1] == 'login' && $routes[2] != 'logout')) {
					header("Location: /storage/");
				} else {
					return true;
				}
			}
		} else {
			if (!empty($routes[1]) && $_SERVER['REQUEST_METHOD'] === 'GET') {
				header("Location: /");
			}
		}
	}

	static function Exists()
	{
		if (isset($_COOKIE['user_id']) && !empty($_COOKIE['user_id'])) {
			$user_id = $_COOKIE['user_id'];
			$user_hash = md5($_COOKIE['user_hash']);

			$ip_user = $_SERVER["REMOTE_ADDR"];

			$user_hash_db = Model::GetUserHash();
			$user_ip_db = Model::GetUserIp();

			if ($user_hash_db == $user_hash && $user_ip_db == $ip_user) return true;
		}
		return false;
	}

}