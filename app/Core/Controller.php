<?php

namespace StandartRus\App\Core;

use \PHPMailer\PHPMailer\PHPMailer;
use \PHPMailer\PHPMailer\Exception;
use \SMSRU\SMSRU\SMSRU;

/**
 * Контроллер.
 *
 * @author Матовников Александр matovnikovaleks@mail.ru
 * @package StandartRus\App\Core
 */
class Controller
{

	/**
	 * @var Model
	 */
	public $model;
	/**
	 * @var View
	 */
	public $view;
	/**
	 * @var
	 */
	protected $mess;

	/**
	 * Controller constructor.
	 */
	function __construct()
	{
		global $mess;
		$this->model = new Model();
		$this->view = new View();
		$this->mess = $mess;
	}

	/**
	 * @return mixed|string
	 */
	public function PasswordGeneration()
	{
		return $this->RandStringGeneration(10);
	}

	/**
	 * @return mixed|string
	 */
	public function KeyGeneration()
	{
		return $this->RandStringGeneration(30);
	}

	/**
	 * @return mixed|string
	 */
	public function HashGeneration()
	{
		return $this->RandStringGeneration(50);
	}

	/**
	 * @param $length
	 * @return mixed|string
	 */
	private function RandStringGeneration($length)
	{
		$chartypes = "lower,upper,numbers";
		$chartypes_array = explode(",", $chartypes);
		$lower = 'abcdefghijklmnopqrstuvwxyz';
		$upper = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$numbers = '1234567890';
		$special = '^@*+-+%()!?';
		$chars = "";
		if (in_array('all', $chartypes_array)) {
			$chars = $lower . $upper . $numbers . $special;
		} else {
			if (in_array('lower', $chartypes_array)) $chars = $lower;
			if (in_array('upper', $chartypes_array)) $chars .= $upper;
			if (in_array('numbers', $chartypes_array)) $chars .= $numbers;
			if (in_array('special', $chartypes_array)) $chars .= $special;
		}
		$chars_length = strlen($chars) - 1;
		$string = $chars{rand(0, $chars_length)};
		for ($i = 1; $i < $length; $i = strlen($string)) {
			$random = $chars{rand(0, $chars_length)};
			if ($random != $string{$i - 1}) $string .= $random;
		}
		return $string;
	}

	/**
	 * @param $date
	 * @param bool $time
	 * @return string
	 */
	public function FormattingDate($date, $time = false)
	{
		$month_string = array(
			"01" => $this->mess['JANUARY'],
			"02" => $this->mess['FEBRUARY'],
			"03" => $this->mess['MARCH'],
			"04" => $this->mess['APRIL'],
			"05" => $this->mess['MAY'],
			"06" => $this->mess['JUNE'],
			"07" => $this->mess['JULY'],
			"08" => $this->mess['AUGUST'],
			"09" => $this->mess['SEPTEMBER'],
			"10" => $this->mess['OCTOBER'],
			"11" => $this->mess['NOVEMBER'],
			"12" => $this->mess['DECEMBER']
		);

		$day = date("j", $date);
		$month = $month_string[date("m", $date)];
		$year = date("Y", $date);
		$hour = date("H", $date);
		$minute = date("i", $date);

		if ($time === false) return $day . " " . $month . " " . $year . " г.";
		else return $day . " " . $month . " " . $year . " г. в " . $hour . ":" . $minute;
	}

	/**
	 * @param $month
	 * @return mixed
	 */
	static function GetNameMonth($month)
	{
		$controller = new Controller();
		$month_string = array(
			"1" => $controller->mess['JANUARY_NOM'],
			"2" => $controller->mess['FEBRUARY_NOM'],
			"3" => $controller->mess['MARCH_NOM'],
			"4" => $controller->mess['APRIL_NOM'],
			"5" => $controller->mess['MAY_NOM'],
			"6" => $controller->mess['JUNE_NOM'],
			"7" => $controller->mess['JULY_NOM'],
			"8" => $controller->mess['AUGUST_NOM'],
			"9" => $controller->mess['SEPTEMBER_NOM'],
			"10" => $controller->mess['OCTOBER_NOM'],
			"11" => $controller->mess['NOVEMBER_NOM'],
			"12" => $controller->mess['DECEMBER_NOM']
		);

		return $month_string[$month];
	}

	/**
	 * @param $price
	 * @return string
	 */
	public function StringPrice($price)
	{
		$nul = 'ноль';
		$ten = array(
			array('', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
			array('', 'одна', 'две', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
		);
		$a20 = array('десять', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать');
		$tens = array(2 => 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто');
		$hundred = array('', 'сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот');
		$unit = array(
			array('копейка', 'копейки', 'копеек', 1),
			array('рубль', 'рубля', 'рублей', 0),
			array('тысяча', 'тысячи', 'тысяч', 1),
			array('миллион', 'миллиона', 'миллионов', 0),
			array('миллиард', 'милиарда', 'миллиардов', 0),
		);

		list($rub, $kop) = explode('.', sprintf("%015.2f", floatval($price)));
		$out = array();
		if (intval($rub) > 0) {
			foreach (str_split($rub, 3) as $uk => $v) {
				if (!intval($v)) continue;
				$uk = sizeof($unit) - $uk - 1; // unit key
				$gender = $unit[$uk][3];
				list($i1, $i2, $i3) = array_map('intval', str_split($v, 1));
				$out[] = $hundred[$i1]; # 1xx-9xx
				if ($i2 > 1) $out[] = $tens[$i2] . ' ' . $ten[$gender][$i3]; # 20-99
				else $out[] = $i2 > 0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
				if ($uk > 1) $out[] = Controller::Morph($v, $unit[$uk][0], $unit[$uk][1], $unit[$uk][2]);
			}
		} else {
			$out[] = $nul;
		}

		$out[] = Controller::Morph(intval($rub), $unit[1][0], $unit[1][1], $unit[1][2]); // rub
		$out[] = $kop . ' ' . Controller::Morph($kop, $unit[0][0], $unit[0][1], $unit[0][2]); // kop

		return trim(preg_replace('/ {2,}/', ' ', join(' ', $out)));
	}

	/**
	 * @param $n
	 * @param $f1
	 * @param $f2
	 * @param $f5
	 * @return mixed
	 */
	private function Morph($n, $f1, $f2, $f5)
	{
		$n = abs(intval($n)) % 100;
		if ($n > 10 && $n < 20) return $f5;
		$n = $n % 10;
		if ($n > 1 && $n < 5) return $f2;
		if ($n == 1) return $f1;
		return $f5;
	}

	/**
	 * @param $price
	 * @return bool
	 */
	static function IsPrice($price)
	{
		if (empty($price) || is_numeric($price) === false) return false;
		else return true;
	}

	/**
	 * Переадресация на 404 страницу
	 */
	static function ErrorPage404()
	{
		header('HTTP/1.0 404 Not Found');
		header('Location: http://' . $_SERVER['HTTP_HOST'] . '/404/');
		exit;
	}

	/**
	 * Переадресация (301 редирект)
	 *
	 * @param $page
	 */
	static function Location301($page)
	{
		header('HTTP/1.1 301 Moved Permanently');
		header('Location: http://' . $_SERVER['HTTP_HOST'] . '/' . $page . '/');
		exit;
	}

	/**
	 * Отправка E-Mail сообщения
	 *
	 * @param $message
	 * @param $subject
	 * @param string $to
	 * @return bool
	 * @throws Exception
	 */
	static function SendEmail($message, $subject, $to = BID_EMAIL)
	{
		$mail = new PHPMailer(true);

		$mail->isSMTP();
		$mail->Host = SMTP_HOST;
		$mail->SMTPAuth = true;
		$mail->Username = MAIL_LOGIN;
		$mail->Password = MAIL_PASSWORD;
		$mail->SMTPSecure = SMTP_SECURE;
		$mail->Port = SMTP_PORT;

		$mail->SetFrom(MAIL_LOGIN, MAIL_USERNAME);
		$mail->addAddress($to);

		$mail->isHTML(true);
		$mail->Subject = $subject;
		$mail->Body = $message;

		$mail->CharSet = 'UTF-8';

		$mail->send();

		return true;
	}

	/**
	 * Отправка SMS сообщения
	 *
	 * @param $message
	 * @param $to
	 * @return bool
	 */
	static function SendSms($message, $to)
	{
		$smsru = new SMSRU(SMS_API_KEY);
		$sms = $smsru->send_one((object)array('to' => $to, 'text' => $message, 'from' => SMS_FROM));

		if ($sms->status == "OK") return true;
		else return false;
	}

	/**
	 * Пагинация
	 *
	 * @param $page
	 * @param $count
	 * @param $count_on_page
	 * @param $url
	 * @return array
	 */
	static function Pagination($page, $count, $count_on_page, $url)
	{
		$pagination = array();
		$count_page = ceil($count / $count_on_page);

		$count = 0;
		if ($page > 1) {
			$page_info = array();
			$page_info['id'] = $page - 1;
			$page_info['url'] = $url;
			$page_info['arrow_prev'] = true;
			$pagination[] = $page_info;
		}

		if ($count_page - $page < 3) {
			$count_prev_page = 3 - ($count_page - $page);
			for ($i = $count_prev_page; $i > 0; $i--) {
				if ($page - $i > 0) {
					$page_info = array();
					$page_info['id'] = $page - $i;
					$page_info['url'] = $url;
					$page_info['active'] = false;
					$pagination[] = $page_info;
				}
			}
		}

		for ($i = $page; $i <= $count_page; $i++) {
			$page_info = array();
			$page_info['id'] = $i;
			$page_info['url'] = $url;
			if ($page == $i) $page_info['active'] = true;
			else $page_info['active'] = false;
			$pagination[] = $page_info;

			if ($count_page - $page > 3) {
				$count++;
				if ($count == 2) {
					$page_info = array();
					$page_info['id'] = 0;
					$page_info['url'] = $url;
					$page_info['space'] = true;
					$pagination[] = $page_info;

					$page_info = array();
					$page_info['id'] = $count_page;
					$page_info['url'] = $url;
					$page_info['active'] = false;
					$pagination[] = $page_info;
					break;
				}
			}
		}
		if ($count_page > 1 && $page != $count_page) {
			$page_info = array();
			$page_info['id'] = $page + 1;
			$page_info['url'] = $url;
			$page_info['arrow_next'] = true;
			$pagination[] = $page_info;
		}

		return $pagination;
	}

	/**
	 * Генерация URL из строки
	 *
	 * @param $str
	 * @return string|string[]|null
	 */
	static function str2url($str)
	{
		$str = Controller::rus2translit($str);
		$str = strtolower($str);
		$str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
		$str = trim($str, "-");
		return $str;
	}

	/**
	 * Транслит
	 *
	 * @param $string
	 * @return string
	 */
	private static function rus2translit($string)
	{
		$converter = array(
			'а' => 'a', 'б' => 'b', 'в' => 'v',
			'г' => 'g', 'д' => 'd', 'е' => 'e',
			'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
			'и' => 'i', 'й' => 'y', 'к' => 'k',
			'л' => 'l', 'м' => 'm', 'н' => 'n',
			'о' => 'o', 'п' => 'p', 'р' => 'r',
			'с' => 's', 'т' => 't', 'у' => 'u',
			'ф' => 'f', 'х' => 'h', 'ц' => 'c',
			'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch',
			'ь' => '\'', 'ы' => 'y', 'ъ' => '\'',
			'э' => 'e', 'ю' => 'yu', 'я' => 'ya',

			'А' => 'A', 'Б' => 'B', 'В' => 'V',
			'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
			'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z',
			'И' => 'I', 'Й' => 'Y', 'К' => 'K',
			'Л' => 'L', 'М' => 'M', 'Н' => 'N',
			'О' => 'O', 'П' => 'P', 'Р' => 'R',
			'С' => 'S', 'Т' => 'T', 'У' => 'U',
			'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
			'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch',
			'Ь' => '\'', 'Ы' => 'Y', 'Ъ' => '\'',
			'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',
		);
		return strtr($string, $converter);
	}

	/**
	 * Подгрузка файла модели
	 *
	 * @param $path
	 * @param $name
	 */
	static function LoadModel($path, $name)
	{
		include_once 'app/Models/' . $path . '/' . $name . '.php';
	}
	/**
	 * Загрузка файла
	 *
	 * @param $image
	 * @param $path
	 * @param $valid_extensions
	 */
	static function UploadFile($image, $path = "", $valid_extensions = array('jpeg', 'jpg', 'png', 'gif', 'bmp', 'pdf', 'doc', 'ppt'))
	{
		if($path == "")
			$path = $_SERVER['DOCUMENT_ROOT'].'/assets/images/';
		if ($image) {
			$img = $image['name'];
			$tmp = $image['tmp_name'];
			$ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
			$final_image = rand(1000, 1000000) . $img;
			if (in_array($ext, $valid_extensions)) {
				$path = $path . strtolower($final_image);
				if (move_uploaded_file($tmp, $path)) {
					return $path;
				}
			}
		}
		return "/";
	}

	/**
	 * Поворот изображения на необходимый угол при нарушенной ориентации
	 *
	 * @param $file
	 * @return bool
	 */
	static function img_orientation($file)
	{
		if (!file_exists($file))
			return false;

		$size = getimagesize($file);

		if ($size === false)
			return false;

		$format = strtolower(substr($size['mime'], strpos($size['mime'], '/') + 1));
		$icfunc = 'image' . $format;

		if (!function_exists($icfunc))
			return false;

		$image = imagecreatefromstring(file_get_contents($file));
		//Поворот изображения
		$exif = exif_read_data($file);
		$angles = array(8 => 90, 3 => 180, 6 => -90);
		if (!empty($exif['Orientation']) && isset($angles[$exif['Orientation']])) {
			$image = imagerotate($image, $angles[$exif['Orientation']], 0);
		}

		$icfunc($image, $file);
		return true;
	}

	/**
	 * Ресайз изображений
	 *
	 * @param $file
	 * @param $dest
	 * @param $max_width
	 * @param $max_height
	 * @param int $quality
	 * @return bool|string
	 */
	static function img_resize($file, $dest, $max_width, $max_height, $quality = 100)
	{
		if (!file_exists($file))
			return false;

		$size = getimagesize($file);

		if ($size === false)
			return false;

		$format = strtolower(substr($size['mime'], strpos($size['mime'], '/') + 1));
		$icfunc = 'imagecreatefrom' . $format;

		if (!function_exists($icfunc))
			return false;

		$isrc = $icfunc($file);
		$w_src = imagesx($isrc);
		$h_src = imagesy($isrc);

		$w = $max_width;
		$h = $max_height;

		if ($w_src > $w || $h_src > $h) {
			if ($w_src > $h_src) $ratio = $w / $w_src;
			else $ratio = $h / $h_src;
			$w_dest = intval($ratio * $w_src);
			$h_dest = intval($ratio * $h_src);
			if ($w_dest > $w) {
				$ratio = $w / $w_src;
				$w_dest = intval($ratio * $w_src);
				$h_dest = intval($ratio * $h_src);
			}
			if ($h_dest > $h) {
				$ratio = $h / $h_src;
				$w_dest = intval($ratio * $w_src);
				$h_dest = intval($ratio * $h_src);
			}
			$idest = imagecreatetruecolor($w_dest, $h_dest);
			imagecopyresampled($idest, $isrc, 0, 0, 0, 0, $w_dest, $h_dest, $w_src, $h_src);
		}

		$i = strrpos($dest, '.');
		if (!$i) return '';
		$l = strlen($dest) - $i;
		$ext = substr($dest, $i + 1, $l);

		switch ($ext) {
			case 'jpeg':
			case 'jpg':
				imagejpeg($idest, $dest, $quality);
				break;
			case 'gif':
				imagegif($idest, $dest);
				break;
			case 'png':
				imagepng($idest, $dest);
				break;
		}

		imagedestroy($isrc);
		imagedestroy($idest);

		return true;
	}

	/**
	 * Криптографическое шифрование файла
	 *
	 * @param $data
	 * @param $key
	 * @return string
	 */
	static function FileEncrypt($data, $key)
	{
		//Remove the base64 encoding from our key
		$encryption_key = base64_decode($key);
		//Generate an initialization vector
		$iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
		//Encrypt the data using AES 256 encryption in CBC mode using our encryption key and initialization vector.
		$encrypted = openssl_encrypt($data, 'aes-256-cbc', $encryption_key, 0, $iv);
		//The $iv is just as important as the key for decrypting, so save it with our encrypted data using a unique separator (::)
		return base64_encode($encrypted . '::' . $iv);
	}

	/**
	 * Криптографическая расшифровка файла
	 *
	 * @param $data
	 * @param $key
	 * @return string
	 */
	static function FileDecrypt($data, $key)
	{
		//Remove the base64 encoding from our key
		$encryption_key = base64_decode($key);
		//To decrypt, split the encrypted data from our IV - our unique separator used was "::"
		list($encrypted_data, $iv) = explode('::', base64_decode($data), 2);
		return openssl_decrypt($encrypted_data, 'aes-256-cbc', $encryption_key, 0, $iv);
	}

	static function RusEnding($n, $n1, $n2, $n5)
	{
		if ($n >= 11 and $n <= 19) return $n5;
		$n = $n % 10;
		if ($n == 1) return $n1;
		if ($n >= 2 and $n <= 4) return $n2;
		return $n5;
	}

}