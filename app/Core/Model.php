<?php

namespace StandartRus\App\Core;

/**
 * Class Model
 *
 * @author Матовников Александр matovnikovaleks@mail.ru
 * @package StandartRus\App\Core
 */
class Model
{

	/**
	 * Филиал, к которому прикреплен пользователь
	 *
	 * @var int|bool
	 */
	public $office;
	/**
	 * Группа доступа пользователя
	 *
	 * @var int|bool
	 */
	public $user_type;
	/**
	 * @var false|\mysqli
	 */
	protected $connection;

	/**
	 * Model constructor.
	 */
	function __construct()
	{
		$this->connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_TABLE);

		mysqli_query($this->connection, "SET NAMES 'utf8';");

		if (!$this->connection) exit(mysqli_error($this->connection));

		$this->user_type = Model::GetUserType();
	}

	/**
	 * Подключение к БД
	 *
	 * @return false|\mysqli
	 */
	public static function DB()
	{
		$connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_TABLE);

		mysqli_query($connection, "SET NAMES 'utf8';");

		if (!$connection) exit(mysqli_error($connection));
		return $connection;
	}

	/**
	 * Hash пользователя
	 *
	 * Используется для проверки корректности авторизации
	 *
	 * @return string|int
	 */
	static function GetUserHash()
	{
		if (isset($_COOKIE['user_id'])) {
			$result = mysqli_query(Model::DB(), "SELECT `hash` FROM `users` WHERE `id` = '{$_COOKIE['user_id']}'");
			$row = mysqli_fetch_array($result);

			if ($result != false && mysqli_num_rows($result) > 0) return $row['hash'];
			else return 0;
		}
		return 0;
	}

	/**
	 * IP пользователя
	 *
	 * Используется для проверки корректности авторизации
	 *
	 * @return string|int
	 */
	static function GetUserIp()
	{
		if (isset($_COOKIE['user_id'])) {
			$result = mysqli_query(Model::DB(), "SELECT `ip` FROM `users` WHERE `id` = '{$_COOKIE['user_id']}'");
			$row = mysqli_fetch_array($result);

			if ($result != false && mysqli_num_rows($result) > 0) return $row['ip'];
			else return 0;
		}
		return 0;
	}

	/**
	 * Тип пользователя
	 *
	 * Используется для разграничения прав доступа для разных пользователей
	 *
	 * @return int|bool
	 */
	static function GetUserType()
	{
		if (isset($_COOKIE['user_id'])) {
			$result = mysqli_query(Model::DB(), "SELECT `type` FROM `users` WHERE `id` = '{$_COOKIE['user_id']}'");
			$row = mysqli_fetch_array($result);

			if ($result != false && mysqli_num_rows($result) > 0) return $row['type'];
			else return false;
		}
		return false;
	}

	/**
	 * Список используемых в системе единиц измерения
	 *
	 * Возвращает массив: id, name
	 *
	 * @return array|bool
	 */
	/*static function GetListUnit()
	{
		$result = mysqli_query(Model::DB(), "SELECT * FROM `unit` WHERE `id` > '0'");
		if ($result != false && mysqli_num_rows($result) > 0) {
			$array = array();
			while ($row = mysqli_fetch_array($result)) {
				$unit = array();
				$unit['id'] = $row['id'];
				$unit['name'] = $row['name'];
				$array[] = $unit;
			}
			return $array;

		} else {
			return false;
		}
	}*/

	static function GetListBreed()
	{
		$result = mysqli_query(Model::DB(), "SELECT * FROM `breed_list` WHERE `id` > '0'");
		if ($result != false && mysqli_num_rows($result) > 0) {
			$array = array();
			while ($row = mysqli_fetch_assoc($result)) {
				$array[] = $row;
			}
			return $array;
		} else {
			return false;
		}
	}

	/**
	 * Список имен для автозаполнения
	 *
	 *
	 *
	 * @return int|bool
	 */
	static function GetListNames()
	{
		$result = mysqli_query(Model::DB(), "SELECT name FROM `raw_materials` WHERE `id` > '0'");
		if ($result != false && mysqli_num_rows($result) > 0) {
			$array = array();
			while ($row = mysqli_fetch_assoc($result)) {
				$array[] = $row['name'];
			}
			return array_unique($array);
		} else {
			return false;
		}
	}

	/**
	 * [GetListNames description]
	 */
	static function GetListCorts()
	{
		$result = mysqli_query(Model::DB(), "SELECT * FROM `cort_list` WHERE `id` > '0'");
		if ($result != false && mysqli_num_rows($result) > 0) {
			$array = array();
			while ($row = mysqli_fetch_assoc($result)) {
				$array[] = $row;
			}
			return $array;
		} else {
			return false;
		}
	}
	
	/**
	 * Список профилей
	 *
	 *
	 *
	 * @return int|bool
	 */
	static function GetProfiles()
	{
		$result = mysqli_query(Model::DB(), "SELECT name FROM `profile_list` WHERE `id` > '0'");
		if ($result != false && mysqli_num_rows($result) > 0) {
			$array = array();
			while ($row = mysqli_fetch_assoc($result)) {
				$array[] = $row['name'];
			}
			return array_unique($array);
		} else {
			return false;
		}
	}


	static function GetProfilesAll($breed = false)
	{
		// для продцукции 
		$result = mysqli_query(Model::DB(), "SELECT DISTINCT(raw.profile) as profile FROM raw_materials raw  LEFT JOIN  consignment cons  ON raw.consignment = cons.id  WHERE cons.storage = 3" . ($breed != false && $breed != 'all' ? " and  raw.breed = '$breed'" : "") );
		if ($result != false && mysqli_num_rows($result) > 0) {
			$array = array();
			while ($row = mysqli_fetch_assoc($result)) {
				$array[] = $row['profile'];
			}
			return array_unique($array);
		} else {
			return false;
		}
	}
	
	static function GetListTint($id = 0)
	{
		$breed = 1;

		if ($id == 0) {
			if (!empty($_GET['breed'])) $breed = $_GET['breed'];
		} else {
			$breed = $id;
		}

		$result = mysqli_query(Model::DB(), "SELECT * FROM `tint_list` WHERE `id` > '0' && `breed` = '$breed'");
		if ($result != false && mysqli_num_rows($result) > 0) {
			$array = array();
			while ($row = mysqli_fetch_assoc($result)) {
				$array[] = $row;
			}
			return $array;
		} else {
			return false;
		}
	}

	static function GetListColor()
	{
		$result = mysqli_query(Model::DB(), "SELECT * FROM `color_list` WHERE `id` > '0'");
		if ($result != false && mysqli_num_rows($result) > 0) {
			$array = array();
			while ($row = mysqli_fetch_assoc($result)) {
				$array[] = $row;
			}
			return $array;
		} else {
			return false;
		}
	}

	static function GetMasloList()
	{
		$result = mysqli_query(Model::DB(), "SELECT * FROM `maslo_list` WHERE `id` > '0'");
		if ($result != false && mysqli_num_rows($result) > 0) {
			$array = array();
			while ($row = mysqli_fetch_assoc($result)) {
				$array[] = $row;
			}
			return $array;
		} else {
			return false;
		}
	}

	static function AddLog($value)
	{
		$time = time();
		mysqli_query(Model::DB(), "INSERT INTO `logs`(`value`, `user_create`, `date_create`) VALUES ('$value', '{$_COOKIE['user_id']}', '$time')");
	}

}