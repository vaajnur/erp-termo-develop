<?php
/**
 * Обработчик запросов
 */

namespace StandartRus\App\Core;

class Route
{

	static function Start()
	{
		// контроллер и действие по умолчанию
		$controller_path = $controller_name = 'Login';
		$action = 'index';
		$action_id = 0;

		global $routes;
		$routes = explode('/', $_SERVER['REQUEST_URI']);
		foreach ($routes as $route) {
			if (strpos($route, '?') === false && strpos($route, '_') !== false) Route::ErrorPage404(); //URL можно использовать только с -. С _ будет 404 Error.
		}

		// получаем папку контроллера
		if (!empty($routes[1]) && strpos($routes[1], '?') === false) $controller_path = $controller_name = mb_convert_case($routes[1], MB_CASE_TITLE, "UTF-8");

		// получаем имя контроллера
		if (!empty($routes[2]) && strpos($routes[2], '?') === false) $controller_name = mb_convert_case(str_replace("-", "_", $routes[2]), MB_CASE_TITLE, "UTF-8");

		// получаем имя экшена
		if (!empty($routes[3]) && strpos($routes[3], '?') === false) $action = $routes[3];

		// получаем доп. параметр экшена
		if (!empty($routes[4]) && strpos($routes[4], '?') === false) $action_id = $routes[4];

		if (empty($action_id)) $action_id = $action; //Если ID экшена пустой, то значит URL 3-х уровневый и нужно записать в action_id параметр из предыдущего уровня урла. Пример: пагинация в блоге

		// подцепляем файл с классом модели (файла модели может и не быть)
		$model_file = $controller_path;
		if ($controller_name != $controller_path) $model_file = $controller_name;

		if (file_exists("app/Models/" . $controller_path . "/" . $model_file . '.php')) {
			include_once "app/Models/" . $controller_path . "/" . $model_file . '.php';
		} elseif (file_exists("app/Models/" . $controller_path . "/" . $controller_path . '.php')) {
			include_once "app/Models/" . $controller_path . "/" . $controller_path . '.php';
		}

		// подцепляем файл с классом контроллера
		$controller_file = $controller_path;
		if ($controller_name != $controller_path) $controller_file = $controller_name;

		if (file_exists("app/Controllers/" . $controller_path . "/" . $controller_file . '.php')) {
			include_once "app/Controllers/" . $controller_path . "/" . $controller_file . '.php';
		} elseif (file_exists("app/Controllers/" . $controller_path . "/" . $controller_path . '.php')) {
			include_once "app/Controllers/" . $controller_path . "/" . $controller_path . '.php';
			$action = $controller_file;
		} else {
			Route::ErrorPage404();
		}

		//Подгружаем нужный контроллер. Используем namespace
		if ($controller_path == 404) {
			$controller_class_name = 'StandartRus\\App\\Controllers\\HTTP404';
		} else {
			if ($controller_path != $controller_file && $action != $controller_file) $controller_class_name = 'StandartRus\\App\\Controllers\\' . $controller_path . '\\' . $controller_file;
			else $controller_class_name = 'StandartRus\\App\\Controllers\\' . $controller_path;
		}

		// создаем контроллер
		$controller = new $controller_class_name;
		if (method_exists($controller, $action)) {
			// вызываем действие контроллера
			if (empty($action_id)) $controller->$action();
			else $controller->$action($action_id);
		} elseif (method_exists($controller, "detail")) {
			if (empty($action)) $controller->detail();
			else $controller->detail($action);
		} else {
			Route::ErrorPage404();
		}

	}

	static function ErrorPage404()
	{
		header('HTTP/1.0 404 Not Found');
		header('Location: http://' . $_SERVER['HTTP_HOST'] . '/404/');
		exit;
	}

}