<?php

namespace StandartRus\App\Core;

class View extends Model
{

	protected $mess;
	protected $routes;

	function generate($path_page, $name_page, $template, $data = null)
	{
		global $mess; //языковая переменная
		global $routes;
		global $seo;

		if (isset($_COOKIE['user_id'])) {
			$user_id = $_COOKIE['user_id'];

			$result = mysqli_query($this->connection, "SELECT `name`, `surname`, `email`, `type`, `position`, `photo` FROM `users` WHERE `id` = '$user_id'");
			if (mysqli_num_rows($result) > 0) {
				$row = mysqli_fetch_array($result);
				// var_dump($row);
				$user_name = $row['name'];
				$user_photo = $row['photo'];
				if($row['photo'] == "/" || $row['photo'] == "")
					$user_photo = PHOTO_EMPTY;
				$user_surname = $row['surname'];
				$user_email = $row['email'];
				$user_type = $row['type'];
				$user_position = $row['position'];
			}
		}

		$active_folder = array();
		$active_folder_exp = explode("_", $name_page); //получаем активную директорию

		foreach ($active_folder_exp as $folder) {
			if (strpos($folder, '.php') !== false) {
				$active_folder[] = mb_strtoupper(strstr($folder, '.', true)); //удаляем расширение файла
			} else {
				$active_folder[] = mb_strtoupper($folder);
			}
		}

		$url = '/';

		foreach ($routes as $route) {
			if (!empty($route)) $url .= $route . '/';
		}

		$storage_list = array();
		$storage_menu = array();
		$result = mysqli_query($this->connection, "SELECT * FROM `storage` WHERE `id` > '0' ORDER BY `order` ASC");
		if (mysqli_num_rows($result) > 0) {
			while ($row = mysqli_fetch_assoc($result)) {
				// $this->pr($row);
				// var_dump($row['group']);
				$storage_menu[$row['group']][] = $row;
				$storage_list[] = $row;
			}
		}

		// $this->pr($storage_menu);
		// exit;

		$breed_active = 'all';
		$tint_active = 'all';

		if (!empty($_GET['breed'])) $breed_active = $_GET['breed'];
		if (!empty($_GET['tint'])) $tint_active = $_GET['tint'];

		$name_storage = '';

		if (isset($routes[2])) {
			$result = mysqli_query($this->connection, "SELECT `name` FROM `storage` WHERE `id` = '{$routes[2]}' LIMIT 1");
			if (mysqli_num_rows($result) > 0) {
				$row = mysqli_fetch_assoc($result);
				$name_storage = $row['name'];
			}
		}

		include 'app/Views/Templates/' . $template;
	}

	public function f12($var){
		echo "<script>console.log(".json_encode($var, JSON_UNESCAPED_UNICODE).")</script>";
	}

	public function pr($d){
		echo "<pre>";
		print_r($d);
		echo "</pre>";
	}

	public function requestUriAddGetParams(array $params)
	{
	    $parseRes=parse_url($_REQUEST['REQUEST_URI']);
	    $params=array_merge($_GET, $params);
	    return $parseRes['path'].'?'.http_build_query($params);
	}

}