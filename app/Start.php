<?php

namespace StandartRus\App;

use \StandartRus\App\Core\Model;
use \StandartRus\App\Core\Auth;
use \StandartRus\App\Core\Route;

class Start
{

	static function Go()
	{
		global $mess; //языковая переменная

		session_start();

		//Добавляем слеш в конце URL, если он отсутствует
		$exp = explode('?', $_SERVER['REQUEST_URI']);
		if (mb_substr($exp[0], -1) != '/' && $_SERVER['REQUEST_METHOD'] === 'GET') {
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: ' . $exp[0] . '/' . (!empty($exp[1]) ? '?' . $exp[1] : ''));
			exit();
		}

		if (!isset($_COOKIE['lang'])) $lang = "Ru"; //по умолчанию устанавливаем русский язык
		else $lang = $_COOKIE['lang'];

		//подключаем языковые файлы
		include_once 'Lang/' . $lang . '/Index.php'; //подключаем основной языковой файл

		Auth::Check(); //проверяем авторизацию пользователя

		$files_lang_page = array();
		Start::findFilesFromDirectory($_SERVER['DOCUMENT_ROOT'] . '/app/Lang/' . $lang, $files_lang_page); //сканируем папку с языковыми файлами страниц

		foreach ($files_lang_page as $value) {
			include_once $value; //подключаем языковые файлы всех страниц
		}

		//Маршрутизатор запросов
		Route::Start(); //запускаем маршрутизатор
	}

	static function FindFilesFromDirectory($path, &$files)
	{
		if (is_dir($path)) {
			$cleanPath = array_diff(scandir($path), array('.', '..'));
			$skip = array('.', '..', '.DS_Store');
			foreach ($cleanPath as $file) {
				$finalPath = $path . '/' . $file;
				$result = Start::findFilesFromDirectory($finalPath, $files);
				if (!is_null($result) && !in_array($file, $skip)) $files[] = $result;
			}
		} else if (is_file($path)) {
			return $path;
		}
	}

}