<?php

namespace StandartRus\App\Controllers;

use \StandartRus\App\Core\Controller;
use \StandartRus\App\Models\Logs as Model;
use \StandartRus\App\Core\View;

class Logs
{

	public $model;
	public $view;

	function __construct()
	{
		$this->model = new Model();
		$this->view = new View();
	}

	function index()
	{
		if(Model::GetUserType() != IS_ADMIN && Model::GetUserType() != IS_MANAGER)
			die();
		$data = $this->model->GetListLogs();

		$this->view->generate('Logs', 'Logs.php', 'Template.php', $data);
	}

}