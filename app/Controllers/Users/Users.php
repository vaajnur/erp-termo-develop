<?php

namespace StandartRus\App\Controllers;

use \StandartRus\App\Core\Controller;
use \StandartRus\App\Models\Users as Model;
use \StandartRus\App\Core\View;
use \StandartRus\App\Core\Auth;

class Users
{

	public $model;
	public $view;
	protected $user_type;

	function __construct()
	{
		$this->model = new Model();
		$this->view = new View();
		$this->user_type = Model::GetUserType();
	}

	function index()
	{
		if(Model::GetUserType() != IS_ADMIN)
			die();
		//if (Auth::Exists() === true) header("Location: /");
		$data['roles'] = Model::getRoles();
		foreach($data['roles'] as $role){
			$data['users'][$role['id']] = Model::getUsersByType($role['id']);
		}
		$this->view->generate('Users', 'Users.php', 'Template.php', $data);
	}

	function Delete(){
		if(Model::GetUserType() != IS_ADMIN )
			die();
		if (isset($_POST)) {
			$this->model->id = $_POST['id'];
			if ($this->model->Delete() === true) echo "success::success;";
			else echo "error:error;";
		} else {
			echo "error:error;";
		}
	}

	function Add()
	{
		if(Model::GetUserType() != IS_ADMIN)
			die();
		$errors = 0;
		if (isset($_POST)) {
			$photo = str_replace($_SERVER['DOCUMENT_ROOT'],"",Controller::UploadFile($_FILES['image']));
			if (empty($_POST['name'])){
				$errors++;
				echo "error:name;";
			}
			if (empty($_POST['surname'])){
				$errors++;
				echo "error:surname;";
			}
			if (empty($_POST['patronymic'])){
				$errors++;
				echo "error:patronymic;";
			}
			if (empty($_POST['phone'])){
				$errors++;
				echo "error:phone;";
			}
			if (empty($_POST['email'])){
				$errors++;
				echo "error:email;";
			}
			if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
				$errors++;
				echo "error:email;";
			}
			if (!empty($_POST['email']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
				$users = Model::getUsers();
				foreach($users as $user){
					if($user['email'] == $_POST['email']){
						$errors++;
						echo "error:email;";
					}
				}
			}
			if (empty($_POST['password'])){
				$errors++;
				echo "error:password;";
			}
			if($errors == 0) {
				$this->model->name = $_POST['name'];
				$this->model->surname = $_POST['surname'];
				$this->model->patronymic = $_POST['patronymic'];
				$this->model->phone = $_POST['phone'];
				$this->model->photo = $photo;
				$this->model->email = $_POST['email'];
				$this->model->password = password_hash(md5(md5($_POST['password'])), PASSWORD_DEFAULT);
				$this->model->role = $_POST['type'];
				if ($this->model->Add() === true) echo "success::success;";
				else echo "error:error;";
			}
		} else {
			echo "error:error;";
		}
	}

	function Update()
	{
		if(Model::GetUserType() != IS_ADMIN)
			die();
		$errors = 0;
		if (isset($_POST)) {
			$photo = str_replace($_SERVER['DOCUMENT_ROOT'],"",Controller::UploadFile($_FILES['image']));
			if (empty($_POST['name'])){
				$errors++;
				echo "error:name;";
			}
			if (empty($_POST['surname'])){
				$errors++;
				echo "error:surname;";
			}
			if (empty($_POST['patronymic'])){
				$errors++;
				echo "error:patronymic;";
			}
			if (empty($_POST['phone'])){
				$errors++;
				echo "error:phone;";
			}
			if (empty($_POST['email'])){
				$errors++;
				echo "error:email;";
			}
			if($errors == 0) {
				$this->model->id = $_POST['id'];
				$this->model->name = $_POST['name'];
				$this->model->surname = $_POST['surname'];
				$this->model->patronymic = $_POST['patronymic'];
				$this->model->photo = $photo;
				$this->model->phone = $_POST['phone'];
				$this->model->email = $_POST['email'];
				if(strlen($_POST['password']) > 0)
					$this->model->password = password_hash(md5(md5($_POST['password'])), PASSWORD_DEFAULT);
				else
					$this->model->password = "";
				$this->model->role = $_POST['type'];
				if ($this->model->Update() === true) echo "success::success;";
				else echo "error:error;";
			}
		} else {
			echo "error:error;";
		}
	}

}