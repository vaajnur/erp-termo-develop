<?php

namespace StandartRus\App\Controllers;

use \StandartRus\App\Core\Controller;
use \StandartRus\App\Models\Guides as Model;
use \StandartRus\App\Core\View;

class Guides
{

	/**
	 * @var Model
	 */
	public $model;
	/**
	 * @var View
	 */
	public $view;

	/**
	 * Storage constructor.
	 */
	function __construct()
	{
		$this->model = new Model();
		$this->view = new View();
	}

	/**
	 * Главная страница
	 */
	function index()
	{
		$data = $this->model->getGuides();

		$this->view->generate('Guides', 'Guides.php', 'Template.php', $data);
	}

	function edit(){
		$id = $_POST['id'];
		$name = $_POST['name'];
		$action = $_POST['action'];
		if($action == 'del'){
			$res = $this->model->DeleteRawGuide($id, $name);
		}elseif($action == 'edit'){
			$new_val = $_POST['new_val'];
			$res = $this->model->EditRawGuide($id, $name, $new_val);
		}
		echo "good";
		// echo $res;
	}

	function add($name){
		$this->model->addGuide($_POST['table'], $_POST['name']);
	}


}