<?php

namespace StandartRus\App\Controllers;

use \StandartRus\App\Core\Controller;
use \StandartRus\App\Models\Storage as Model;
use \StandartRus\App\Core\View;

/**
 * Class Storage
 *
 * Контроллер складов.
 *
 * @author Матовников Александр matovnikovaleks@mail.ru
 * @package StandartRus\App\Controllers
 */
class Storage
{

	/**
	 * @var Model
	 */
	public $model;
	/**
	 * @var View
	 */
	public $view;

	/**
	 * Storage constructor.
	 */
	function __construct()
	{
		$this->model = new Model();
		$this->view = new View();
	}

	/**
	 * Главная страница
	 */
	function index()
	{
		if(Model::GetUserType() != IS_ADMIN && Model::GetUserType() != IS_MANAGER)
			die();
		$this->view->generate('Storage', 'Storage.php', 'Template.php');
	}

	/**
	 * Детальная страница
	 *
	 * @param $id
	 */
	function detail($id)
	{
		// if(!empty($_POST))
			// file_put_contents(__DIR__.'/filename.log', var_export($_POST, true));
		// редактирование партии
		if($_POST['postavka_nomer'] !== null) {
			$this->model->consignment = $_POST['consignment'];
      
			$this->model->postavka_nomer = $_POST['postavka_nomer'];
			$this->model->postavka_postavsik = $_POST['postavka_postavsik'];
			$this->model->postavka_dispecer = $_POST['postavka_dispecer'];
			$this->model->postavka_adres = $_POST['postavka_adres'];
			$this->model->postavka_dataop = $_POST['postavka_dataop'];
			$this->model->postavka_dataof = $_POST['postavka_dataof'];
			$this->model->postavka_datapp = $_POST['postavka_datapp'];
			$this->model->postavka_datapf = $_POST['postavka_datapf'];
			$this->model->postavka_markaam = $_POST['postavka_markaam'];
			$this->model->postavka_nomeram = $_POST['postavka_nomeram'];
			$this->model->postavka_obiem = $_POST['postavka_obiem'];
			$cons_id = $_POST['cons_id'];
			if ($this->model->editConsignments($cons_id) === true) 
				header('Location: '.$_SERVER['REQUEST_URI']);
			// else echo "error:error;";
		}
		if(Model::GetUserType() != IS_ADMIN && Model::GetUserType() != IS_MANAGER ){
			//бикос и покраска
			if(!(($id == 5 || $id == 6) && Model::GetUserType() == IS_MANAGER_BIKOS)){
				if(!($id == 4 && Model::GetUserType() == IS_PAINTER)){
					die();
				}
			}
		}
		if($id == 2){
			$data['btn_text'] = "Отправить на склад";
			$data['btn_text_post'] = "Товары созданы";
			$data['storage_success'] = 3;
		}
		if($id == 7){
			$data['btn_text'] = "Отправить на термирование";
			$data['btn_text_post'] = "Товары отправлены";
			$data['storage_success'] = 8;
		}

		if($id == 8){
			$data['btn_text'] = "Отправить на строгание";
			$data['btn_text_post'] = "Товары отправлены";
			$data['storage_success'] = 2;
		}
		$data['names'] = Model::GetListNames();
		$data['cort'] = Model::GetListCorts();
		$data['breed'] = Model::GetListBreed();
		$data['tint'] = Model::GetListTint();
		$data['color'] = Model::GetListColor();
		$data['maslo'] = Model::GetMasloList();
		$data['profiles'] = Model::GetProfiles();
		$breed_active = 'all';
		$profiles_active = 'all';
		if (!empty($_GET['breed'])) $breed_active = $_GET['breed'];
		if (!empty($_GET['profiles'])) $profiles_active = $_GET['profiles'];
		// for filter
		$data['profiles_list'] = Model::GetProfilesAll($breed_active);
		/*echo "<pre>";
		print_r($data);
		echo "</pre>";*/

		$tint_active = 'all';

		if (!empty($data['tint'])) {
			foreach ($data['tint'] as $tint) {
				if ($tint['breed'] == $breed_active) {
					$tint_active = $tint['id'];
					break;
				}
			}
		}

		if (!empty($_GET['tint'])) $tint_active = $_GET['tint'];

		$this->model->storage = $id;
		$this->model->breed = $breed_active;
		$this->model->profile = $profiles_active;
		$this->model->tint = $tint_active;
		$data['consignments'] = $this->model->GetConsignments();
		// echo "<pre>";
		// print_r($data['consignments']);
		// echo "</pre>";
		$data['consignmentsAllStorage'] = $this->model->GetConsignmentsAllStorage();

		$this->view->generate('Storage', 'Detail.php', 'Template.php', $data);
	}

	/**
	 * Получение списка оттенков породы по ID породы
	 */
	function GetListTintByID()
	{
		echo json_encode(Model::GetListTint($_POST['id']));
	}

	/**
	 * Создание новой партии
	 */
	function AddConsignment()
	{
		// file_put_contents(__DIR__.'/filename.log', var_export($_POST, true));
		$id = $_POST['storage'];
		if(Model::GetUserType() != IS_ADMIN && Model::GetUserType() != IS_MANAGER ){
			//бикос и покраска
			if(!(($id == 5 || $id == 6) && Model::GetUserType() == IS_MANAGER_BIKOS)){
				if(!($id == 4 && Model::GetUserType() == IS_PAINTER)){
					die();
				}
			}
		}
		if (false /*empty($_POST['breed'])*/) echo "error:breed;";
		else {
			$this->model->consignment = $_POST['consignment'];
      
			$this->model->postavka_nomer = $_POST['postavka_nomer'];
			$this->model->postavka_postavsik = $_POST['postavka_postavsik'];
			$this->model->postavka_dispecer = $_POST['postavka_dispecer'];
			$this->model->postavka_adres = $_POST['postavka_adres'];
			$this->model->postavka_dataop = $_POST['postavka_dataop'];
			$this->model->postavka_dataof = $_POST['postavka_dataof'];
			$this->model->postavka_datapp = $_POST['postavka_datapp'];
			$this->model->postavka_datapf = $_POST['postavka_datapf'];
			$this->model->postavka_markaam = $_POST['postavka_markaam'];
			$this->model->postavka_nomeram = $_POST['postavka_nomeram'];
			$this->model->postavka_obiem = $_POST['postavka_obiem'];
			$this->model->title = $_POST['title'];
			if(isset($_POST['date_finish'])){
				$date1 = new \DateTime($_POST['date_finish']);
				$this->model->date_finish = $date1->getTimestamp();
			}else{
				$this->model->date_finish = null;
			}
      
			$this->model->profile = $_POST['profile'];      
			$this->model->storage = $_POST['storage'];
			$this->model->breed = $_POST['breed'];
			$this->model->tint = $_POST['tint'];
			$this->model->pack = $_POST['pack'];
			$this->model->name = $_POST['name'];
			// $this->model->cort = $_POST['cort'];
			$this->model->quantity = $_POST['quantity'];
			$this->model->thickness = $_POST['thickness'];
			$this->model->width = $_POST['width'];
			$this->model->length = $_POST['length'];
			$this->model->volume_m3 = $_POST['volume_m3'];
			if ($this->model->AddConsignments() === true) echo "success::success;";
			else echo "error:error;";
		}
	}

	/**
	 * Удаление товара из партии по ID
	 */
	function DeleteRawMaterial()
	{
		if (!empty($_POST['id'])) {
			$this->model->id = $_POST['id'];
			$this->model->DeleteRawMaterial();
		}
	}

	/**
	 * Удаление партии по ID
	 */
	function DeleteAllMaterial()
	{
		if (!empty($_POST['id'])) {
			$this->model->id = $_POST['id'];
			$this->model->DeleteAllMaterial();
		}
	}

	/**
	 * Сохранение изменений товара из партии по ID
	 */
	function SaveRawMaterial()
	{
		// file_put_contents(__DIR__.'/filename-2.log', var_export($_POST, true));
		if (isset($_POST)) {
			if (isset($_POST['pack']) && empty($_POST['pack'])) echo "error:pack;";
			elseif (empty($_POST['name'])) echo "error:name;";
			elseif (empty($_POST['quantity'])) echo "error:quantity;";
			elseif (empty($_POST['thickness'])) echo "error:thickness;";
			elseif (empty($_POST['width'])) echo "error:width;";
			elseif (empty($_POST['length'])) echo "error:length;";
			elseif (empty($_POST['volume_m3'])) echo "error:volume_m3;";
			else {
				$this->model->id = $_POST['id'];
				$this->model->breed = $_POST['breed'];
				$this->model->pack = $_POST['pack'];
				$this->model->color = $_POST['color'];
				$this->model->name = $_POST['name'];
				$this->model->profile = $_POST['profile'];
				$this->model->quantity = $_POST['quantity'];
				$this->model->thickness = $_POST['thickness'];
				$this->model->width = $_POST['width'];
				$this->model->length = $_POST['length'];
				$this->model->volume_m3 = $_POST['volume_m3'];
				$this->model->consignment_id_success = $_POST['consignment_id_success'];
				// дата сушки
				if ($_POST['date_finish'] != false) {
					$d = new \datetime($_POST['date_finish']);
					$this->model->date_finish = $d->gettimestamp();
					var_dump($this->model->date_finish);
				}
				$answer = $this->model->SaveRawMaterial();
				if ($answer === true) echo "success::success;";
				else echo "success::success;id::$answer";
			}
		} else {
			echo "error:error;";
		}
	}

	/**
	 * Отправка выбранных материалов с склада Сырье на склад Производство
	 */
	function SendRawMaterialsToManufacture()
	{
		// file_put_contents(__DIR__.'/filename.log', var_export($_POST, true));
		if (isset($_POST)) {
			$date = strtotime($_POST['date']);
			if (false /* || empty($date) || $date < time()*/) echo "error:date;";
			else {
				$this->model->consignment = $_POST['consignment'];
				$this->model->storage = $_POST['storage'];
				$this->model->breed = $_POST['breed'];
				$this->model->tint = $_POST['tint'];
				$this->model->data = $_POST['array'];
				$this->model->tocolor = $_POST['tocolor'];
				$this->model->date = $date;
				if ($this->model->SendRawMaterialsToManufacture() === true) echo "success::success;";
				else echo "error:error;";
			}
		} else {
			echo "error:error;";
		}
	}

	/**
	 * Создании партии для склада Продукция с конечными товарами после производства
	 */
	function AddConsignmentProducts()
	{
		$id = $_POST['storage'];
		if(Model::GetUserType() != IS_ADMIN && Model::GetUserType() != IS_MANAGER ){
			//бикос и покраска
			if(!(($id == 5 || $id == 6) && Model::GetUserType() == IS_MANAGER_BIKOS)){
				if(!($id == 4 && Model::GetUserType() == IS_PAINTER)){
					die();
				}
			}
		}
		if (empty($_POST['breed'])) echo "error:breed;";
		elseif (empty($_POST['tint'])) echo "error:tint;";
		else {
			$this->model->storage = $_POST['storage'];
			$this->model->consignment_id_success = $_POST['consignment'];
			$this->model->breed = $_POST['breed'];
			$this->model->tint = $_POST['tint'];
			$this->model->name = $_POST['name'];
			$this->model->pack = $_POST['pack'];
			$this->model->profile = $_POST['profile'];
			$this->model->quantity = $_POST['quantity'];
			$this->model->thickness = $_POST['thickness'];
			$this->model->width = $_POST['width'];
			$this->model->length = $_POST['length'];
			$this->model->volume_m3 = $_POST['volume_m3'];
			$ids = $this->model->AddConsignmentsProducts();
			if (count($ids) > 0){
				if($_POST['paint'] == "y"){
					$color = $_POST['select_color'];
					$data = [];
					foreach($ids as $k=>$id){
						$item[0] = $id;
						$item[1] = $this->model->quantity[$k];
						$item[2] = $color;
						$data[] = $item;
					}
					$this->model->data = $data;
					if ($this->model->SendProductsToRepair() === true) echo "success::success;";
				}
				else{
					echo "success::success;";
				}
			}
			else echo "error:error;";
		}
	}

	/**
	 * Отправка товаров с склада Продукция на склад Покраска
	 */
	function SendProductsToRepair()
	{
		if (isset($_POST)) {
			$this->model->breed = $_POST['breed'];
			$this->model->tint = $_POST['tint'];
			$this->model->data = $_POST['array'];
			if ($this->model->SendProductsToRepair() === true) echo "success::success;";
			else echo "error:error;";
		} else {
			echo "error:error;";
		}
	}

	/**
	 * Изменение статуса покраски партии на складе Покраска
	 */
	function UpdateStatusConsignment()
	{
		if (isset($_POST)) {
			$this->model->id = $_POST['id'];
			$this->model->status = $_POST['status'];
			if ($this->model->UpdateStatusConsignment() === true) echo "success::success;";
			else echo "error:error;";
		} else {
			echo "error:error;";
		}
	}

	function DeleteConsignment()
	{
		if (isset($_POST)) {
			$this->model->id = $_POST['id'];
			if ($this->model->DeleteConsignment() === true) echo "success::success;";
			else echo "error:error;";
		} else {
			echo "error:error;";
		}
	}

	function UpdateDescriptionConsignment()
	{
		if (isset($_POST)) {
			$this->model->id = $_POST['id'];
			$this->model->description = $_POST['description'];
			if ($this->model->UpdateDescriptionConsignment() === true) echo "success::success;";
			else echo "error:error;";
		} else {
			echo "error:error;";
		}
	}

	function UpdateTitleConsignment()
	{
		if (isset($_POST)) {
			$this->model->id = $_POST['id'];
			$this->model->title = $_POST['title'];
			if ($this->model->UpdateTitleConsignment() === true) echo "success::success;";
			else echo "error:error;";
		} else {
			echo "error:error;";
		}
	}

	public function joinGroups(){
		// echo "consign_ids";
		// var_dump($_POST['consign_ids']);
		// echo "raw_materials_ids";
		// var_dump($_POST['raw_materials_ids']);
		if(count($_POST['consign_ids']) > 1 ){
			$res = $this->model->join_consigns($_POST['consign_ids']);
			var_dump($res);
		}
		if(count($_POST['raw_materials_ids']) > 1){
			$res = $this->model->join_raws($_POST['raw_materials_ids'], $_POST['storage']);
			var_dump($res);
		}
	}

}