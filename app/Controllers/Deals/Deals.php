<?php

namespace StandartRus\App\Controllers;

use \StandartRus\App\Core\Controller;
use \StandartRus\App\Models\Deals as Model;
use \StandartRus\App\Core\View;

class Deals
{
		/**
	 * @var Model
	 */
	public $model;
	/**
	 * @var View
	 */
	public $view;

	/**
	 * Storage constructor.
	 */
	function __construct()
	{
		$this->model = new Model();
		$this->view = new View();
	}

	/**
	 * Главная страница
	 */
	function index()
	{

		if(!empty($_POST))
			file_put_contents(__DIR__.'/filename.log', var_export($_POST, true));
		$data['deals'] = $this->model->getDeals();
		$data['guides'] = $this->model->getGuides();
		if(isset($_POST['action']) && $_POST['action'] == 'edit' && $_POST['id'] > 0 )
			$this->model->editDeal($_POST);
		elseif(isset($_POST['action']) && $_POST['action'] == 'edit')
			$this->model->addDeal($_POST);
		$this->view->generate('Deals', 'Deals.php', 'Template.php', $data);
	}
}