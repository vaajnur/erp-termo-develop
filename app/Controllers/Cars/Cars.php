<?php

namespace StandartRus\App\Controllers;

use \StandartRus\App\Core\Controller;
use \StandartRus\App\Models\Cars as Model;
use \StandartRus\App\Core\View;

class Cars
{

	/**
	 * @var Model
	 */
	public $model;
	/**
	 * @var View
	 */
	public $view;

	/**
	 * Storage constructor.
	 */
	function __construct()
	{
		$this->model = new Model();
		$this->view = new View();
	}

	/**
	 * Главная страница
	 */
	function index()
	{
		$data['cars'] = $this->model->GetCars();
		$this->view->generate('Cars', 'Cars.php', 'Template.php', $data);
	}


}