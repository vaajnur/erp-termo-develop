<?php

namespace StandartRus\App\Controllers;

use \StandartRus\App\Core\Controller;
use \StandartRus\App\Models\Test as Model;
use \StandartRus\App\Core\View;

class Test
{

	/**
	 * @var Model
	 */
	public $model;
	/**
	 * @var View
	 */
	public $view;

	/**
	 * Storage constructor.
	 */
	function __construct()
	{
		$this->model = new Model();
		$this->view = new View();
	}

	/**
	 * Главная страница
	 */
	function index()
	{
		// $row = $this->model->GetData();
		// $row = $this->model->DeleteRow();
		// $row = $this->model->UpdateRow();
		$row = $this->model->getRowsConsign();
		echo "<pre>";
		// print_r($row);

		$row = array_reduce($row, function($carry, $item){
			/*if(empty($carry))
				$carry = array_merge_recursive($carry, $item);
			else*/
				$carry = array_merge_recursive($carry, $item);
			// print_r($carry);
			array_walk($carry, function(&$item){
				// var_dump($item);
				if(is_array($item)){
					$item = array_unique(array_filter($item))[0];
				}
			});

			return ($carry);
		}, []);

		// print_r($row);
		echo "</pre>";
		// var_dump(12312312312);
		// $this->view->generate('Test', 'Test.php', 'Empty.php');
		$this->view->generate('Test', 'Test.php', 'Template.php');
	}


}