<?php

namespace StandartRus\App\Controllers;

use \StandartRus\App\Core\Controller;
use \StandartRus\App\Models\Login as Model;
use \StandartRus\App\Core\View;
use \StandartRus\App\Core\Auth;

class Login
{

	public $model;
	public $view;
	protected $user_type;

	function __construct()
	{
		$this->model = new Model();
		$this->view = new View();
		$this->user_type = Model::GetUserType();
	}

	function index()
	{
		//if (Auth::Exists() === true) header("Location: /");
		$this->view->generate('Login', 'Login.php', 'Template_login.php');
	}

	function CheckForm()
	{
		$email = $_POST['email'];
		$password = $_POST['password'];

		if (empty($email)) echo "error:email;";
		elseif (empty($password)) echo "error:password;";
		else {
			$this->model->email = $email;
			$this->model->password = $password;
			$check_acc = $this->model->check_acc();
			echo $check_acc;
		}
	}

	function Logout()
	{
		setcookie("user_id", "", time() - 360000, "/");
		setcookie("user_hash", "", time() - 360000, "/");
		header("Location: /");
	}

}