<?php

namespace StandartRus\App\Controllers;

use \StandartRus\App\Core\View;

class HTTP404
{

	public $view;

	function __construct()
	{
		$this->view = new View();
	}

	function index()
	{
		header('HTTP/1.1 404 Not Found');
		header("Status: 404 Not Found");
		$this->view->generate('404', '404.php', 'Template_404.php');
	}

}