<?php

namespace StandartRus\App\Controllers;

use \StandartRus\App\Core\Controller;
use \StandartRus\App\Models\Sales as Model;
use \StandartRus\App\Core\View;

class Sales
{

	public $model;
	public $view;

	function __construct()
	{
		$this->model = new Model();
		$this->view = new View();
	}

	function index()
	{
		if(Model::GetUserType() != IS_ADMIN && Model::GetUserType() != IS_MANAGER)
			die();
		$data['archive'] = $this->model->CheckArchive();
		$data['breed'] = Model::GetListBreed();
		$data['tint'] = Model::GetListTint();
		$data['color'] = Model::GetListColor();
		$data['goods'] = $this->model->GetAllGoods();
		$data['sales'] = $this->model->GetSales();
		$data['status'] = $this->model->GetStatusList();
		$this->view->generate('Sales', 'Sales.php', 'Template.php', $data);
	}
	function Archive( $id = 1 ){
		if(Model::GetUserType() != IS_ADMIN && Model::GetUserType() != IS_MANAGER)
			die();
		$data['status'] = $this->model->GetStatusList();
		$data['page'] = $id;
		$data['limit'] = 10;
		$data['count'] = $this->model->CheckArchive();
		$data['pages'] = ceil(intval($data['count'])/intval($data['limit']));
		$data['sales'] = $this->model->GetSales("archive",$id,$data['limit']);
		$this->view->generate('Sales', 'SalesArchive.php', 'Template.php', $data);
	}
	function UpdateStatusSale(){
		if(Model::GetUserType() != IS_ADMIN && Model::GetUserType() != IS_MANAGER)
			die();
		if (isset($_POST)) {
			$this->model->id = $_POST['id'];
			$this->model->status = $_POST['status'];
			if ($this->model->UpdateStatusSale() === true) echo "success::success;";
			else echo "error:error;";
		} else {
			echo "error:error;";
		}
	}
	function Delete(){
		if(Model::GetUserType() != IS_ADMIN && Model::GetUserType() != IS_MANAGER)
			die();
		if (isset($_POST)) {
			$this->model->id = $_POST['id'];
			if ($this->model->Delete() === true) echo "success::success;";
			else echo "error:error;";
		} else {
			echo "error:error;";
		}
	}
	function Add()
	{
		if(Model::GetUserType() != IS_ADMIN && Model::GetUserType() != IS_MANAGER)
			die();
		if (isset($_POST)) {
			if (empty($_POST['contract'])) echo "error:contract;";
			if (empty($_POST['type'])) echo "error:type;";
			if (empty($_POST['cost'])) echo "error:cost;";
			if (empty($_POST['payment_type'])) echo "error:payment_type;";
			else {
				$this->model->contract = $_POST['contract'];
				$this->model->fio = $_POST['fio'];
				$this->model->type = $_POST['type'];
				$this->model->status = $_POST['status'];
				$this->model->cost = $_POST['cost'];
				$this->model->prepaid = $_POST['prepaid'];
				$this->model->payment_type = $_POST['payment_type'];
				$this->model->data = $_POST['array'];
				if ($this->model->Add() === true) echo "success::success;";
				else echo "error:error;";
			}
		} else {
			echo "error:error;";
		}
	}
	function Update()
	{
		if(Model::GetUserType() != IS_ADMIN && Model::GetUserType() != IS_MANAGER)
			die();
		if (isset($_POST)) {
			if (empty($_POST['id'])) echo "error:id;";
			if (empty($_POST['contract'])) echo "error:contract;";
			if (empty($_POST['type'])) echo "error:type;";
			if (empty($_POST['cost'])) echo "error:cost;";
			if (empty($_POST['payment_type'])) echo "error:payment_type;";
			else {
				$this->model->id = $_POST['id'];
				$this->model->contract = $_POST['contract'];
				$this->model->fio = $_POST['fio'];
				$this->model->type = $_POST['type'];
				$this->model->status = $_POST['status'];
				$this->model->cost = $_POST['cost'];
				$this->model->prepaid = $_POST['prepaid'];
				$this->model->payment_type = $_POST['payment_type'];
				$this->model->data = $_POST['array'];
				if ($this->model->Update() === true) echo "success::success;";
				else echo "error:error;";
			}
		} else {
			echo "error:error;";
		}
	}

}